import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { TrackAndCart } from "./trackandcart.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { StandardScene } from "./standardscene.js";
import { SVGPlot } from "./svgplot.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var TwoCartCollision = function( inparams ) 
{
    // debugger;
    var self = this;
    var params = { "campos": new THREE.Vector3( 0., 19., 94. ),
                   "camlook": new THREE.Vector3( 0., 0, 0. ),
                   "camfov": 60.,
                   "recorddt": 0.1,
                   "showinterface": true,
                   "showplots": true,
                   "debugfocusscale": 3,
                   "axeslen": 20,
                   "printfpsevery": 0 // 120
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );

    this.showinterface = params.showinterface;
    this.showplots = params.showplots;
    
    this.track = new TrackAndCart.Track( this.scene );
    this.cart1 = new TrackAndCart.Cart( this.track, { color: 0x990000,
                                                      mass: 450,
                                                      barmass: 250,
                                                      name: "Red Cart"
                                                    } );
    this.cart1.x = -50;
    this.cart2 = new TrackAndCart.Cart( this.track, { color: 0x000099,
                                                      mass: 450,
                                                      barmass: 250,
                                                      name: "Blue Cart"
                                                    } );
    this.cart2.x = 50;

    this.x1dataset = null;
    this.x2dataset = null;
    this.vx1dataset = null;
    this.vx2dataset = null;
    this.recordcallback = function( t ) { self.recordData( t ); };
    this.t0 = 0;
    this.recorddt = params.recorddt;
    
    this.track.refpos = { x: 0, y: -20, z: 0 };
    this.defaulttrackangle = 0;
    this.minangle = -60;
    this.maxangle = 60;
    this.track.setAngle( this.defaulttrackangle );

    this.table = new PhysicsObjects.Table( );
    // table.obj.position.set( 0, track.obj.position.y-4*track.height, 0 );
    this.table.position = { x: 0, y: this.track.y - this.track.height - this.track.leglength, z: 0 };
    this.scene.addObject( this.table );

    if ( params.axes )
    {
        this.axes = new PhysicsObjects.Axes( );
        this.scene.addObject( this.axes );
    }

    var self = this;
    this.scene.addAnimationCallback( function( t ) { self.track.animate( t ) } );
};

// **********************************************************************

TwoCartCollision.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( TwoCartCollision.prototype, "constructor",
                       { value: TwoCartCollision,
                         enumerable: false,
                         writeable: true } );



// **********************************************************************
// This function lays out the simulation window and the buttons to the

TwoCartCollision.prototype.layout = function( )
{
    StandardScene.prototype.layout.call( this );

    var self = this;
    
    if ( this.showinterface ) {
        // ****************************************
        // Buttons to right of scene

        var buttons = {
            type: "box",
            _class: "controlsdiv",
            id: "buttondiv",
            spec: [
                { type: "box",
                  _class: "vbox bordered",
                  id: "trackcontrolsdiv",
                  spec: [
                      { type: "box",
                        _class: "hbox",
                        id: "gravdiv",
                        spec: [
                            { type: "checkbox",
                              id: "gravcheck",
                              label: "Gravity On?",
                              checked: this.track.gravity,
                              callback: function( event ) {
                                  if ( self.gravcheck.checked ) self.track.gravity = true;
                                  else self.track.gravity = false;
                              }
                            },
                            { type: "label",
                              id: "gravcheck_label",
                              _for: "gravcheck",
                              text: "Gravity On?"
                            }
                        ]
                      },

                      { type: "box",
                        _class: "hbox",
                        id: "animdiv",
                        spec: [
                            { type: "checkbox",
                              id: "animcheck",
                              checked: true,
                              callback: function( event ) {
                                  if ( self.animcheck.checked ) self.scene.resumeDynamics();
                                  else self.scene.pauseDynamics();
                              }
                            },
                            { type: "label",
                              id: "animcheck_label",
                              _for: "animcheck",
                              text: "Dynamics On?"
                            }
                        ]
                      },

                      { type: "box",
                        id: "anglediv",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "anglediv_text_1",
                              text: "Track angle: "
                            },
                            { type: "numinput",
                              id: "angleinput",
                              min: -60,
                              max: 60,
                              width: "6ex",
                              initval: this.defaulttrackangle,
                              changecallback: function( event ) {
                                  if ( self.angleinput.value < self.minangle )
                                      self.angleinput.value = self.minangle;
                                  if ( self.angleinput.value > self.maxangle )
                                      self.angleinput.value = self.maxangle;
                                  self.track.setAngle( self.angleinput.value * Math.PI / 180 );
                              }
                            },
                            { type: "span",
                              id: "anglediv_text_2",
                              text: "⁰"
                            }
                        ]
                      },

                      // This is an ugly hack, but for some reason, at least on Firefox, my number inputs
                      //   are spilling over on the bottom
                      { type: "box",
                        id: "uglyhack",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "uglyhack_text",
                              text: " "
                            }
                        ]
                      },
                      
                      { type: "box",
                        id: "lefthitdiv",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "lefthitdiv_text_1",
                              text: "At left edge: "
                            },
                            { type: "select",
                              id: "lefthit",
                              options: [ "Stick", "Bounce", "Wrap" ],
                              selindex: 1,
                              changecallback: function( event ) {
                                  self.track.stick.x0 = false
                                  self.track.periodic.x0 = false
                                  if ( self.lefthit.selectedIndex == 2 )
                                      self.track.periodic.x0 = true
                                  else if ( self.lefthit.selectedIndex == 0 )
                                      self.track.stick.x0 = true;
                              }
                            }
                        ]
                      },

                      { type: "box",
                        id: "righthitdiv",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "righthitdiv_text_1",
                              text: "At right edge: "
                            },
                            { type: "select",
                              id: "righthit",
                              options: [ "Stick", "Bounce", "Wrap" ],
                              selindex: 1,
                              changecallback: function( event ) {
                                  self.track.stick.x1 = false
                                  self.track.periodic.x1 = false
                                  if ( self.righthit.selectedIndex == 2 )
                                      self.track.periodic.x1 = true
                                  else if ( self.righthit.selectedIndex == 0 )
                                      self.track.stick.x1 = true;
                              }
                            }
                        ]
                      },

                      { type: "box",
                        id: "dragdiv",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "dragdiv_text_0",
                              text: "Mouse drag: "
                            },
                            { type: "select",
                              id: "drag",
                              options: [ "Repositions", "Flings" ],
                              selindex: 0,
                              changecallback: function( event ) {
                                  if ( self.drag.selectedIndex == 1 )
                                      self.track.flingcart = true;
                                  else
                                      self.track.flingcart = false;
                              }
                            }
                        ]
                      },
                  ]
                },

                { type: "box",
                  id: "cartcontrolsbox",
                  _class: "vbox bordered margintop",
                  spec: [
                      { type: "button",
                        id: "resetcartsbutton",
                        _class: "margintop",
                        text: "Reset Carts",
                        callback: function() { self.resetCarts(); }
                      },

                      { type: "box",
                        _class: "hbox margintop",
                        id: "collisiontypebox",
                        spec: [
                            { type: "select",                    
                              id: "collisiontype",
                              options: [ "Elastic", "Sticky" ],
                              selindex: 0,
                              changecallback: function() { self.setCollisionType(); }
                            },
                            { type: "span",
                              text: " collisions",
                              id: "collisionlabel"
                            }
                        ]
                      },
                      
                      { type: "box",
                        id: "cart1barsbox",
                        _class: "hbox margintop",
                        spec: [
                            { type: "span",
                              id: "cart1barslabel",
                              text: "Cart 1 bars:"
                            },
                            { type: "numinput",
                              id: "cart1bars",
                              width: "4ex",
                              initval: 0,
                              min: 0,
                              max: 4,
                              step: 1,
                              changecallback: function() { self.adjustBars(); }
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart1massbox",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "cart1masslabel",
                              text: "m₁: "
                            },
                            { type: "input",
                              id: "cart1mass",
                              readonly: true,
                              width: "6ex",
                              _class: "grow"
                            },
                            { type: "span",
                              id: "cart1massunit",
                              text: "g"
                            }
                        ]
                      },
                      
                      { type: "box",
                        id: "cart2barsbox",
                        _class: "hbox margintop",
                        spec: [
                            { type: "span",
                              id: "cart2barslabel",
                              text: "Cart 2 bars:"
                            },
                            { type: "numinput",
                              id: "cart2bars",
                              width: "4ex",
                              initval: 0,
                              min: 0,
                              max: 4,
                              step: 1,
                              changecallback: function() { self.adjustBars(); }
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart2massbox",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "cart2masslabel",
                              text: "m₂: "
                            },
                            { type: "input",
                              id: "cart2mass",
                              readonly: true,
                              width: "6ex",
                              _class: "grow"
                            },
                            { type: "span",
                              id: "cart2massunit",
                              text: "g"
                            }
                        ]
                      },
                      
                      { type: "box",
                        id: "cart1v0box",
                        _class: "hbox margintop",
                        spec: [
                            { type: "span",
                              id: "cart1v0label",
                              text: "v₁₀: "
                            },
                            { type: "input",
                              id: "cart1v0",
                              width: "6ex",
                              initval: "20",
                              _class: "grow",
                            },
                            { type: "span",
                              id: "cart1v0units",
                              text: " cm/s"
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart2v0box",
                        _class: "hbox",
                        spec: [
                            { type: "span",
                              id: "cart2v0label",
                              text: "v₂₀: "
                            },
                            { type: "input",
                              id: "cart2v0",
                              width: "6ex",
                              initval: "0",
                              _class: "grow",
                            },
                            { type: "span",
                              id: "cart2v0units",
                              text: " cm/s"
                            }
                        ]
                      },
                      { type: "button",
                        id: "kickcartsbutton",
                        text: "Kick Carts",
                        callback: function() { self.kickCarts(); }
                      },
                  ]
                },
                { type: "button",
                  id: "startrecbutton",
                  text: "Start Recording",
                  _class: "margintop",
                  callback: function() { self.startRecording(); }
                },
                { type: "button",
                  id: "stoprecbutton",
                  text: "Stop Recording",
                  callback: function() { self.stopRecording(); }
                }
            ] // end of buttondiv spec
        };
        var elemlist = InterfaceMaker( buttons );
        this.gravcheck = elemlist[ "gravcheck" ];
        this.animcheck = elemlist[ "animcheck" ];
        this.angleinput = elemlist[ "angleinput" ];
        this.lefthit = elemlist[ "lefthit" ];
        this.righthit = elemlist[ "righthit" ];
        this.drag = elemlist[ "drag" ];
        this.collisiontype = elemlist[ "collisiontype" ];
        this.cart1bars = elemlist[ "cart1bars" ];
        this.cart1mass = elemlist[ "cart1mass" ];
        this.cart2bars = elemlist[ "cart2bars" ];
        this.cart2mass = elemlist[ "cart2mass" ];
        this.cart1v0 = elemlist[ "cart1v0" ];
        this.cart2v0 = elemlist[ "cart2v0" ];
        this.startrecbutton = elemlist[ "startrecbutton" ];
        this.stoprecbutton = elemlist[ "stoprecbutton" ];
        this.stoprecbutton.disabled = true;
        
        this.maindiv.appendChild( elemlist[ "buttondiv" ] );

        this.adjustBars();
    }
    else {
        var buttons = {
            type: "box",
            _class: "vbox",
            id: "buttondiv",
            spec: [
                { type: "box",
                  _class: "hbox",
                  id: "firstbuttons",
                  spec: [
                      { type: "box",
                        _class: "hbox bordered",
                        id: "collisiontypebox",
                        spec: [
                            { type: "select",                    
                              id: "collisiontype",
                              options: [ "Elastic", "Sticky" ],
                              selindex: 0,
                              changecallback: function() { self.setCollisionType(); }
                            },
                            { type: "span",
                              text: " collisions",
                              id: "collisionlabel"
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart1v0box",
                        _class: "hbox bordered",
                        spec: [
                            { type: "span",
                              id: "cart1v0label",
                              text: "v₁₀: "
                            },
                            { type: "input",
                              id: "cart1v0",
                              width: "6ex",
                              initval: "20",
                              _class: "grow",
                            },
                            { type: "span",
                              id: "cart1v0units",
                              text: " cm/s"
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart2v0box",
                        _class: "hbox bordered",
                        spec: [
                            { type: "span",
                              id: "cart2v0label",
                              text: "v₂₀: "
                            },
                            { type: "input",
                              id: "cart2v0",
                              width: "6ex",
                              initval: "0",
                              _class: "grow",
                            },
                            { type: "span",
                              id: "cart2v0units",
                              text: " cm/s"
                            }
                        ]
                      },
                      { type: "button",
                        id: "kickcartsbutton",
                        text: "Kick Carts",
                        callback: function() { self.kickCarts(); }
                      },
                      { type: "button",
                        id: "resetcartsbutton",
                        text: "Reset Carts",
                        callback: function() { self.resetCarts(); }
                      },
                  ]
                },
                { type: "box",
                  _class: "hbox bordered",
                  id: "secondbuttons",
                  spec: [
                      { type: "box",
                        id: "cart1barsbox",
                        _class: "hbox margintop",
                        spec: [
                            { type: "span",
                              id: "cart1barslabel",
                              text: "Cart 1 bars:"
                            },
                            { type: "numinput",
                              id: "cart1bars",
                              width: "4ex",
                              initval: 0,
                              min: 0,
                              max: 4,
                              step: 1,
                              changecallback: function() { self.adjustBars(); }
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart1massbox",
                        _class: "hbox bordered",
                        spec: [
                            { type: "span",
                              id: "cart1masslabel",
                              text: "m₁: "
                            },
                            { type: "input",
                              id: "cart1mass",
                              readonly: true,
                              width: "6ex",
                              _class: "grow"
                            },
                            { type: "span",
                              id: "cart1massunit",
                              text: "g"
                            }
                        ]
                      },


                
                      { type: "box",
                        id: "cart2barsbox",
                        _class: "hbox bordered",
                        spec: [
                            { type: "span",
                              id: "cart2barslabel",
                              text: "Cart 2 bars:"
                            },
                            { type: "numinput",
                              id: "cart2bars",
                              width: "4ex",
                              initval: 0,
                              min: 0,
                              max: 4,
                              step: 1,
                              changecallback: function() { self.adjustBars(); }
                            }
                        ]
                      },
                      { type: "box",
                        id: "cart2massbox",
                        _class: "hbox bordered",
                        spec: [
                            { type: "span",
                              id: "cart2masslabel",
                              text: "m₂: "
                            },
                            { type: "input",
                              id: "cart2mass",
                              readonly: true,
                              width: "6ex",
                              _class: "grow"
                            },
                            { type: "span",
                              id: "cart2massunit",
                              text: "g"
                            }
                        ]
                      },
                  ]
                }
            ]
        };
        var elemlist = InterfaceMaker( buttons );
        this.collisiontype = elemlist[ "collisiontype" ];
        this.cart1bars = elemlist[ "cart1bars" ];
        this.cart1mass = elemlist[ "cart1mass" ];
        this.cart2bars = elemlist[ "cart2bars" ];
        this.cart2mass = elemlist[ "cart2mass" ];
        this.cart1v0 = elemlist[ "cart1v0" ];
        this.cart2v0 = elemlist[ "cart2v0" ];
        
        document.body.appendChild( elemlist[ "buttondiv" ] );

        this.adjustBars();
    }
    // Two plots at bottom

    if ( this.showplots ) {
        var hbox = document.createElement( "div" );
        hbox.setAttribute( "class", "hbox" );
        document.body.appendChild( hbox );

        this.posplot = new SVGPlot.Plot( { divid: "posplotdiv",
                                           svgid: "posplot",
                                           title: "Cart Positions",
                                           xtitle: "t (s)",
                                           ytitle: "x (cm)",
                                           width: 800,
                                           height: 600
                                         } );
        this.velplot = new SVGPlot.Plot( { divid: "velplotdiv",
                                           svgid: "velplot",
                                           title: "Cart Velocities",
                                           xtitle: "t (s)",
                                           ytitle: "vx (cm/s)",
                                           width: 800,
                                           height: 600
                                         } );
        hbox.appendChild( this.posplot.topdiv );
        hbox.appendChild( this.velplot.topdiv );
        this.posplot.redraw();
        this.velplot.redraw();
    }
}
    
// **********************************************************************

TwoCartCollision.prototype.adjustBars = function()
{
    var bars1 = parseInt( this.cart1bars.value );
    if ( isNaN( bars1 ) ) {
        window.alert( "Error parsing cart 1 bars = \"" + this.cart1bars.value + "\" as an integer." );
        bars1 = 0;
    }
    var bars2 = parseInt( this.cart2bars.value );
    if ( isNaN( bars2 ) ) {
        window.alert( "Error parsing cart 2 bars = \"" + this.cart2bars.value + "\" as an integer." );
        bars2 = 0;
    }

    while ( this.cart1.bars.length > bars1 ) this.cart1.removeBar();
    while ( this.cart1.bars.length < bars1 ) this.cart1.addBar();
    while ( this.cart2.bars.length > bars2 ) this.cart2.removeBar();
    while ( this.cart2.bars.length < bars2 ) this.cart2.addBar();

    this.cart1mass.value = this.cart1.mass;
    this.cart2mass.value = this.cart2.mass;
}

// **********************************************************************

TwoCartCollision.prototype.resetCarts = function()
{
    if ( this.cart1.stukcparent == null )
        this.track.removeObject( this.cart1 );
    else
        this.cart1.stuckparent.removeObject( this.cart1 );
    if ( this.cart2.stuckparent == null )
        this.track.removeObject( this.cart2 );
    else
        this.cart2.stuckparent.removeObject( this.cart2 );
    this.cart1.mass = this.cart1.myorigmass;
    this.cart1.x0 = this.cart1.myorigx0;
    this.cart1.x1 = this.cart1.myorigx1;
    this.cart1.stuckparent = null;
    this.cart1.stuckcarts = [];
    this.cart2.mass = this.cart2.myorigmass;
    this.cart2.x0 = this.cart2.myorigx0;
    this.cart2.x1 = this.cart2.myorigx1;
    this.cart2.stuckparent = null;
    this.cart2.stuckcarts = [];

    this.track.addObject( this.cart1 );
    this.track.addObject( this.cart2 );
    this.cart1.x = -50;
    this.cart2.x = 50;
    this.cart1.vx = 0;
    this.cart2.vx = 0
    this.cart1.ax = 0;
    this.cart2.ax = 0;
}

// **********************************************************************

TwoCartCollision.prototype.kickCarts = function()
{
    var v10 = parseFloat( this.cart1v0.value );
    var v20 = parseFloat( this.cart2v0.value );
    if ( isNaN( v10 ) || isNaN( v20 ) ) {
        window.alert( "Error parsing initial v_x \"" + this.cart1v0.value + "\" and/or \""
                      + this.cart2v0.value + "\" as a number" );
        return;
    }
    this.cart1.vx = v10;
    this.cart2.vx = v20;
}

// **********************************************************************

TwoCartCollision.prototype.startRecording = function()
{
    if ( this.x1dataset != null ) {
        this.posplot.removeDatasetByIndex( 1 );
        this.posplot.removeDatasetByIndex( 0 );
        this.velplot.removeDatasetByIndex( 1 );
        this.velplot.removeDatasetByIndex( 0 );
        this.posplot.redraw();
        this.velplot.redraw();
    }
    this.x1dataset = new SVGPlot.Dataset( { color: "#cc0000" } );
    this.x2dataset = new SVGPlot.Dataset( { color: "#0000cc" } );
    this.posplot.addDataset( this.x1dataset );
    this.posplot.addDataset( this.x2dataset );
    this.vx1dataset = new SVGPlot.Dataset( { color: "#cc0000" } );
    this.vx2dataset = new SVGPlot.Dataset( { color: "#0000cc" } );
    this.velplot.addDataset( this.vx1dataset );
    this.velplot.addDataset( this.vx2dataset );
    this.t0 = this.scene.t;
    this.nextrecordt = this.recorddt;
    this.stoprecbutton.disabled = false;
    this.startrecbutton.disabled = true;
    this.scene.addAnimationCallback( this.recordcallback );
}

// **********************************************************************

TwoCartCollision.prototype.stopRecording = function()
{
    this.scene.removeAnimationCallback( this.recordcallback );
    this.startrecbutton.disabled = false;
    this.stoprecbutton.disabled = true;
    this.posplot.redraw();
    this.velplot.redraw();
}

// **********************************************************************

TwoCartCollision.prototype.recordData = function( t )
{
    var dt = t - this.t0;
    if ( dt >= this.nextrecordt ) {
        var x = this.cart1.x;
        var vx = this.cart1.vx;
        if ( this.cart1.stuckparent != null ) {
            x += this.cart1.stuckparent.x;
            vx = this.cart1.stucparent.vx;
        }
        this.x1dataset.addPoint( dt, x );
        this.vx1dataset.addPoint( dt, vx );

        x = this.cart2.x;
        vx = this.cart2.vx;
        if ( this.cart2.stuckparent != null ) {
            x += this.cart2.stuckparent.x;
            vx = this.cart2.stuckparent.vx;
        }
        this.x2dataset.addPoint( dt, x );
        this.vx2dataset.addPoint( dt, vx );

        this.nextrecordt += this.recorddt;
    }
}

// **********************************************************************

TwoCartCollision.prototype.setCollisionType = function()
{
    if ( this.collisiontype.value == "Sticky" )
        this.track.stickycarts = true;
    else
        this.track.stickycarts = false;
}

// **********************************************************************

export { TwoCartCollision };
