import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { PhysicsVector } from "./physicsvector.js";
import { SVGPlot } from "./svgplot.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var RocketMotion = function() {}

RocketMotion.Simulation = function( inparams )
{
    var self = this;
    
    var params = { "name": "Rocket Motion",
                   "campos": new THREE.Vector3( 0., 2., 15. ),
                   "aspect": 0.5,
                   "axeslen": 4,
                   "showaxes": true
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );
    
    this.plotdt = 0.25;

    this.rocket = new PhysicsObjects.SmallRocket( { loadfunction: function( obj ) { self.rocketLoaded(); } } );
    this.rocketloaded = false;
    this.interfacelaidout = false;
}

// **********************************************************************

RocketMotion.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( RocketMotion.Simulation.prototype, "constructor",
                       { value: RocketMotion.Simulation,
                         enumerable: false,
                         writeable: true } );

// **********************************************************************

RocketMotion.Simulation.prototype.rocketLoaded = function()
{
    this.scene.addObject( this.rocket )

    this.theta = 0;
    this.phi = 0;
    
    this.plume = new PhysicsObjects.RocketPlume( {} );
    this.plume.visobj.rotateZ( Math.PI/2. );
    this.plume.visobj.scale.set( 2., 1., 1. );
    this.plume.y = -3.4;

    this.vvectorscale = 1.;
    this.vvector = new PhysicsVector( { color: 0xffcc00,
                                        name: "Velocity",
                                        cylrad: 0.15,
                                        conelength: 0.4,
                                        conebaserad: 0.3,
                                      } );
    this.vvectorshown = false;

    this.thrustvectorscale = 0.01;
    this.thrustvector = new PhysicsVector( { color: 0xff4400,
                                             name: "Thrust",
                                             cylrad: 0.15,
                                             conelength: 0.4,
                                             conebaserad: 0.3
                                           } );
    this.thrustvectorshwon = false;
    
    this.rocketloaded = true;
    if ( this.interfacelaidout ) {
        this.setPosition();
        this.setVelocity();
    }
}

// **********************************************************************

RocketMotion.Simulation.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );

    var hbox, vbox, span, br, div, button, hr, b;
    var self = this;

    this.buttondiv = document.createElement( "div" );
    this.buttondiv.setAttribute( "class", "controlsdiv" );
    this.maindiv.appendChild( this.buttondiv );
    
    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Mass: " ) );
    this.massinput = document.createElement( "input" );
    hbox.appendChild( this.massinput );
    hbox.appendChild( document.createTextNode( " kg" ) );
    this.massinput.style[ "width" ] = "6ex";
    this.massinput.value = 1000;
    this.massinput.addEventListener( "change", function() {
        var mass = self.massinput.value;
        if ( mass < 10 ) mass = 10;
        if ( mass > 5000 ) mass = 5000;
        self.massinput.value = mass;
    } );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "θ: " ) );
    this.thetainput = document.createElement( "input" );
    this.thetainput.style[ "width" ] = "4ex";
    this.thetainput.value = 0;
    this.thetainput.addEventListener( "change", function() { self.setPosition() } );
    hbox.appendChild( this.thetainput );
    hbox.appendChild( document.createTextNode( "⁰  φ: " ) );
    this.phiinput = document.createElement( "input" );
    this.phiinput.style[ "width" ] = "4ex";
    this.phiinput.value = 0;
    this.phiinput.addEventListener( "change", function() { self.setPosition() } );
    hbox.appendChild( this.phiinput );
    hbox.appendChild( document.createTextNode( "⁰" ) );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "pos: " ) );
    this.x0input = document.createElement( "input" );
    hbox.appendChild( this.x0input );
    this.x0input.style[ "width" ] = "5ex"
    this.x0input.value = "-7.0";
    this.x0input.addEventListener( "change", function() { self.setPosition() } );
    this.y0input = document.createElement( "input" );
    hbox.appendChild( this.y0input );
    this.y0input.style[ "width" ] = "5ex"
    this.y0input.value = "-3.0";
    this.y0input.addEventListener( "change", function() { self.setPosition() } );
    this.z0input = document.createElement( "input" );
    hbox.appendChild( this.z0input );
    this.z0input.style[ "width" ] = "5ex"
    this.z0input.value = "0";
    this.z0input.addEventListener( "change", function() { self.setPosition() } );
    hbox.appendChild( document.createTextNode( " m" ) );
    
    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "vel: " ) );
    this.v0xinput = document.createElement( "input" );
    hbox.appendChild( this.v0xinput );
    this.v0xinput.style[ "width" ] = "5ex"
    this.v0xinput.value = "4.0";
    this.v0xinput.addEventListener( "change", function() { self.setVelocity() } );
    this.v0yinput = document.createElement( "input" );
    hbox.appendChild( this.v0yinput );
    this.v0yinput.style[ "width" ] = "5ex"
    this.v0yinput.value = "0";
    this.v0yinput.addEventListener( "change", function() { self.setVelocity() } );
    this.v0zinput = document.createElement( "input" );
    hbox.appendChild( this.v0zinput );
    this.v0zinput.style[ "width" ] = "5ex"
    this.v0zinput.value = "0";
    this.v0zinput.addEventListener( "change", function() { self.setVelocity() } );
    hbox.appendChild( document.createTextNode( " m/s" ) );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Thrust: " ) );
    this.thrustinput = document.createElement( "input" );
    hbox.appendChild( this.thrustinput );
    hbox.appendChild( document.createTextNode( " N" ) );
    this.thrustinput.style[ "width" ] = "6ex";
    this.thrustinput.value = 500;
    this.thrustinput.addEventListener( "change", function() { self.setThrustVector() } );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "tf: " ) );
    this.tfinput = document.createElement( "input" );
    hbox.appendChild( this.tfinput );
    hbox.appendChild( document.createTextNode( " s" ) );
    this.tfinput.style[ "width" ] = "6ex"
    this.tfinput.value = 5;
    this.tfinput.addEventListener( "change", function() {
        var tf = self.tfinput.value;
        if ( tf < 0.1) tf = 0.1;
        if ( tf > 30 ) tf = 30;
        self.tfinput.value = tf;
    } );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Show " ) );
    this.showvel = document.createElement( "input" );
    this.showvel.setAttribute( "type", "checkbox" );
    this.showvel.checked = false;
    this.showvel.addEventListener( "change", function() { self.setVVector() } );
    hbox.appendChild( this.showvel );
    b = document.createElement( "b" );
    b.appendChild( document.createTextNode( "v  " ) );
    hbox.appendChild( b );
    this.showthrust = document.createElement( "input" );
    this.showthrust.setAttribute( "type", "checkbox" );
    this.showthrust.checked = false;
    this.showthrust.addEventListener( "change", function() { self.setThrustVector() } );
    hbox.appendChild( this.showthrust );
    hbox.appendChild( document.createTextNode( "thrust  " ) );
    
    this.gobutton = document.createElement( "button" );
    this.gobutton.appendChild( document.createTextNode( "Go!" ) );
    this.buttondiv.appendChild( this.gobutton );
    this.gobutton.addEventListener( "click", function() { self.launchRocket() } );

    // ****************************************
    // Plots

    var plotbox = document.createElement( "div" );
    document.body.appendChild( plotbox );
    plotbox.setAttribute( "class", "hbox bordered" );

    var baseplotparams = { "titlesize": 40,
                           "axistitlesize": 40,
                           "axislabelsize": 32,
                           "ticklen": 15,
                           "subticklen": 10,
                         };
    var plotparams;

    this.xplot = new SVGPlot.Plot( { ...baseplotparams,
                                     "divid": "xplotdiv",
                                     "svgid": "xplot",
                                     "title": "Rocket x",
                                     "xtitle": "t (s)",
                                     "ytitle": "x (m)"
                                   } );
    plotbox.appendChild( this.xplot.topdiv );

    this.yplot = new SVGPlot.Plot( { ...baseplotparams,
                                     "divid": "yplotdiv",
                                     "svgid": "yplot",
                                     "title": "Rocket y",
                                     "xtitle": "t (s)",
                                     "ytitle": "y (m)"
                                   } );
    plotbox.appendChild( this.yplot.topdiv );

    this.zplot = new SVGPlot.Plot( { ...baseplotparams,
                                     "divid": "zplotdiv",
                                     "svgid": "zplot",
                                     "title": "Rocket z",
                                     "xtitle": "t (s)",
                                     "ytitle": "z (m)"
                                   } );
    plotbox.appendChild( this.zplot.topdiv );

    var plotbox = document.createElement( "div" );
    document.body.appendChild( plotbox );
    plotbox.setAttribute( "class", "hbox bordered" );

    this.vxplot = new SVGPlot.Plot( { ...baseplotparams,
                                      "divid": "vxplotdiv",
                                      "svgid": "vxplot",
                                      "title": "Rocket vx",
                                      "xtitle": "t (s)",
                                      "ytitle": "vx (m/s)"
                                    } );
    plotbox.appendChild( this.vxplot.topdiv );

    this.vyplot = new SVGPlot.Plot( { ...baseplotparams,
                                      "divid": "vyplotdiv",
                                      "svgid": "vyplot",
                                      "title": "Rocket vy",
                                      "xtitle": "t (s)",
                                      "ytitle": "vy (m/s)"
                                    } );
    plotbox.appendChild( this.vyplot.topdiv );

    this.vzplot = new SVGPlot.Plot( { ...baseplotparams,
                                      "divid": "vzplotdiv",
                                      "svgid": "vzplot",
                                      "title": "Rocket vz",
                                      "xtitle": "t (s)",
                                      "ytitle": "vz (m/s)"
                                    } );
    plotbox.appendChild( this.vzplot.topdiv );

    this.xplot.redraw();
    this.yplot.redraw();
    this.zplot.redraw();
    this.vxplot.redraw();
    this.vyplot.redraw();
    this.vzplot.redraw();

    // ****************************************
    
    this.ghostinputs = [ this.massinput, this.x0input, this.y0input, this.z0input,
                         this.v0xinput, this.v0yinput, this.v0zinput, this.thrustinput,
                         this.tfinput, this.gobutton ];

    this.interfacelaidout = true;
    if ( this.rocketloaded ) {
        this.setPosition();
        this.setVelocity();
    }
}

// **********************************************************************

RocketMotion.Simulation.prototype.setPosition = function()
{
    var x = parseFloat( this.x0input.value );
    if ( isNaN( x ) )
        window.alert( "Invalid x0; please make it a number." );
    else
        this.rocket.x = x;
    var y = parseFloat( this.y0input.value );
    if ( isNaN( y ) )
        window.alert( "Invalid y0; please make it a number." );
    else
        this.rocket.y = y;
    var z = parseFloat( this.z0input.value );
    if ( isNaN( z ) )
        window.alert( "Invalid z0; please make it a number." );
    else
        this.rocket.z = z;

    this.theta = parseFloat( this.thetainput.value );
    this.phi = parseFloat( this.phiinput.value );
    if ( isNaN( this.theta ) || isNaN( this.phi ) )
        window.alert( "Invalid θ and θ; please make them numbers" );
    else {
        if ( this.theta < 0 ) this.theta = 0;
        if ( this.theta > 180 ) this.theta = 180;
        if ( this.phi < 0 ) this.phi = 0;
        if ( this.phi > 360 ) this.phi = 360;
        this.thetainput.value = this.theta;
        this.phiinput.value = this.phi;
        this.theta *= Math.PI / 180.;
        this.phi *= Math.PI / 180.;
        this.rocket.visobj.setRotationFromEuler( new THREE.Euler( 0., this.phi, -this.theta, "XYZ" ) );
    }

    this.setVVector();
    this.moveThrustVector();
}

// **********************************************************************

RocketMotion.Simulation.prototype.setVelocity = function()
{
    var vx = parseFloat( this.v0xinput.value );
    if ( isNaN( vx ) )
        window.alert( "Invalid v0x; please make it a number." );
    else
        this.rocket.vx = vx;
    var vy = parseFloat( this.v0yinput.value );
    if ( isNaN( vy ) )
        window.alert( "Invalid v0y; please make it a number." );
    else
        this.rocket.vy = vy;
    var vz = parseFloat( this.v0zinput.value );
    if ( isNaN( vz ) )
        window.alert( "Invalid v0z; please make it a number." );
    else
        this.rocket.vz = vz;

    this.setVVector();
}

// **********************************************************************

RocketMotion.Simulation.prototype.setVVector = function()
{
    const l = this.rocket.nosey;

    this.vvector.x = this.rocket.x + l * Math.sin( this.theta ) * Math.cos( this.phi );
    this.vvector.y = this.rocket.y + l * Math.cos( this.theta );
    this.vvector.z = this.rocket.z + l * Math.sin( this.theta ) * ( -Math.sin( this.phi ) );
    
    this.vvector.xcomp = this.rocket.vx * this.vvectorscale;
    this.vvector.ycomp = this.rocket.vy * this.vvectorscale;
    this.vvector.zcomp = this.rocket.vz * this.vvectorscale;

    if ( this.vvectorshown && ( ! this.showvel.checked ) ) {
        this.scene.removeObject( this.vvector );
        this.vvectorshown = false;
    }
    else if ( this.showvel.checked && ( ! this.vvectorshown ) ) {
        this.scene.addObject( this.vvector );
        this.vvectorshown = true;
    }
}

// **********************************************************************

RocketMotion.Simulation.prototype.moveThrustVector = function()
{
    const l = this.rocket.nosey;

    this.thrustvector.x = this.rocket.x + l * Math.sin( this.theta ) * Math.cos( this.phi );
    this.thrustvector.y = this.rocket.y + l * Math.cos( this.theta );
    this.thrustvector.z = this.rocket.z + l * Math.sin( this.theta ) * ( -Math.sin( this.phi ) );
}

// **********************************************************************

RocketMotion.Simulation.prototype.setThrustVector = function()
{
    this.moveThrustVector();
    
    var thrust = this.thrustinput.value;
    if ( thrust < 0 ) thrust = 0;
    if ( thrust > 5000 ) thrust = 5000;
    this.thrustinput.value = thrust;

    this.thrustvector.xcomp = this.thrustvectorscale * thrust * Math.sin( this.theta ) * Math.cos( this.phi );
    this.thrustvector.ycomp = this.thrustvectorscale * thrust * Math.cos( this.theta );
    this.thrustvector.zcomp = this.thrustvectorscale * thrust * Math.sin( this.theta ) * (-Math.sin( this.phi ));

    if ( this.thrustvectorshown & ( ! this.showthrust.checked ) ) {
        this.scene.removeObject( this.thrustvector );
        this.thrustvectorshown = false;
    }
    else if ( this.showthrust.checked && ( ! this.thrustvectorshwon ) ) {
        this.scene.addObject( this.thrustvector );
        this.thrustvectorshown = true;
    }
}

// **********************************************************************

RocketMotion.Simulation.prototype.launchRocket = function()
{
    var self = this;

    if ( ! this.rocketloaded ) {
        window.alert( "Rocket isn't loaded yet, can't launch." );
        return;
    }
    
    this.setPosition();
    this.setVelocity();
    var tf = parseFloat( this.tfinput.value );
    if ( isNaN( tf ) ) {
        window.aert( "Final time must be a number!  Not launching." );
        return;
    }
    this.tf = tf;
    var mass = parseFloat( this.massinput.value );
    if ( isNaN( mass ) ) {
        window.alert( "Mass must be a number!  Not launching." );
        return;
    }
    this.rocket.mass = mass;
    var thrust = parseFloat( this.thrustinput.value );
    if ( isNaN( thrust ) ) {
        window.alert( "Thrust must be a number!  Not launching." );
        return;
    }
    var theta = parseFloat( this.thetainput.value );
    var phi = parseFloat( this.phiinput.value );
    if ( isNaN( theta ) || isNaN( phi ) ) {
        window.alert( "Not launching due to invalid θ and/or φ." );
        return;
    }
    theta *= Math.PI / 180.;
    phi *= Math.PI / 180.;
    var xthrust = thrust / self.rocket.mass * Math.sin( theta ) * Math.cos( phi );
    var ythrust = thrust / self.rocket.mass * Math.cos( theta );
    var zthrust = thrust / self.rocket.mass * Math.sin( theta ) * ( -Math.sin( phi ) );
    this.rocket.accelfunc = function() { return { x: xthrust, y: ythrust, z: zthrust } };
    this.setThrustVector();
        
    for ( var ghost of this.ghostinputs ) ghost.disabled = true

    this.nextplot = this.plotdt;
    this.xplot.clear( false );
    this.xdata = new SVGPlot.Dataset( { markersize: 24 } );
    this.xplot.addDataset( this.xdata );
    this.xdata.addPoint( 0, this.rocket.x );
    this.yplot.clear( false );
    this.ydata = new SVGPlot.Dataset( { markersize: 24 } );
    this.yplot.addDataset( this.ydata );
    this.ydata.addPoint( 0, this.rocket.y );
    this.zplot.clear( false );
    this.zdata = new SVGPlot.Dataset( { markersize: 24 } );
    this.zplot.addDataset( this.zdata );
    this.zdata.addPoint( 0, this.rocket.z );
    this.vxplot.clear( false );
    this.vxdata = new SVGPlot.Dataset( { markersize: 24 } );
    this.vxplot.addDataset( this.vxdata );
    this.vxdata.addPoint( 0, this.rocket.vx );
    this.vyplot.clear( false );
    this.vydata = new SVGPlot.Dataset( { markersize: 24 } );
    this.vyplot.addDataset( this.vydata );
    this.vydata.addPoint( 0, this.rocket.vy );
    this.vzplot.clear( false );
    this.vzdata = new SVGPlot.Dataset( { markersize: 24 } );
    this.vzplot.addDataset( this.vzdata );
    this.vzdata.addPoint( 0, this.rocket.vz );

    // Give it a short sec delay to make sure redrawing is done and such
    // Empirically, I was seeing that there was a lurch at the first
    // animation frame as it was taking a fraction of a second to
    // actually get to the first update callback
    window.setTimeout( function() { self.actuallyLaunchRocket( thrust ) }, 0 );
}

RocketMotion.Simulation.prototype.actuallyLaunchRocket = function( thrust )
{
    var self = this;
    this.rocket.t = this.scene.t;
    this.t0 = this.rocket.t;
    this.animationcallback = function( t ) { self.moveRocket( t ); };
    this.rocket.addObject( this.plume );
    this.plume.visobj.scale.x = thrust / 500;
    this.plume.animate();
    this.scene.addAnimationCallback( this.animationcallback );
}

// **********************************************************************

RocketMotion.Simulation.prototype.moveRocket = function( t )
{
    this.rocket.updateState( t );
    this.setVVector();
    this.moveThrustVector();
    this.x0input.value = this.rocket.x.toFixed( 2 );
    this.y0input.value = this.rocket.y.toFixed( 2 );
    this.z0input.value = this.rocket.z.toFixed( 2 );
    this.v0xinput.value = this.rocket.vx.toFixed( 2 );
    this.v0yinput.value = this.rocket.vy.toFixed( 2 );
    this.v0zinput.value = this.rocket.vz.toFixed( 2 );

    if ( t - this.t0 >= this.nextplot ) {
        this.nextplot += this.plotdt;
        this.xdata.addPoint( t - this.t0, this.rocket.x );
        this.ydata.addPoint( t - this.t0, this.rocket.y );
        this.zdata.addPoint( t - this.t0, this.rocket.z );
        this.vxdata.addPoint( t - this.t0, this.rocket.vx );
        this.vydata.addPoint( t - this.t0, this.rocket.vy );
        this.vzdata.addPoint( t - this.t0, this.rocket.vz );
    }
    
    if ( t - this.t0 >= this.tf ) {
        this.scene.removeAnimationCallback( this.animationcallback );
        for ( var ghost of this.ghostinputs ) ghost.disabled = false;
        this.rocket.removeObject( this.plume );
        this.plume.stopAnimate();
        this.xplot.redraw();
        this.yplot.redraw();
        this.zplot.redraw();
        this.vxplot.redraw();
        this.vyplot.redraw();
        this.vzplot.redraw();
    }
}


// **********************************************************************

export { RocketMotion };
