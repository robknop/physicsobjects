import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { StandardScene } from "./standardscene.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { SVGPlot } from "./svgplot.js";

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var Springction = function( inparams )
{
    var self = this;
    var geom, mat, mesh;

    this.params = { name: "Horizontal Spring with Friction",
                    nviews: 1,
                    aspect: 0.5,
                    campos: new THREE.Vector3( 0., 2.7, 5.2 ),
                    camfov: 60.,
                    camlook: new THREE.Vector3( 0., 1.4, 0. ),
                    equilspringl: 4,
                    springk: 20,
                    wallthick: 0.1,
                    wallwid: 2.0,
                    wallh: 2.0,
                    floorl: 10,
                    springrad: 0.4,
                    springrots: 5,
                    springcircrad: 0.1,
                    boxw: 1,
                    boxmass: 1,
                    dx0: -1,
                    mu_s: 0.6,
                    mu_k: 0.3,
                    dumpcameraoption: true
                  };
    Object.assign( this.params, inparams );
    StandardScene.call( this, this.params );

    this.springk = this.params.springk;
    this.boxmass = this.params.boxmass;
    this.mu_s = this.params.mu_s;
    this.mu_k = this.params.mu_k;
    this.animcallback = function( t ) { self.update( t ); }
    this.g = 9.8;
    this.tinyvel = 1e-3;
    this.tinyacc = 1e-3;
    
    this.ordinal = 0;
    this.colordex = 0;
    this.plotdt = 0.1;
    this.xdatasets = [];
    this.vdatasets = [];
    this.listhandles = [];
    this.curxdataset = null;
    this.curvdataset = null;

    const springleft = 0 - this.params.equilspringl - this.params.boxw/2;

    this.spring = new PhysicsObjects.Spring( { length: this.params.equilspringl,
                                               rad: this.params.springrad,
                                               circrad: this.params.springcircrad,
                                               nrots: this.params.springrots,
                                             } );
    // My rx, ry, rz things are a disaster
    this.spring.position = { x: springleft, y: this.params.boxw/2 - this.params.springrad, z: 0 };
    this.scene.addObject( this.spring );
    
    this.box = new PhysicsObjects.Block( { rx: this.params.boxw/2,
                                           ry: this.params.boxw/2,
                                           rz: this.params.boxw/2,
                                           name: "Mass m",
                                           color: 0x202080,
                                           accelfunc: function( t, pos, vel, obj, dt ) {
                                               return self.boxaccel( t, pos, vel, obj, dt );
                                           },
                                         } );
    this.scene.addObject( this.box );

    // Floor
    geom = new THREE.BoxGeometry( this.params.floorl, this.params.wallthick, this.params.wallh );
    mat = new THREE.MeshLambertMaterial( { color: 0x602000 } );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( -this.params.equilspringl - this.params.boxw/2 + this.params.floorl/2,
                       -this.params.boxw/2 - this.params.wallthick/2, 0. );
    this.scene.scene.add( mesh );

    // Wall
    geom = new THREE.BoxGeometry( this.params.wallthick, this.params.wallh, this.params.wallwid );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( -this.params.equilspringl - this.params.boxw/2 - this.params.wallthick/2,
                       -this.params.boxw/2 + this.params.wallh/2, 0 );
    this.scene.scene.add( mesh );
}

Springction.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( Springction.prototype, "constructor",
                       {  value: Springction,
                          enumerable: false,
                          writeable: true } );

// **********************************************************************

Springction.colors = [ "#cc0000",
                       "#007000",
                       "#0000cc",
                       "#cc9900",
                       "#33cc00",
                       "#cc00cc" ];

Springction.colornames = [ "Red", "Green", "Blue", "Gold", "Lime", "Purple" ];

// **********************************************************************

Springction.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );

    var self = this;
    
    var controls = {
        type: "box",
        id: "controlsdiv",
        _class: "controlsdiv",
        spec: [
            { type: "box",
              id: "springkbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "springklabel",
                    text: "k: ",
                  },
                  { type: "input",
                    id: "springk",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.springk,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "springkumnits",
                    text: " N/m",
                  },
              ]
            },
            { type: "box",
              id: "massbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "masslabel",
                    text: "m: ",
                  },
                  { type: "input",
                    id: "mass",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.boxmass,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "massunits",
                    text: " kg",
                  },
              ]
            },
            { type: "box",
              id: "mu_sbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "mu_slabel",
                    text: "μ_s: ",
                  },
                  { type: "input",
                    id: "mu_s",
                    width: "6ex",
                    _class: "grow",
                    initval: this.mu_s,
                    changecallback: function() { self.initialize(); }
                  }
              ]
            },
            { type: "box",
              id: "mu_kbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "mu_klabel",
                    text: "μ_k: ",
                  },
                  { type: "input",
                    id: "mu_k",
                    width: "6ex",
                    _class: "grow",
                    initval: this.mu_k,
                    changecallback: function() { self.initialize(); }
                  }
              ]
            },
            { type: "box",
              id: "initxbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "initxlabel",
                    text: "x₀: ",
                  },
                  { type: "input",
                    id: "initx",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.dx0,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "x0units",
                    text: " m",
                  },
              ]
            },
            { type: "box",
              id: "resetbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "resetbutton",
                    _class: "grow",
                    text: "Reset",
                    callback: function() { self.initialize(); }
                  }
              ]
            },
            { type: "box",
              id: "gobox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "gobutton",
                    _class: "grow",
                    text: "Go!",
                    callback: function() { self.go(); }
                  }
              ]
            },
            { type: "box",
              id: "stopbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "stopbutton",
                    _class: "grow",
                    text: "Stop!",
                    callback: function() { self.stop(); }
                  }
              ]
            },
            { type: "span",
              id: "spacer0",
              text: " "
            },
            { type: "listwidget",
              id: "datasetlist",
              multiselect: true
            },
            { type: "box",
              id: "deletesetsbuttonbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "deletesetsbutton",
                    _class: "grow",
                    text: "Delete Sets",
                    callback: function() { self.deleteSelectedSets(); }
                  }
              ]
            }
        ]
    };
    this.controls = InterfaceMaker( controls );
    this.setlist = this.controls[ "datasetlist" ];
    
    this.disablewhilego = [];
    this.disablewhilestop = [];
    for ( let it of [ "springk", "mass", "initx", "mu_s", "mu_k", "resetbutton", "gobutton", "deletesetsbutton" ] ) {
        this.disablewhilego.push( this.controls[ it ] );
    }
    for ( let it of [ "stopbutton" ] ) {
        this.disablewhilestop.push( this.controls[ it ] );
        this.controls[ it ].disabled = true;
    }
        
    this.maindiv.appendChild( this.controls[ "controlsdiv" ] );
    this.initialize();


    // Plots

    var plotwid = 800;
    var plothei = 600;

    var hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    document.body.appendChild( hbox );

    this.xplot = new SVGPlot.Plot( { divid: "xplotdiv",
                                     svgid: "yplotsvg",
                                     ytitle: "x (m)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   }
                                 );
    this.vplot = new SVGPlot.Plot( { divid: "vplotdiv",
                                     svgid: "vyplotdiv",
                                     ytitle: "v_x (m/s)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   }
                                 );
    hbox.appendChild( this.xplot.topdiv );
    hbox.appendChild( this.vplot.topdiv );
    this.xplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

Springction.prototype.go = function()
{
    for ( let widget of this.disablewhilego )
        widget.disabled = true;
    for ( let widget of this.disablewhilestop )
        widget.disabled = false;
    this.box.initializeState( this.scene.t, this.box.position, this.box.velocity, this.box.acceleration );
    this.scene.addAnimationCallback( this.animcallback );

    this.stuck = false;
    this.t0 = this.scene.t;
    this.ordinal += 1;
    this.curxdataset = new SVGPlot.Dataset( { name: "Set " + this.ordinal + "(" +
                                                     Springction.colornames[ this.colordex ] + ")",
                                              color: Springction.colors[ this.colordex ],
                                            } );
    this.xplot.addDataset( this.curxdataset );
    this.xdatasets.push( this.curxdataset );
    this.curvdataset = new SVGPlot.Dataset( { name: "Set " + this.ordinal + "(" +
                                                     Springction.colornames[ this.colordex ] + ")",
                                              color: Springction.colors[ this.colordex ],
                                            } );
    this.vplot.addDataset( this.curvdataset );
    this.vdatasets.push( this.curvdataset );
    this.nextplot = -1e30;
    this.colordex += 1;
    if ( this.colordex >= Springction.colors.length ) this.colordex = 0;
    var handle = this.setlist.addItem( this.curxdataset.name );
    this.listhandles.push( handle );
}

// **********************************************************************

Springction.prototype.stop = function()
{
    for ( let widget of this.disablewhilego )
        widget.disabled = false;
    for ( let widget of this.disablewhilestop )
        widget.disabled = true;
    this.scene.removeAnimationCallback( this.animcallback );
    this.xplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

Springction.prototype.boxaccel = function( t, pos, vel, obj, dt ) {
    var acc = { x: 0, y: 0, z : 0 };
    var fspx = -this.springk * pos.x;
    if ( Math.abs( vel.x ) < this.tinyvel ) {
        if ( this.mu_s * this.boxmass * this.g >= Math.abs( fspx ) ) {
            acc.x = -vel.x / dt;
            console.log( "Stuck with fspx = " + fspx + " and |μ_s * m * g| = " +
                         this.mu_s * this.boxmass * this.g );
            this.stuck = true;
        }
    }

    if ( ! this.stuck ) {
        var fkf = this.boxmass * this.g * this.mu_k;
        if ( vel.x > 0 ) fkf *= -1;
        acc.x = ( fspx + fkf ) / this.boxmass;
    }
    return acc;
}

// **********************************************************************

Springction.prototype.update = function( t ) {
    this.box.updateState( t );
    this.updateSpring();
    if ( t > this.nextplot ) {
        this.curxdataset.addPoint( t - this.t0, this.box.x );
        this.curvdataset.addPoint( t - this.t0, this.box.vx );
        this.nextplot = t + this.plotdt;
    }
    if ( this.stuck ) this.stop();
}

// **********************************************************************

Springction.prototype.updateSpring = function() {
    this.spring.length = this.params.equilspringl + this.box.x;
}

// **********************************************************************

Springction.prototype.initialize = function() {
    var txt, val;

    var controls = [ "springk", "mass", "mu_k", "mu_s", "initx" ];
    var properties = [ "springk", "boxmass", "mu_k", "mu_s", "initx" ];
    var desc = [ "spring constant", "box mass", "μ_k", "μ_s", "x₀" ];
    var limits = [ [0.1, 1000], [0.1, 100], [0., 1.], [0., 1.], [-0.75*this.params.equilspringl,
                                                                 0.75*this.params.equilspringl ] ];

    for ( let i = 0 ; i < controls.length ; ++i ) {
        txt = this.controls[ controls[i] ].value;
        val = parseFloat( txt );
        if ( isNaN( val ) ) {
            window.alert( "Error parsing " + desc[i] + " \"" + txt + "\" as a number" );
        }
        else {
            if ( val < limits[i][0] ) val = limits[i][0];
            if ( val > limits[i][1] ) val = limits[i][1];
            this[ properties[i] ] = val;
            this.controls[ controls[i] ].value = val;
        }
    }

    if ( this.mu_s < this.mu_k ) {
        this.mu_s = this.mu_k;
        this.controls[ "mu_s" ].value = this.mu_s
    }
    this.box.x = this.initx;
    this.box.vx = 0;
    this.box.ax = 0;
    this.updateSpring();
}

// **********************************************************************

Springction.prototype.deleteSelectedSets = function() {
    var sel = this.setlist.getSelectedHandles();
    for ( let del of sel ) {
        let dex = this.listhandles.indexOf( del );
        if ( dex >= 0 ) {
            this.xplot.removeDatasetByIndex( dex );
            this.vplot.removeDatasetByIndex( dex );
            this.listhandles.splice( dex, 1 );
            this.xdatasets.splice( dex, 1 );
            this.vdatasets.splice( dex, 1 );
            this.setlist.removeItem( del );
        }
    }
    this.xplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

export { Springction };
