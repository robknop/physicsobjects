import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { ListWidget } from "./listwidget.js";
import { Cannon, CannonBall, Defilade } from "./cannon.js";
import { ProjectilePlot } from "./projectileplot.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var Projectile = function() {}

Projectile.Simulation = function( inparams )
{
    var self = this;
    
    var params = { "name": "Cannon Test",
                   "campos": new THREE.Vector3( 0., 2., 10. ),
                   "aspect": 0.5,
                   "gridbg": false,
                   "bgcolor": 0xccccff,
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );
    
    this.cannon = new Cannon( { "loadfunction": function() { self.cannonLoaded(); } } );
    this.ground = new Defilade( this.cannon, { "loadfunction": function() { self.groundLoaded(); } } );

    this.cannon.altitude = 0;
    this.cannon.azimuth = 0;

    this.ballcount = 0;
    this.cannonballs = {};
    this.datalisteners = [];
    
    this.ballmovelistener = function( ball ) { self.updateBallPos( ball ); };

    this.plot = null;
}

// **********************************************************************

Projectile.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( Projectile.Simulation.prototype, "constructor",
                       { value: Projectile.Simulation,
                         enumerable: false,
                         writeable: true } );

// **********************************************************************

Projectile.Simulation.prototype.cannonLoaded = function()
{
    console.log( "Adding cannon to scene." );
    this.scene.addObject( this.cannon );
}

Projectile.Simulation.prototype.groundLoaded = function()
{
    console.log( "Adding ground to scene." );
    this.scene.addObject( this.ground );
    this.ground.setCannonAngle( true );
}

// **********************************************************************

Projectile.Simulation.prototype.addDataListener = function( listener )
{
    var dex = this.datalisteners.indexOf( listener );
    if ( dex < 0 )
        this.datalisteners.push( listener );
}

// **********************************************************************

Projectile.Simulation.prototype.removeDataListener = function( listener )
{
    var dex = this.datalisteners.indexOf( listener );
    if ( dex >= 0 )
        this.datalisteners.splice( dex, 1 );
}

// **********************************************************************

Projectile.Simulation.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );

    var hbox, vbox, span, br, div, button, hr;
    var self = this;

    this.buttondiv = document.createElement( "div" );
    this.buttondiv.setAttribute( "class", "controlsdiv" );
    this.maindiv.appendChild( this.buttondiv );
    
    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Alt.:" ) );
    this.altinput = document.createElement( "input" );
    hbox.appendChild( this.altinput );
    hbox.appendChild( document.createTextNode( "⁰ (0⁰-90⁰)" ) );
    // I'm fucking up my flexbox somewhere since the number inputs overlap each other
    // this.altinput.setAttribute( "type", "number" );
    // this.altinput.setAttribute( "min", 0 );
    // this.altinput.setAttribute( "max", 90 );
    this.altinput.style[ "width" ] = "4ex";
    this.altinput.value = ( this.cannon.altitude * 180. / Math.PI  ).toFixed( 0 );
    this.altinput.addEventListener( "change", function() {
        var alt = self.altinput.value;
        if ( alt < 0 ) alt = 0;
        if ( alt > 90 ) alt = 90;
        self.altinput.value = alt;
        self.cannon.altitude = alt * Math.PI / 180.;
        if ( self.defiladecheckbox.checked )
            self.ground.setCannonAngle( );
    } );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Az.:" ) );
    this.azinput = document.createElement( "input" );
    hbox.appendChild( this.azinput );
    hbox.appendChild( document.createTextNode( "⁰ (0⁰-360⁰)" ) );
    // this.azinput.setAttribute( "type", "number" );
    // this.azinput.setAttribute( "min", 0 );
    // this.azinput.setAttribute( "max", 359 );
    this.azinput.style[ "width" ] = "4ex";
    this.azinput.value = ( this.cannon.azimuth * 180. / Math.PI  ).toFixed( 0 );
    this.azinput.addEventListener( "change", function() {
        var az = self.azinput.value;
        if ( az < 0 ) az = 0
        if ( az > 360 ) az = 360;
        self.azinput.value = az;
        self.cannon.azimuth = az * Math.PI / 180.;
        if ( self.defiladecheckbox.checked )
            self.ground.setCannonAngle( );
    } );

    hbox = document.createElement( "div" );
    this.buttondiv.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "Muzzle sp.: " ) );
    this.muzvelinput = document.createElement( "input" );
    // this.muzvelinput.setAttribute( "type", "number" );
    // this.muzvelinput.setAttribute( "min", 1 );
    // this.muzvelinput.setAttribute( "max", 30 );
    this.muzvelinput.style[ "width" ] = "4ex";
    this.muzvelinput.value = 20;
    hbox.appendChild( this.muzvelinput );
    hbox.appendChild( document.createTextNode( "\u00A0m/s" ) );
    this.muzvelinput.addEventListener( "change", function() {
        var val = self.muzvelinput.value;
        if ( val < 1 ) val = 1;
        if ( val > 30 ) val = 30;
        self.muzvelinput.value = val;
    } );
                      
    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox" );
    this.buttondiv.appendChild( hbox );
    this.defiladecheckbox = document.createElement( "input" );
    this.defiladecheckbox.setAttribute( "type", "checkbox" );
    hbox.appendChild( this.defiladecheckbox );
    hbox.appendChild( document.createTextNode( "launch @ origin" ) );
    this.defiladecheckbox.addEventListener( "change", function() {
        if ( self.defiladecheckbox.checked )
            self.ground.setCannonAngle( );
        else
            self.ground.setCannonAngle( true );
    } );

    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox" );
    this.buttondiv.appendChild( hbox );
    this.trailcheckbox = document.createElement( "input" );
    this.trailcheckbox.setAttribute( "type", "checkbox" );
    this.trailcheckbox.checked = true;
    hbox.appendChild( this.trailcheckbox );
    hbox.appendChild( document.createTextNode( "trails" ) );

    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Launch" ) );
    button.setAttribute( "class", "marginblock" );
    this.buttondiv.appendChild( button );
    button.addEventListener( "click", function() { self.launch(); } );

    hr = document.createElement( "hr" );
    hr.style.width = "100%";
    this.buttondiv.appendChild( hr );

    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox" );
    this.buttondiv.appendChild( hbox );
    this.highlightselected = document.createElement( "input" );
    this.highlightselected.setAttribute( "type", "checkbox" );
    hbox.appendChild( this.highlightselected );
    hbox.appendChild( document.createTextNode( "highlight selected" ) );
    this.highlightselected.addEventListener( "change", function() { self.ballSelected(); } );
    
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "View Selected" ) );
    button.setAttribute( "class", "marginblock" );
    this.buttondiv.appendChild( button );
    button.addEventListener( "click", function() { self.viewSelectedBall(); } );

    this.listofballs = new ListWidget( { multiselect: true } );
    this.buttondiv.appendChild( this.listofballs.div );
    this.listofballs.div.addEventListener( "selectionchanged", function() { self.ballSelected(); } );

    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Remove Selected" ) );
    button.setAttribute( "class", "marginblock" );
    this.buttondiv.appendChild( button );
    button.addEventListener( "click", function() { self.removeSelectedBall(); } );
    
    // Info at bottom

    var buttonbox = document.createElement( "div" );
    buttonbox.setAttribute( "class", "hbox bordered" );
    this.leftbigbox.appendChild( buttonbox );

    vbox = document.createElement( "div" );
    vbox.setAttribute( "class", "vbox bordered" );
    buttonbox.appendChild( vbox );
    hbox = document.createElement( "div" );
    vbox.appendChild( hbox );
    hbox.innerHTML = "<b>r</b>₀: ";
    this.r0display = document.createElement( "input" );
    this.r0display.setAttribute( "size", 24 );
    this.r0display.setAttribute( "readonly", true );
    hbox.appendChild( this.r0display );
    hbox.appendChild( document.createTextNode( "m" ) );
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox grow" );
    vbox.appendChild( hbox );
    hbox.innerHTML = "<b>v</b>₀: ";
    this.v0display = document.createElement( "input" );
    this.v0display.setAttribute( "size", 24 );
    this.v0display.setAttribute( "readonly", true );
    hbox.appendChild( this.v0display );
    hbox.appendChild( document.createTextNode( "m/s" ) );
    
    vbox = document.createElement( "div" );
    vbox.setAttribute( "class", "vbox bordered" );
    vbox.style["margin-left"] = "1em";
    buttonbox.appendChild( vbox );
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox grow" );
    vbox.appendChild( hbox );
    hbox.innerHTML = "<b>r</b>: ";
    this.rdisplay = document.createElement( "input" );
    this.rdisplay.setAttribute( "size", 24 );
    this.rdisplay.setAttribute( "readonly", true );
    hbox.appendChild( this.rdisplay );
    hbox.appendChild( document.createTextNode( "m" ) );
    hbox = document.createElement( "div" );
    vbox.appendChild( hbox );
    hbox.innerHTML = "<b>v</b>: ";
    this.vdisplay = document.createElement( "input" );
    this.vdisplay.setAttribute( "size", 24 );
    this.vdisplay.setAttribute( "readonly", true );
    hbox.appendChild( this.vdisplay );
    hbox.appendChild( document.createTextNode( "m/s" ) );

    // Plot below info

    var plotdiv = document.createElement( "div" );
    plotdiv.setAttribute( "class", "vbox" );
    this.leftbigbox.appendChild( plotdiv );

    this.createplotbox = document.createElement( "div" );
    plotdiv.appendChild( this.createplotbox );
    this.createplotbox.setAttribute( "class", "hbox" );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Create Plot" ) );
    button.addEventListener( "click", function() { self.createPlot(); } );
    this.createplotbox.appendChild( button );
    this.createplotbox.appendChild( document.createTextNode("  X-axis:") );
    this.xaxisselect = document.createElement( "select" );
    this.createplotbox.appendChild( this.xaxisselect );
    this.createplotbox.appendChild( document.createTextNode("  Y-axis:") );
    this.yaxisselect = document.createElement( "select" );
    this.createplotbox.appendChild( this.yaxisselect );
    for ( var selectwidget of [ this.xaxisselect, this.yaxisselect ] ) {
        var seloption = document.createElement( "option" );
        seloption.appendChild( document.createTextNode( "--choose one--" ) );
        seloption.setAttribute( "value", "" );
        selectwidget.appendChild( seloption );
        const options = [ "time", "x0", "y0", "z0", "xf", "yf", "zf", "ymax", "range",
                        "height", "theta", "phi", "thetaf", "phif", "v0", "v" ];
        for ( var option of options ) {
            seloption = document.createElement( "option" );
            seloption.appendChild( document.createTextNode( ProjectilePlot.plottables[ option ][0] ) );
            seloption.setAttribute( "value", option );
            selectwidget.appendChild( seloption );
        }
    }
                               
    this.removeplotbox = document.createElement( "div" );
    plotdiv.appendChild( this.removeplotbox );
    this.removeplotbox.setAttribute( "class", "hbox" );
    this.removeplotbox.style[ "display" ] = "none";
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Remove Plot" ) );
    button.addEventListener( "click", function() { self.removePlot(); } );
    this.removeplotbox.appendChild( button );
}

// **********************************************************************

Projectile.Simulation.prototype.ballSelected = function()
{
    var selords = this.listofballs.getSelectedHandles();
    // console.log( "Selected ordinals: " + ords.join() );
    for ( var ord in this.cannonballs )
        this.cannonballs[ord].removeMoveListener( this.ballmovelistener );
    if ( selords.length > 0 ) {
        this.cannonballs[ selords[0] ].addMoveListener( this.ballmovelistener );
        this.updateBallPos( this.cannonballs[ selords[selords.length-1] ] );
    }
    this.focusOnSelected( selords );
}

// **********************************************************************

Projectile.Simulation.prototype.focusOnSelected = function( selords )
{
    if ( this.highlightselected.checked ) {
        for ( var ord in this.cannonballs ) {
            // Bloody JavaScript is loosely typed... except when it isn't...
            ord = parseInt( ord );
            if ( selords.indexOf( ord ) >= 0 )
                this.cannonballs[ ord ].focus()
            else
                this.cannonballs[ ord ].clearFocus();
        }
    }
    else
    {
        for ( var ord in this.cannonballs )
            this.cannonballs[ ord ].clearFocus();
    }
}

// **********************************************************************

Projectile.Simulation.prototype.viewSelectedBall = function()
{
    var ords = this.listofballs.getSelectedHandles();
    if ( ords.length == 0 ) return;
    var ball = this.cannonballs[ ords[ords.length-1] ]
    var look = new THREE.Vector3( ball.x, ball.y, ball.z );
    var pos = look.clone();
    pos.divideScalar( pos.length() );
    pos.applyAxisAngle( new THREE.Vector3( 0, -1, 0 ), 25 * Math.PI / 180. );
    pos.multiplyScalar( 10. );
    pos.y += 3;
    pos.add( look );
    this.views[0].setCamera( pos, look);
}

// **********************************************************************

Projectile.Simulation.prototype.removeSelectedBall = function()
{
    var ords = this.listofballs.getSelectedHandles();
    if ( ords.length == 0 ) return;
    var removenames = []
    for ( var ord of ords ) {
        this.cannonballs[ ord ].die();
        removenames.push( this.cannonballs[ ord ].name );
        // Gah... this is going to generate a bunch of events.  Oh well.
        this.listofballs.removeItem( ord );
    }
    if ( this.plot != null )
        this.plot.removeData( removenames );
}


// **********************************************************************

Projectile.Simulation.prototype.createPlot = function()
{
    if ( this.plot != null ) return;
    var xaxis = this.xaxisselect.value;
    var yaxis = this.yaxisselect.value;
    if ( xaxis == "" || yaxis == "" ) {
        window.alert( "Please select something to plot on both x and y axes." );
        return;
    }
    this.plot = new ProjectilePlot( this, xaxis, yaxis );
    this.leftbigbox.appendChild( this.plot.elem );
    this.plot.redraw();
    this.createplotbox.style["display"] = "none";
    this.removeplotbox.style["display"] = "flex";
}

// **********************************************************************

Projectile.Simulation.prototype.removePlot = function()
{
    if ( this.plot == null ) return;
    this.removeDataListener( this.plot.datalistener );
    this.plot.elem.remove();
    this.plot = null;
    this.createplotbox.style["display"] = "flex";
    this.removeplotbox.style["display"] = "none";
}
    
// **********************************************************************

Projectile.Simulation.prototype.updateBallPos = function( ball )
{
    // console.log( "updateBallPos for " + ball.name );
    var r0 = "[ " + ball.r0.x.toFixed(3) + ", " + ball.r0.y.toFixed(3) + ", " +
        ball.r0.z.toFixed(3) + "]";
    var v0 = "[ " + ball.v0.x.toFixed(3) + ", " + ball.v0.y.toFixed(3) + ", " +
        ball.v0.z.toFixed(3) + "]";
    var r = "[ " + ball.x.toFixed(3) + ", " + ball.y.toFixed(3) + ", " +
        ball.z.toFixed(3) + "]";
    var v = "[ " + ball.vx.toFixed(3) + ", " + ball.vy.toFixed(3) + ", " +
        ball.vz.toFixed(3) + "]";
    this.r0display.value = r0;
    this.v0display.value = v0;
    this.rdisplay.value = r;
    this.vdisplay.value = v;
}

// **********************************************************************

Projectile.Simulation.prototype.launch = function()
{
    var alt = this.cannon.altitude;
    var az = this.cannon.azimuth;
    var xoff = ( this.cannon.Lx * Math.cos( alt ) - this.cannon.Ly * Math.sin( alt ) );
    var yoff = ( this.cannon.Lx * Math.sin( alt ) + this.cannon.Ly * Math.cos( alt ) );
    var pos = { x: this.cannon.x + xoff * Math.cos( az ),
                y: this.cannon.y + this.cannon.objaboveground + yoff,
                z: this.cannon.z - xoff * Math.sin( az ) };
    var vmag = this.muzvelinput.value;
    var vel = { x: vmag * Math.cos( alt ) * Math.cos( az ),
                y: vmag * Math.sin( alt ),
                z: vmag * Math.cos( alt ) * Math.sin( -az ) };
    
    console.log( "Launching with pos = ( " + pos.x.toFixed( 2 ) + ", " + pos.y.toFixed( 2 )
                 + ", " + pos.z.toFixed( 2 ) + " ), vel = ( " + vel.x.toFixed( 2 )
                 + ", " + vel.y.toFixed( 2 ) + ", " + vel.z.toFixed( 2 ) + ")" );

    var trailcolor = null;
    if ( this.trailcheckbox.checked )
        trailcolor = new THREE.Color( 0xcc00cc );
    this.ballcount += 1;
    var ball = new CannonBall( this.cannon.ballrad, pos, vel, this.scene,
                               { trailcolor: trailcolor,
                                 name: "Ball " + this.ballcount,
                                 datalisteners: this.datalisteners
                               } );
    var ballord = this.listofballs.addItem( ball.name );
    this.cannonballs[ ballord ] = ball;
    this.listofballs.selectNone();
    this.listofballs.select( ballord );
    this.ballSelected();
}

// **********************************************************************

export { Projectile };
