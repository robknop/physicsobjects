import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { PhysicsVector } from "./physicsvector.js";
import { SVGPlot } from "./svgplot.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var RocketBlock = function() {}

RocketBlock.Simulation = function( inparams )
{
    var self = this;
    
    var params = { "name": "Rocket Block",
                   "aspect": 0.5,
                   "campos": new THREE.Vector3( 10., 2., 20.),
                   "camlook": new THREE.Vector3( 10., 0., 0. )
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );

    this.plotdt = 0.25;
    this.blockx0 = 0;
    this.blockgoalx = 20;
    
    this.block = new PhysicsObjects.Block( { rx: 2,
                                             ry: 2,
                                             rz: 2,
                                             mass: 750,
                                             color: 0xff3333,
                                             name: "Block"
                                           } );

    this.initvec = new PhysicsVector( { x: this.blockx0, y: (this.block.y1-this.block.y0)*1.75, z: 0,
                                        xcomp: 0, ycomp: -(this.block.y1-this.block.y0), zcomp: 0,
                                        color: 0xffcc00,
                                        cylrad: 0.2, conelength: 0.8, conebaserad: 0.4 } );
    this.scene.addObject( this.initvec );
    this.finalvec = new PhysicsVector( { x: this.blockgoalx, y: (this.block.y1-this.block.y0)*1.75, z: 0,
                                         xcomp: 0, ycomp: -(this.block.y1-this.block.y0), zcomp: 0,
                                         color: 0xffcc00,
                                         cylrad: 0.2, conelength: 0.8, conebaserad: 0.4 } );
    this.scene.addObject( this.finalvec );
    
    this.rocket = new PhysicsObjects.SmallRocket( { loadfunction: function( obj ) { self.rocketLoaded(); } } );
    this.rocketloaded = false;
    this.interfacelaidout = false;
}

// **********************************************************************

RocketBlock.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( RocketBlock.Simulation.prototype, "constructor",
                       { value: RocketBlock.Simulation,
                         enumerable: false,
                         writeable: true } );

// **********************************************************************

RocketBlock.Simulation.prototype.rocketLoaded = function()
{
    this.scene.addObject( this.rocket );
    this.scene.addObject( this.block );
    
    this.plume = new PhysicsObjects.RocketPlume( {} );
    this.plume.visobj.rotateZ( Math.PI/2. );
    this.plume.visobj.scale.set( 2., 1., 1. );
    this.plume.y = -3.4;

    this.rocketloaded = true;
    this.reset();
}

// **********************************************************************

RocketBlock.Simulation.prototype.reset = function()
{
    if ( this.rocketloaded ) {
        this.rocket.position = { x: this.blockx0 - (this.block.x1-this.block.x0)/2. - this.rocket.nosey, y: 0, z: 0 };
        this.rocket.visobj.setRotationFromEuler( new THREE.Euler( 0., 0., -Math.PI/2., "XYZ" ) );
        this.block.position = { x: this.blockx0 , y: 0, z: 0 };
        this.rocket.velocity = { x: 0, y: 0, z: 0 };
        this.block.velocity = { x: 0, y: 0, z: 0 } ;
        // Hmm... gotta think about the interface.  Not sure I want to have to manually call this
        this.rocket.curStateToOld();
        this.block.curStateToOld();
        
        if ( this.interfacelaidout ) {
            this.twidget.value = 0;
            this.rocketxwidget.value = this.rocket.x.toFixed( 2 );
            this.rocketvxwidget.value = this.rocket.vx.toFixed( 2 );
            this.blockxwidget.value = this.block.x.toFixed( 2 );
            this.blockvxwidget.value = this.block.vx.toFixed( 2 );
        }
    }
}

// **********************************************************************

RocketBlock.Simulation.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );
    
    var hbox, vbox, span, br, div, button, hr, b, p;
    var self = this;

    this.buttondiv = document.createElement( "div" );
    this.buttondiv.setAttribute( "class", "controlsdiv" );
    this.maindiv.appendChild( this.buttondiv );
    
    p = document.createElement( "p" );
    p.innerHTML = "Rocket mass: " + this.rocket.mass.toFixed(0) + " kg<br>Block mass: "
        + this.block.mass.toFixed(0) + " kg";
    this.buttondiv.appendChild( p );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox margintop" );
    this.buttondiv.appendChild( hbox );
    hbox.appendChild( document.createTextNode( "Rocket Thrust:" ) );
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox marginbottom" );
    this.buttondiv.appendChild( hbox );
    this.thrustinput = document.createElement( "input" );
    this.thrustinput.style[ "width" ] = "8ex";
    this.thrustinput.value = 0;
    this.thrustinput.addEventListener( "change", function() {
        var thrust = self.thrustinput.value;
        if ( isNaN( thrust ) ) thrust = 0;
        self.thrustinput.value = thrust;
    } );
    hbox.appendChild( this.thrustinput );
    hbox.appendChild( document.createTextNode( " N" ) );
    
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox margintop" );
    this.buttondiv.appendChild( hbox );
    hbox.appendChild( document.createTextNode( "Fx Rocket on Block:" ) );
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox marginbottom" );
    this.buttondiv.appendChild( hbox );
    this.rocketonblockinput = document.createElement( "input" );
    this.rocketonblockinput.style[ "width" ] = "8ex";
    this.rocketonblockinput.value = 0;
    this.rocketonblockinput.addEventListener( "change", function() {
        var force = self.rocketonblockinput.value;
        if ( isNaN( force ) ) force = 0;
        self.rocketonblockinput.value = force;
    } );
    hbox.appendChild( this.rocketonblockinput );
    hbox.appendChild( document.createTextNode( " N" ) );
    
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox margintop" );
    this.buttondiv.appendChild( hbox );
    hbox.appendChild( document.createTextNode( "Fx Block on Rocket:" ) );
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox marginbottom" );
    this.buttondiv.appendChild( hbox );
    this.blockonrocketinput = document.createElement( "input" );
    this.blockonrocketinput.style[ "width" ] = "8ex";
    this.blockonrocketinput.value = 0;
    this.blockonrocketinput.addEventListener( "change", function() {
        var force = self.blockonrocketinput.value;
        if ( isNaN( force ) ) force = 0;
        self.blockonrocketinput.value = force;
    } );
    hbox.appendChild( this.blockonrocketinput );
    hbox.appendChild( document.createTextNode( " N" ) );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    this.buttondiv.appendChild( hbox );
    hbox.appendChild( document.createTextNode( "tf: " ) );
    this.tfinput = document.createElement( "input" );
    this.tfinput.style[ "width" ] = "4ex";
    this.tfinput.value = 5;
    this.tfinput.addEventListener( "change", function() {
        var tf = self.tfinput.value;
        if ( isNaN( tf ) ) tf = 5;
        if ( tf < 1 ) tf = 1;
        if ( tf > 20 ) tf = 20;
        self.tfinput.value = tf;
    } );
    hbox.appendChild( this.tfinput );
    hbox.appendChild( document.createTextNode( " s" ) );
    
    this.resetbutton = document.createElement( "button" );
    this.resetbutton.appendChild( document.createTextNode( "Reset" ) );
    this.buttondiv.appendChild( this.resetbutton );
    this.resetbutton.addEventListener( "click", function() { self.reset(); } );
    
    this.gobutton = document.createElement( "button" );
    this.gobutton.appendChild( document.createTextNode( "Go!" ) );
    this.buttondiv.appendChild( this.gobutton );
    this.gobutton.addEventListener( "click", function() { self.runSimulation() } );

    // ****************************************
    // Postioin and velocity:

    var hbox = document.createElement( "div" );
    this.leftbigbox.appendChild( hbox );
    hbox.setAttribute( "class", "hbox" );
    hbox.appendChild( document.createTextNode( "t=" ) );
    this.twidget = document.createElement( "input" );
    this.twidget.style["width"] = "6ex";
    this.twidget.setAttribute( "readonly", true );
    hbox.appendChild( this.twidget );
    hbox.appendChild( document.createTextNode( " s  ; Rocket: x=" ) );
    this.rocketxwidget = document.createElement( "input" );
    this.rocketxwidget.style["width"] = "6ex";
    this.rocketxwidget.setAttribute( "readonly", true );
    hbox.appendChild( this.rocketxwidget );
    hbox.appendChild( document.createTextNode( " m, vx=" ) );
    this.rocketvxwidget = document.createElement( "input" );
    this.rocketvxwidget.style["width"] = "6ex";
    this.rocketvxwidget.setAttribute( "readonly", true );
    hbox.appendChild( this.rocketvxwidget );
    hbox.appendChild( document.createTextNode( " m/s  ;  Block x=" ) );
    this.blockxwidget = document.createElement( "input" );
    this.blockxwidget.style["width"] = "6ex";
    this.blockxwidget.setAttribute( "readonly", true );
    hbox.appendChild( this.blockxwidget );
    hbox.appendChild( document.createTextNode( " m, vx=" ) );
    this.blockvxwidget = document.createElement( "input" );
    this.blockvxwidget.style["width"] = "6ex";
    this.blockvxwidget.setAttribute( "readonly", true );
    hbox.appendChild( this.blockvxwidget );
    hbox.appendChild( document.createTextNode( " m/s" ) );

    // ****************************************
    // Plots

    this.plot = new SVGPlot.Plot( { "divid": "xplotdiv",
                                    "svgid": "xplot",
                                    "title": "Rocket: blue, Block: red",
                                    "xtitle": "t (s)",
                                    "ytitle": "x (m)"
                                   } );
    this.leftbigbox.appendChild( this.plot.topdiv );
    this.plot.redraw();
    
    // ****************************************
    
    this.ghostinputs = [ this.thrustinput, this.rocketonblockinput, this.blockonrocketinput, this.tfinput,
                         this.resetbutton, this.gobutton ];

    this.interfacelaidout = true;
    this.reset();
}

// **********************************************************************

RocketBlock.Simulation.prototype.runSimulation = function()
{
    var self = this;

    if ( ! this.rocketloaded ) {
        window.alert( "Rocket isn't loaded yet, can't launch." );
        return;
    }

    this.reset();
        
    for ( var ghost of this.ghostinputs ) ghost.disabled = true

    this.tf = parseFloat( this.tfinput.value );
    this.nextplot = this.plotdt;
    this.plot.clear( false );
    this.rocketdata = new SVGPlot.Dataset( { color: "#0000cc" } );
    this.blockdata = new SVGPlot.Dataset( { color: "#cc0000" } );
    this.plot.addDataset( this.rocketdata );
    this.plot.addDataset( this.blockdata );

    var thrust = parseFloat( this.thrustinput.value );
    var FRonB = parseFloat( this.rocketonblockinput.value );
    var FBonR = parseFloat( this.blockonrocketinput.value );

    this.rocket.accelfunc = function( t, r, v, obj ) {
        return { x: ( thrust + FBonR ) / obj.mass, y: 0, z: 0 }
    };
    this.block.accelfunc = function( t, r, v, obj ) {
        return { x: FRonB / obj.mass, y: 0, z: 0 }
    };
    
    this.rocket.addObject( this.plume );
    this.plume.visobj.scale.x = thrust / 500;
    this.plume.animate();

    // Give it a chance to do any redraws
    window.setTimeout( function() { self.actuallyRunSimulation( thrust ) }, 0 );
}

RocketBlock.Simulation.prototype.actuallyRunSimulation = function( thrust )
{
    var self = this;
    this.rocket.t = this.scene.t;
    this.block.t = this.scene.t
    this.t0 = this.rocket.t;
    this.animationcallback = function( t ) { self.moveObjects( t ); };
    this.scene.addAnimationCallback( this.animationcallback );
}

// **********************************************************************

RocketBlock.Simulation.prototype.moveObjects = function( t )
{
    this.rocket.updateState( t );
    this.block.updateState( t );

    this.twidget.value = ( t - this.t0 ).toFixed( 2 );
    this.rocketxwidget.value = this.rocket.x.toFixed( 2 );
    this.rocketvxwidget.value = this.rocket.vx.toFixed( 2 );
    this.blockxwidget.value = this.block.x.toFixed( 2 );
    this.blockvxwidget.value = this.block.vx.toFixed( 2 );

    if ( t - this.t0 >= this.nextplot ) {
        this.nextplot += this.plotdt;
        this.rocketdata.addPoint( t - this.t0, this.rocket.x );
        this.blockdata.addPoint( t - this.t0, this.block.x );
    }
    
    if ( t - this.t0 >= this.tf ) {
        this.scene.removeAnimationCallback( this.animationcallback );
        for ( var ghost of this.ghostinputs ) ghost.disabled = false;
        this.rocket.removeObject( this.plume );
        this.plume.stopAnimate();
        this.plot.redraw();
    }
}


// **********************************************************************

export { RocketBlock };
