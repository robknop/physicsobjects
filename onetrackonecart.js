import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { CartCharts } from "./cartcharts.js";
import { TrackAndCart } from "./trackandcart.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var OneCart = {};

OneCart.Simulation = function( inparams ) 
{
    // debugger;
    var params = { "axes": false ,
                   "poschart": null,
                   "velchart": null,
                   "accchart": null,
                   "campos": new THREE.Vector3( 0., 19., 94. ),
                   "camlook": new THREE.Vector3( 0., 0, 0. ),
                   "camfov": 60.,
                   "debugfocusscale": 3,
                   "axeslen": 20,
                   // "printfpsevery": 60
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );
    
    if ( params.poschart ) {
        this.hasposchart = true;
        this.poschartdivid = params.poschart;
    }
    if ( params.velchart ) {
        this.hasvelchart = true;
        this.velchartdivid = params.velchart;
    }
    if ( params.accchart ) {
        this.hasaccchart = true;
        this.accchartdivid = params.accchart;
    }
    this.hasanychart = this.hasposchart || this.hasvelchart || this.hasaccchart;
    
    this.track = new TrackAndCart.Track( this.scene );
    this.cart = new TrackAndCart.Cart( this.track );
    this.track.refpos = { x: 0, y: -20, z: 0 };

    this.defaulttrackangle = 0;
    this.minangle = -60;
    this.maxangle = 60;
    this.track.setAngle( this.defaulttrackangle );

    this.table = new PhysicsObjects.Table( );
    // table.obj.position.set( 0, track.obj.position.y-4*track.height, 0 );
    this.table.position = { x: 0, y: this.track.y - this.track.height - this.track.leglength, z: 0 };
    this.scene.addObject( this.table );

    if ( params.axes )
    {
        this.axes = new PhysicsObjects.Axes( );
        this.scene.addObject( this.axes );
    }

    var self = this;
    this.scene.addAnimationCallback( function( t ) { self.track.animate( t ) } );
};

// **********************************************************************

OneCart.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( OneCart.Simulation.prototype, "constructor",
                       { value: OneCart.Simulation,
                         enumerable: false,
                         writeable: true } );



// **********************************************************************
// This function lays out the simulation window and the buttons to the
// right.  It does not add charts.

OneCart.Simulation.prototype.layout = function( )
{
    StandardScene.prototype.layout.call( this );

    var self = this;
    
    // ****************************************
    // Buttons to right of scene

    var buttons = {
        type: "box",
        _class: "controlsdiv",
        id: "buttondiv",
        spec: [ 
            { type: "box",
              _class: "hbox",
              id: "gravdiv",
              spec: [
                  { type: "checkbox",
                    id: "gravcheck",
                    label: "Gravity On?",
                    checked: this.track.gravity,
                    callback: function( event ) {
                        if ( self.gravcheck.checked ) self.track.gravity = true;
                        else self.track.gravity = false;
                    }
                  },
                  { type: "label",
                    id: "gravcheck_label",
                    _for: "gravcheck",
                    text: "Gravity On?"
                  }
              ]
            },

            { type: "box",
              _class: "hbox",
              id: "animdiv",
              spec: [
                  { type: "checkbox",
                    id: "animcheck",
                    checked: true,
                    callback: function( event ) {
                        if ( self.animcheck.checked ) self.scene.resumeDynamics();
                        else self.scene.pauseDynamics();
                    }
                  },
                  { type: "label",
                    id: "animcheck_label",
                    _for: "animcheck",
                    text: "Dynamics On?"
                  }
              ]
            },

            { type: "box",
              id: "anglediv",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "anglediv_text_1",
                    text: "Track angle: "
                  },
                  { type: "numinput",
                    id: "angleinput",
                    min: -60,
                    max: 60,
                    width: "6ex",
                    initval: this.defaulttrackangle,
                    changecallback: function( event ) {
                        if ( self.angleinput.value < self.minangle )
                            self.angleinput.value = self.minangle;
                        if ( self.angleinput.value > self.maxangle )
                            self.angleinput.value = self.maxangle;
                        self.track.setAngle( self.angleinput.value * Math.PI / 180 );
                    }
                  },
                  { type: "span",
                    id: "anglediv_text_2",
                    text: "⁰"
                  }
              ]
            },

            // This is an ugly hack, but for some reason, at least on Firefox, my number inputs
            //   are spilling over on the bottom
            { type: "box",
              id: "uglyhack",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "uglyhack_text",
                    text: " "
                  }
              ]
            },
            
            { type: "box",
              id: "lefthitdiv",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "lefthitdiv_text_1",
                    text: "At left edge: "
                  },
                  { type: "select",
                    id: "lefthit",
                    options: [ "Stick", "Bounce", "Wrap" ],
                    selindex: 1,
                    changecallback: function( event ) {
                        self.track.stick.x0 = false
                        self.track.periodic.x0 = false
                        if ( self.lefthit.selectedIndex == 2 )
                            self.track.periodic.x0 = true
                        else if ( self.lefthit.selectedIndex == 0 )
                            self.track.stick.x0 = true;
                    }
                  }
              ]
            },

            { type: "box",
              id: "righthitdiv",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "righthitdiv_text_1",
                    text: "At right edge: "
                  },
                  { type: "select",
                    id: "righthit",
                    options: [ "Stick", "Bounce", "Wrap" ],
                    selindex: 1,
                    changecallback: function( event ) {
                        self.track.stick.x1 = false
                        self.track.periodic.x1 = false
                        if ( self.righthit.selectedIndex == 2 )
                            self.track.periodic.x1 = true
                        else if ( self.righthit.selectedIndex == 0 )
                            self.track.stick.x1 = true;
                    }
                  }
              ]
            },

            { type: "box",
              id: "dragdiv",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "dragdiv_text_0",
                    text: "Mouse drag: "
                  },
                  { type: "select",
                    id: "drag",
                    options: [ "Repositions", "Flings" ],
                    selindex: 0,
                    changecallback: function( event ) {
                        if ( self.drag.selectedIndex == 1 )
                            self.track.flingcart = true;
                        else
                            self.track.flingcart = false;
                    }
                  }
              ]
            }
        ] // end of buttondiv spec
    };
    var elemlist = InterfaceMaker( buttons );
    this.buttondiv = elemlist[ "buttondiv" ];
    this.gravcheck = elemlist[ "gravcheck" ];
    this.animcheck = elemlist[ "animcheck" ];
    this.angleinput = elemlist[ "angleinput" ];
    this.lefthit = elemlist[ "lefthit" ];
    this.righthit = elemlist[ "righthit" ];
    this.drag = elemlist[ "drag" ];
                        
    this.maindiv.appendChild( this.buttondiv );
}
    
// ****************************************
// Add charts

OneCart.Simulation.prototype.addCharts = function()
{
    if ( ! this.hasanychart ) return;
    var container,div, posdiv, veldiv, accdiv, button, label, h4, tr, td;
    
    var self = this;

    this.charts = new CartCharts.ChartCollection( this.hasposchart, this.hasvelchart, this.hasaccchart )

    var chartnames = [ "pos", "vel", "acc" ];

    container = document.createElement( "div" );
    container.setAttribute( "class", "hbox" );
    document.body.appendChild( container );
    
    for ( const chartname of chartnames.values() ) {
        if ( this[ "has" + chartname + "chart"] ) {
            div = document.createElement("div");
            div.setAttribute( "class", "cartchart" );
            div.setAttribute( "id", this[ chartname + "chartdivid" ] );
            container.appendChild( div );
            div.appendChild( this.charts[ chartname + "chart"].chart.topdiv );
            // button = document.createElement( "input" );
            // button.setAttribute( "type", "Button" );
            // button.setAttribute( "value", "Reset Zoom" );
            // button.addEventListener( "click", function() {
            //     self.charts[ chartname + "chart"].chart.resetZoom();
            // } );
            // div.appendChild( button );
        }
    }
    this.charts.redraw();
    
    var hr = document.createElement("hr");
    this.buttondiv.appendChild(hr);

    var div, label;
    
    // div = document.createElement("div");
    // div.style.display = "block";
    // div.style.margin = this.buttonmargin;
    // this.buttondiv.elem.appendChild(div);
    // this.realtimeplotcheck = document.createElement("input");
    // this.realtimeplotcheck.setAttribute("type", "checkbox");
    // this.realtimeplotcheck.setAttribute("id", "realtimeplotcheck")
    // this.realtimeplotcheck.checked = false;
    // this.realtimeplotcheck.addEventListener("click", function(event)
    //                                         {
    //                                             if ( this.hasposchart ) 
    //                                                 self.charts.poschart.realtime = self.realtimeplotcheck.checked;
    //                                             if ( this.hasvelchart )
    //                                                 self.charts.velchart.realtime = self.realtimeplotcheck.checked;
    //                                             if ( this.hasaccchart )
    //                                                 self.charts.accchart.realtime = self.realtimeplotcheck.checked;
    //                                         }
    //                                        );
    // label = document.createElement("label");
    // label.setAttribute("for", "realtimeplotcheck");
    // label.appendChild(document.createTextNode("Realtime plot update"));
    // div.appendChild(this.realtimeplotcheck);
    // div.appendChild(label);

    this.startbutton = document.createElement("input");
    this.startbutton.setAttribute("type", "button");
    this.startbutton.setAttribute("value", "Start Recording");
    this.startbutton.style.display = "block";
    this.startbutton.style.margin = this.buttonmargin;
    this.buttondiv.appendChild( this.startbutton );
    this.startbutton.addEventListener("click", function(event) {
        // Rob, make this next line better
        if ( ! self.charts.poschart.recording ) {
            self.charts.startRecording( self.cart )
            self.startbutton.disabled = true;
            self.stopbutton.disabled = false;
        }
    } );

    this.stopbutton = document.createElement( "input" );
    this.stopbutton.setAttribute( "type", "button" );
    this.stopbutton.setAttribute( "value", "Stop Recording" );
    this.stopbutton.disabled = true;
    this.stopbutton.style.display = "block";
    this.stopbutton.style.margin = this.buttonmargin;
    this.buttondiv.appendChild( this.stopbutton );
    this.stopbutton.addEventListener("click", function(event) {
        // Rob, make this next line better
        if ( self.charts.poschart.recording ) {
            self.charts.stopRecording();
            self.startbutton.disabled = false
            self.stopbutton.disabled = true;
            self.renderDataSetTable();
        }
    } );

    // this.buttondiv.elem.appendChild( document.createElement( "br" ) );
    this.buttondiv.appendChild( document.createElement( "hr" ) );
    // this.buttondiv.elem.appendChild( document.createElement( "br" ) );

    h4 = document.createElement( "h4" );
    h4.appendChild( document.createTextNode( "Datasets" ) );
    this.buttondiv.appendChild( h4 );

    this.datasettable = document.createElement( "table" );
    this.datasettable.setAttribute( "class", "datasettable" );
    this.buttondiv.appendChild( this.datasettable );

    this.renderDataSetTable();

    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Delete Selected" ) );
    this.buttondiv.appendChild( button );
    button.addEventListener( "click", function() {
        self.deleteSelectedDatasets();
    } );
}


// **********************************************************************

OneCart.Simulation.prototype.renderDataSetTable = function()
{
    var tr, td, checkbox, label;

    this.datasetcheckboxes = [];
    while ( this.datasettable.firstChild ) this.datasettable.removeChild( this.datasettable.firstChild );
    
    for ( var i = 0 ; i < this.charts.datasets.length ; ++i ) {
        tr = document.createElement( "tr" )
        this.datasettable.appendChild( tr );
        td = document.createElement( "td" );
        tr.appendChild( td );
        checkbox = document.createElement( "input" )
        checkbox.setAttribute( "type", "checkbox" );
        this.datasetcheckboxes[i] = checkbox;
        td.appendChild( checkbox );
        td = document.createElement( "td" );
        tr.appendChild( td );
        td.appendChild( document.createTextNode( this.charts.datasets[i].name ) );
        td.style.color = this.charts.datasets[i].color;
    }    
}

// **********************************************************************

OneCart.Simulation.prototype.deleteSelectedDatasets = function()
{
    var dellist = [];
    for ( var i = 0 ; i < this.datasetcheckboxes.length ; ++i )
        if ( this.datasetcheckboxes[i].checked )
            dellist.push( i );

    // Do it backwards so that the indexes don't
    //  get screwed up inside ChartCollection
    for ( var i = dellist.length-1 ; i >= 0 ; --i )
        this.charts.removeDataSet( dellist[ i ] );

    this.renderDataSetTable();
}

// **********************************************************************

export { OneCart };
