import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var TrackAndCart = {};

// **********************************************************************
// **********************************************************************
// **********************************************************************
// track is a silver track thingy that can have carts on it.
//
// Its parent MUST be a PhysicsObject.scene
//
// (refx, refy, refz) is the position of the top of the right
// side of the track.  Angle is angle the left side is raised above
// the right side.  Oriented along x.
//
// Receives mousedown events.  Any threejs object that is directly a child
//   of track.obj will have its "mousedown" function called when clicked on
//   (if that function exists).
//
// Key properties:
//   obj — the three.js object of the track
//   scene — the PhysObjects.scene this track is in
//   length, width, ieght — dimensions of the track (same usits as threejs scene)
//   cartlist — List of movable carts on the track
//   addCart() — adds a cart to cartlist AND adds cart.obj as a child of obj
//   removeCart()

TrackAndCart.Track = function( scene, passparams )
{
    var params = { length: 200,
                   width: 10,
                   height: 2,
                   leglength: 8,
                   angle: 0,
                   springedge: false,
                   stickycarts: false,
                   carteloss: 0
                 };
    for ( const param in passparams )
        params[param] = passparams[param];

    PhysicsObjects.World.call( this, { name: "Track",
                                       bbox: { x0: -params.length/2, x1: params.length/2,
                                               y0: -1000 * params.height, y1: 1000 * params.height,
                                               z0: -1000 * params.width, z1: 1000 * params.width },
                                       springedge: params.springedge,
                                     } );
    
    var self = this;
    this.parent = scene;
    this.length = params.length;
    this.width = params.width;
    this.height = params.height;
    this.angle = 0;
    this.cartbasey = this.height/2;
    var legwidth = params.width/8;
    this.leglength = params.leglength;
    this.legxoffset = params.width/4;
    this.legzoffset = 3/8 * params.width;
    this._refpos = { x: 0, y: 0, z: 0 };
    Object.assign( this._refpos, this.position );

    this.visobj = new THREE.Group();
    this.visobj.name = "Track";
    this.visobj.position.copy( this.position );
    
    var boxgeo = new THREE.BoxGeometry( params.length, params.height, params.width );
    var metalmat = new THREE.MeshLambertMaterial( { color: 0x999999 } );
    this.trackobj = new THREE.Mesh( boxgeo, metalmat );
    this.trackobj.name = "Track Top";
    this.trackobj.position.set( 0, -params.height/2, 0 );
    this.visobj.add(this.trackobj);

    var cylgeo = new THREE.CylinderGeometry( legwidth, legwidth, this.leglength );
    var cylmat = new THREE.MeshLambertMaterial( { color: 0x333333 } );
    // left back, left front, right back, right front
    this.legs = [];
    var legx = [ -this.length/2 + this.legxoffset , -this.length/2 + this.legxoffset,
                 this.length/2 - this.legxoffset, this.length/2 - this.legxoffset ];
    var legz = [ -this.legzoffset , this.legzoffset , -this.legzoffset , this.legzoffset ];
    for ( var i = 0 ; i < 4 ; ++i )
    {
        this.legs.push( new THREE.Mesh( cylgeo, cylmat ) );
        this.legs[i].position.set( legx[i], -params.height-this.leglength/2, legz[i] );
        this.legs[i].name = "Leg " + i;
        this.visobj.add(this.legs[i]);
    }
    
    this.parent.addObject( this );

    this.stickycarts = params.stickycarts;
    this.carteloss = params.carteloss;

    this.downcallback = {};
    this.movingcallback = {};
    this.upcallback = {};
    this.initmousex = {};
    this.initmousey = {};
    this.cartx0 = {};
    // ROB!  Think about what happens if a view
    //  is added after the track is added to the scene!
    for ( let view of this.parent.views ) {
        this.downcallback[ view ] = function(event) { self.mousedown( view, event ); }
        this.movingcallback[ view ] = function(event) { self.mousemove( view, event ); }
        this.upcallback[ view ] = function(event) { self.mouseup( view, event ); }
        view.elem.addEventListener( "mousedown", this.downcallback[ view ] );
        this.initmousex[ view ] = 0;
        this.initmousey[ view ] = 0;
        this.cartx0[ view ] = 0;
    }

    this.manualmove = false;
    this.movingcart = undefined;
    this.movingview = undefined;
    this.gravity = true;
    this.flingcart = false;
}

TrackAndCart.Track.prototype = Object.create( PhysicsObjects.World.prototype );
Object.defineProperty( TrackAndCart.Track.prototype, 'constructor',
                       {
                           value: TrackAndCart.Track,
                           enumerable: false,
                           writable: true
                       } );


// ****************************************

Object.defineProperty( TrackAndCart.Track.prototype, "refpos", {
    get() { return this._refpos},
    set( pos ) {
        this._refpos.x = pos.x;
        this._refpos.y = pos.y;
        this._refpos.z = pos.z;
        this.setAngle( this.angle );
    }
} );
    

// **********************************************************************

TrackAndCart.Track.prototype.getAccel = function( mass, pos, vel, mu_k=0., mu_s=0., dt=1. )
{
    const minvel = 1e-8;
    var a  = { x:0, y:0, z: 0 };
    var stopped = false;
    if ( this.gravity )
        a.x = 980 * Math.sin( this.angle );
    if ( mu_k > 0. ) {
        if ( vel.x > 0 )
            a.x -= mu_k * 980 * Math.cos( this.angle );
        else
            a.x += mu_k * 980 * Math.cos( this.angle );
    }
    if ( ( vel.x + a.x * dt ) * ( vel.x ) < 0 ) {
        stopped = true;
    }
    if ( Math.abs( vel.x ) < minvel ) stopped = true;
    if ( stopped ) {
        if ( Math.abs( Math.sin( this.angle ) ) < mu_s * Math.abs( Math.cos( this.angle ) ) ) {
            if ( dt <= 0 )
                a.x = 0;
            else
                a.x = -vel.x / dt;
        }
    }
    return a;
}

// ****************************************
// pass angle in radians

TrackAndCart.Track.prototype.setAngle = function( angle )
{
    this.angle = angle;
    var quat = new THREE.Quaternion();
    quat.setFromAxisAngle( new THREE.Vector3( 0, 0, -1 ), this.angle );
    var invquat = new THREE.Quaternion();
    invquat.setFromAxisAngle( new THREE.Vector3( 0, 0, 1 ), this.angle );
    this.visobj.setRotationFromQuaternion( quat );
    this.position = { x: this._refpos.x,
                      y: this._refpos.y + this.length/2 * Math.sin( Math.abs(angle) ),
                      z: 0
                    };
    // console.log( "x = " + this.position.x + " , y = " + this.position.y + " , z = " + this.position.z );
    // console.log( "y - refposy = " + ( this.position.y - this._refpos.y ) );
    var leftlen, rightlen, lefty, righty, leftx, rightx;
    if ( angle < 0 )
    {
        leftlen = this.leglength + this.legxoffset * Math.sin( -angle );
        rightlen = this.leglength + (this.length - 2*this.legxoffset) * Math.sin( -angle );
    }
    else
    {
        leftlen = this.leglength + (this.length - 2*this.legxoffset) * Math.sin( angle );
        rightlen = this.leglength + this.legxoffset * Math.sin( angle );
    }
    leftx = - this.length/2 + this.legxoffset + leftlen/2 * Math.sin( Math.abs( angle ) );
    rightx = this.length/2 - this.legxoffset - rightlen/2 * Math.sin( Math.abs( angle ) );
    lefty = - this.height * Math.cos( angle ) - leftlen/2 * Math.cos( angle );
    righty = - this.height * Math.cos( angle ) - rightlen/2 * Math.cos( angle );

    this.legs[0].scale.y = leftlen / this.leglength;
    this.legs[1].scale.y = leftlen / this.leglength;
    this.legs[2].scale.y = rightlen / this.leglength;
    this.legs[3].scale.y = rightlen / this.leglength;
    for( var i = 0 ; i < 4 ; ++i )
    {
        this.legs[i].setRotationFromQuaternion( invquat );
        if ( i < 2 )
        {
            this.legs[i].scale.y = leftlen / this.leglength;
            this.legs[i].position.x = leftx;
            this.legs[i].position.y = lefty;
        }
        else
        {
            this.legs[i].scale.y = rightlen / this.leglength;
            this.legs[i].position.x = rightx;
            this.legs[i].position.y = righty;
        }
    }
}

// ****************************************
// Call this inside an animate function (with requestAnimationFrame).
// You probably want to add this function in a scene.addAnimationCallback

TrackAndCart.Track.prototype.animate = function( t )
{
    this._t = t

    // console.log(" manualmove = " + this.manualmove);
    // if ( this.manualmove)
    //     console.log( " movingcart id = " + this.movingcart.visobj.id
    //             + " obj[0] id " + this.children[0].visobj.id );

    for ( let cart of this.children ) {
        if ( !this.manualmove || ( cart != this.movingcart ) ) {
            cart.updateState( t );
        }
    }

    // Look for cart collisions.  Note that "updateState" will have already done
    //   edge detection for the track
    var collisionpairs = [];
    for ( let i = 0 ; i < this.children.length-1 ; ++i ) {
        let cart1 = this.children[i];
        for ( let j = i+1 ; j < this.children.length ; ++j ) {
            let cart2 = this.children[j];
            if ( ( ( cart1.x < cart2.x ) &&
                   ( cart1.vx - cart2.vx > 0 ) &&
                   ( cart1.x + cart1.x1 > cart2.x + cart2.x0 ) )
                 ||
                 ( ( cart1.x > cart2.x ) &&
                   ( cart1.vx - cart2.vx < 0 ) &&
                   ( cart1.x + cart1.x0 < cart2.x + cart2.x1 ) )
               )
                collisionpairs.push( [ cart1, cart2 ] );
        }
    }

    // If one of the cart is a manual mover, treat it as having infinite mass
    for ( let pair of collisionpairs ) {
        let cart1 = pair[0];
        let cart2 = pair[1];
        if ( this.stickycarts ) {
            if ( this.manualmove && cart1 == this.movingcart ) {
                cart1.stickCart( cart2 );
            }
            else if ( this.manualmove && cart2 == this.movingcart ) {
                cart2.stickCart( cart1 );
            }
            else {
                let vf = ( cart1.mass * cart1.vx + cart2.mass * cart2.vx ) / ( cart1.mass + cart2.mass );
                cart1.stickCart( cart2 );
                cart1.vx = vf;
            }
        }
        else {
            if ( this.carteloss != 0 ) {
                console.log( "WARNING! Only elastic and perfectly inelastic collisions supported. " +
                             "Treating this collision as elastic." );
            }
            let v1i = cart1.vx;
            let v2i = cart2.vx;
            if ( this.manualmove && cart1 == this.movingcart ) {
                cart2.vx = 2 * v1i - v2i;
            }
            else if ( this.manualmove && cart2 == this.movingcart ) {
                cart1.vx = 2 * v2i - v1i;
            }
            else {
                let m1 = cart1.mass
                let m2 = cart2.mass;
                cart1.vx = ( 2 * m2 * v2i + (m1-m2) * v1i ) / ( m1 + m2 );
                cart2.vx = ( 2 * m1 * v1i + (m2-m1) * v2i ) / ( m1 + m2 );
            }
        }
    }
}

// ****************************************
// Mouse events

TrackAndCart.Track.prototype.mousedown = function(view, event)
{
    if ( event.shiftKey || event.ctrlKey ) return;
    if ( this.movingcart != undefined ) return;    // Don't process down if we're moving

    var canvasdims = new THREE.Vector2();
    var mouse = new THREE.Vector2();
    var obj;
    canvasdims.set( view.elem.width , view.elem.height );
    mouse.x = ( event.offsetX / canvasdims.x ) * 2 - 1;
    mouse.y = -( event.offsetY / canvasdims.y ) * 2 + 1;
    // console.log( "track.mousedown at (" + event.offsetX + ", " + event.offsetY + "), normalized (" +
    //              mouse.x + ", " + mouse.y + ")");
    view.raycaster.setFromCamera( mouse, view.camera );
    var intersects = view.raycaster.intersectObjects( this.parent.scene.children, true );

    var found = false;
    var foundcartdex = undefined;
    for ( var i = 0 ; ( !found ) && (i < intersects.length) ; i++)
    {
        obj = intersects[i].object;
        // console.log("Intersects...");
        while ( ( !found ) && ( obj.id != this.parent.scene.id ) )
        {
            // console.log("   " + obj.name);
            // Is this one of my carts?
            for ( var j = 0 ; j < this.children.length ; j++ )
            {
                if ( this.children[j].visobj.id == obj.id )
                {
                    // console.log("   ...which is one of my carts!  Index " + j);
                    found = true;
                    foundcartdex = j;
                }
            }
            if ( !found ) obj = obj.parent;
        }
    }
    if ( found )
    {
        // console.log( "Found cart " + obj.name + " (id " + obj.id + ")");
        this.movingcart = this.children[foundcartdex];
        this.movingview = view;
        this.manualmove = true;
        this.initmousex[ view ] = event.x;
        this.initmousey[ view ] = event.y;
        this.cartx0[ view ] = this.movingcart.x;
        this.movingcart.startManualMove( this.t );
        view.elem.addEventListener( "mousemove", this.movingcallback[ view ] );
        view.elem.addEventListener( "mouseup", this.upcallback[ view ] );
    }
}


TrackAndCart.Track.prototype.movecart = function( view, mousex, mousey )
{
    if ( view != this.movingview ) return;
    var dmouse = new THREE.Vector3( mousex - this.initmousex[ view ], mousey - this.initmousey[ view ], 0. );
    // Convert dmouse to Normalized Device Coordinates
    var canvasdims = new THREE.Vector2();
    canvasdims.set( view.elem.width, view.elem.height );
    dmouse.x *= 2 / canvasdims.x;
    dmouse.y *= -2 / canvasdims.y;

    // console.log("In movecart.  this.x = " + this.x + ", this.y = " + this.y);
    // console.log("  this.length = " + this.length + ", this.angle = " + this.angle);
    
    // Get the lenght and unit vector of the track in NDC
    var leftend = new THREE.Vector3( this.x - Math.cos( this.angle ) * this.length/2,
                                     this.y + Math.sin( this.angle ) * this.length/2,
                                     this.z );
    var dtrack = new THREE.Vector3( this.x + Math.cos( this.angle ) * this.length/2,
                                    this.y - Math.sin( this.angle ) * this.length/2,
                                    this.z );
    // console.log("leftend = " + leftend.x + ", " + leftend.y + ", " + leftend.z)
    // console.log("dtrack = " + dtrack.x + ", " + dtrack.y + ", " + dtrack.z)
    leftend.project( view.camera );
    dtrack.project( view.camera );
    dtrack.sub( leftend );
    var tracklen = dtrack.length();
    dtrack.normalize();
    var trackfrac = dmouse.dot(dtrack) / tracklen;
    
    var newpos = this.cartx0[ view ] + trackfrac * this.length;

    // console.log( "movecart ; x0 = " + this.cartx0 + " ; trackfrac = " + trackfrac +
    //              " ; newpos = " + newpos ); 
    // console.log( "Moving cart to t = " + this.t + " , newpos = " + newpos );

    this.movingcart.moveTo( this.t, newpos, 0, 0 );
}

TrackAndCart.Track.prototype.mousemove = function( view, event )
{
    if ( this.movingcart == undefined ) return;
    if ( view != this.movingview ) return;
    this.movecart( view, event.x, event.y );
}

TrackAndCart.Track.prototype.mouseup = function( view, event )
{
    if ( this.movingcart == undefined ) return;
    if ( view != this.movingview ) return;
    this.movecart( view, event.x, event.y );
    this.manualmove = false;
    if ( ! this.flingcart )
        this.movingcart.velocity = { x: 0, y: 0, z: 0 };
    this.movingcart.curStateToOld();
    this.movingcart = undefined;
    this.movingview = undefined;
    view.elem.removeEventListener( "mousemove", this.movingcallback[ view ] );
    view.elem.removeEventListener( "mouseup", this.upcallback[ view ] );
}


// **********************************************************************
// **********************************************************************
// **********************************************************************
// Cart

TrackAndCart.Cart = function( track, inparams = { } )
{
    var self = this;
    var params = { 
        mass: 1,
        barmass: 0.75,
        name: "Cart",
        color: 0x990000,
        rx: track.length/20,
        ry: 0.3 * track.width,
        rz: 0.45 * track.width,
        barlenfrac: 0.9,
        barheifrac: 0.4,
        barwidfrac: 0.4,
        nbarshoriz: 2,
        wheels: true,
        mu_k: 0.,
        mu_s: 0.,
        accelfunc: null,
    }
    Object.assign( params, inparams )
    params.parent = track;

    if ( params.mu_k < 0. || params.mu_k >= 1. || params.mu_s < 0. || params.mu_s >= 1. ) {
        window.alert( "Warning: friction coefficients not in the range [0, 1) will produce distressing behavior!" );
    }
    if ( params.mu_k > params.mu_s ) {
        window.alert( "Warning: mu_k > mu_s will produce distressing behavior!" );
    }

    this._mu_k = params.mu_k;
    this._mu_s = params.mu_s;
    
    if ( params.accelfunc == null ) {
        params.accelfunc = function( t, pos, vel, obj, dt ) {
            return track.getAccel( self.mass, pos, vel, self._mu_k, self._mu_s, dt );
        };
    }

    PhysicsObjects.Object.call( this, params );

    var cartlen = this.x1 - this.x0;
    var cartwid = this.z1 - this.z0;
    var carthei = this.y1 - this.y0;
    var wheelrad = carthei * 0.4;
    var wheelwid = wheelrad / 4;
    var wheelvertoff = 0.7 * wheelrad;

    this.cartwid = cartwid;
    
    // console.log("Making cart width length " + cartlen + ", width " + cartwid +
    //             ", height " + carthei + ", wheelrad " + wheelrad + ", wheelwid " + wheelwid);
        
    var boxgeom = new THREE.BoxGeometry( cartlen, carthei, cartwid );
    var boxmat = new THREE.MeshLambertMaterial( { color: params.color } );

    if ( params.wheels ) {
        var wheelgeom = new THREE.CylinderGeometry( wheelrad, wheelrad, wheelwid, 16 );
        wheelgeom.rotateX(Math.PI/2);
        var wheelmat = new THREE.MeshLambertMaterial( { color: 0x333333 } );
    }
    else {
        wheelwid = 0;
        wheelrad = 0;
        wheelvertoff = 0;
    }

    this.bars = [];
    this.barmass = params.barmass;
    this.barbase = carthei + wheelrad - wheelvertoff;
    this.nbarshoriz = 2;
    this.barlen = params.barlenfrac * cartlen;
    this.barhei = params.barheifrac * carthei;
    this.barwid = params.barwidfrac * cartwid;
    this.bargeom = new THREE.BoxGeometry( this.barlen, this.barhei, this.barwid );
    this.barmat = new THREE.MeshStandardMaterial( { color: 0xc0c0c0,
                                                    metalness: 1.0,
                                                    roughness: 0.5
                                                  } );

    this.visobj = new THREE.Group()

    var box = new THREE.Mesh( boxgeom, boxmat );
    box.name = "Cart Body";
    box.position.y = (wheelrad - wheelvertoff) + carthei/2;
    this.visobj.add(box);

    if ( params.wheels ) {
        var posx = [ -cartlen/2. + 1.1*wheelrad, -cartlen/2. + 1.1*wheelrad,
                     cartlen/2. - 1.1*wheelrad, cartlen/2. - 1.1*wheelrad ];
        var posz = [ -cartwid/2, cartwid/2, -cartwid/2, cartwid/2 ];
        var posy = wheelrad;
        var i;
        for (i = 0 ; i < 4 ; i++)
        {
            var wheel = new THREE.Mesh( wheelgeom, wheelmat );
            wheel.position.set(posx[i], posy, posz[i]);
            wheel.name = "wheel " + i;
            this.visobj.add(wheel)
        }
    }

    this.myorigmass = this.mass;
    this.myorigx0 = this.x0;
    this.myorigx1 = this.x1;

    // Carts that move with this cart.  One cart is the parent, others are children

    this.stuckcarts = [];
    this.stuckparent = null;
    
    this.visobj.name = name;
    this.parent.addObject( this );
}

TrackAndCart.Cart.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( TrackAndCart.Cart.prototype, 'constructor',
                       {
                           value: TrackAndCart.Cart,
                           enumerate: false,
                           writable: true } );

Object.defineProperty( TrackAndCart.Cart.prototype, "mu_k", {
    get() { return this._mu_k },
    set( val ) {
        if ( val < 0 ) val = 0;
        if ( val > 0.99 ) val = 0.99;
        this._mu_k = val
    }
} );

Object.defineProperty( TrackAndCart.Cart.prototype, "mu_s", {
    get() { return this._mu_s },
    set( val ) {
        if ( val < 0 ) val = 0;
        if ( val > 0.99 ) val = 0.99;
        this._mu_s = val
    }
} );

// **********************************************************************

TrackAndCart.Cart.prototype.stickCart = function( cart )
{
    if ( cart.stuckparent != null ) {
        window.alert( "Trying to stick a non-stuck-parent!  This should never happen.  Doing nothing." );
        return;
    }
    this.stuckcarts.push( cart );
    cart.stuckparent = this;

    this.parent.removeObject( cart );
    this.addObject( cart );

    cart.x -= this.x

    let x0 = this.myorigx0;
    let x1 = this.myorigx1;
    let mass = this.myorigmass;

    // Note that the x coordinates of the stuck carts are all realtive
    //   to this cart.
    for ( let stuck of this.stuckcarts ) {
        mass += stuck.mass;
        if ( stuck.x + stuck.x0 < x0 ) x0 = stuck.x + stuck.x0;
        if ( stuck.x + stuck.x1 > x1 ) x1 = stuck.x + stuck.x1;
    }
    this.mass = mass;
    this.x0 = x0
    this.x1 = x1;

}

// **********************************************************************

TrackAndCart.Cart.prototype.addBar = function( )
{
    var bar = new THREE.Mesh( this.bargeom, this.barmat );
    var barsp = this.cartwid / this.nbarshoriz;
    bar.position.z = -this.cartwid/2 + barsp/2 + (this.bars.length % this.nbarshoriz) * barsp;
    bar.position.y = Math.floor( this.bars.length  / this.nbarshoriz ) * 1.05*this.barhei +
        0.5025*this.barhei + this.barbase;
    this.visobj.add( bar );
    this.bars.push( bar );
    this.mass += this.barmass;
    this.myorigmass += this.barmass;
    
    // Also have to add the mass to the top of the stuck parent tree

    if ( this.stuckparent != null ) {
        var parent = this.stuckparent;
        while ( parent.sutkcparent != null ) parent = parent.stuckparent;
        parent.mass += this.barmass;
    }
}

// **********************************************************************

TrackAndCart.Cart.prototype.removeBar = function( )
{
    var bar = this.bars.pop();
    this.visobj.remove( bar );
    this.mass -= this.barmass;
    this.myorigmass -= this.barmass;
    if ( this.stuckparent != null ) {
        var parent = this.stuckparent;
        while ( parent.stuckparent != null ) parent = parent.stuckparent;
        parent.mass -= this.barmass;
    }
}

// **********************************************************************

export { TrackAndCart };
