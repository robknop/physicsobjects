// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// **********************************************************************
// Width-adjusting div thingy
// **********************************************************************

var DivBox = function(widthfrac = 0.5, aspect = undefined, styleprops = {}, elem=undefined)
{
    var self = this;

    var attrs = { "padding": 4,
                  "margin": 4,
                  "border-style": "solid",
                  "border-width": 2,
                  "border-color": "black" };
    Object.assign(attrs, styleprops);

    this.border_width = attrs["border-width"];
    this.padding = attrs["padding"];
    this.margin = attrs["margin"];
    this.aspect = aspect;
    this.widthfrac = widthfrac;
    
    if (elem === undefined) this.elem = document.createElement("div");
    else this.elem = elem;

    for ( var attr in attrs )
    {
        var val = attrs[attr];
        if (typeof(val) == "number") val += "px";
        this.elem.style[attr] = val;
    }
    this.resize();
    document.defaultView.addEventListener("resize", function(event) { self.resize(event) });
}

DivBox.prototype.resize = function(event)
{
    // console.log("In divbox.resize...");
    // console.log("  ...innerWidth = " + document.defaultView.innerWidth + " (" + typeof(document.defaultView.innerWidth) + ")");
    // console.log("  ...border_width = " + this.border_width + " (" + typeof(this.border_width) + ")");
    // console.log("  ...padding = " + this.padding + " (" + typeof(this.padding) + ")");
    // console.log("  ...margin = " + this.margin + " (" + typeof(this.margin) + ")");
    // console.log("  ...aspect = " + this.aspect + " (" + typeof(this.aspect) + ")");
    var width = Math.floor( this.widthfrac*document.defaultView.innerWidth
                            - 2*this.border_width - 2*this.padding - 2*this.margin);
    this.elem.style["width"] = width + "px";
    if ( !(this.aspect === undefined) )
    {
        var height = Math.floor(width * this.aspect);
        this.elem.style["height"] = height + "px";
    }
}

// **********************************************************************

export { DivBox };
