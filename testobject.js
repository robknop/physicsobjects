import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var TestObject = function() {}

TestObject.Simulation = function( inparams )
{
    var self = this;
    var params = { name: "Test Object",
                   aspect: 0.75,
                   gridbg: false,
                   showaxes: false,
                   bboxoption: true,
                   nviews: 1,
                   printfpsevery: 60,
                 };
    Object.assign( params, inparams );
    
    StandardScene.call( this, params );
    // this.debugfocus = true;
    
    // this.rocket = new PhysicsObjects.SmallRocket( {
    //     loadfunction: function( obj ) {
    //         self.scene.addObject( obj );
    //         self.plume = new PhysicsObjects.RocketPlume( { } );
    //         self.rocket.addObject( self.plume );
    //         self.plume.visobj.rotateZ( Math.PI/2. );
    //         self.plume.visobj.scale.set( 2, 1., 1. );
    //         self.plume.y = -3.4;
    //         self.plume.animate();
    //         self.rocket.visobj.rotateZ( -Math.PI/2. );
    //     } } );

    // this.first = true;
    // this.length0 = 3
    // this.ampl = 1;
    // this.T = 4;
    // this.spring = new PhysicsObjects.Spring( { length: this.length0 } );
    // this.scene.addObject( this.spring );
    // this.animfunc = function( t ) { self.sproingy( t ); }
    // this.scene.addAnimationCallback( this.animfunc );
}

TestObject.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( TestObject.Simulation.prototype, "constructor",
                       {
                           value: TestObject.Simulation,
                           enumerable: false,
                           writeable: true } );

export { TestObject };

// **********************************************************************

TestObject.Simulation.prototype.sproingy = function( t )
{
    if ( this.first ) {
        this.first = false;
        this.t0 = t;
    }
    this.spring.length = this.length0 + this.ampl * Math.sin( 2*Math.PI * (t-this.t0) / this.T );
}
