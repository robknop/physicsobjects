import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { StandardScene } from "./standardscene.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { SVGPlot } from "./svgplot.js";

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var VertSpringLaunch = function( inparams )
{
    var self = this;
    var geom, mat, mesh;

    this.params = { name: "Veritcal Spring Launcher",
                    nviews: 2,
                    aspect0: 1.25,
                    campos0: new THREE.Vector3( 0., 0., 10. ),
                    camfov0: 60.,
                    camlook0: new THREE.Vector3( 0., 0., 0. ),
                    aspect1: 0.534,
                    campos1: new THREE.Vector3( 0, 7., 28. ),
                    camfov1: 60.,
                    camlook1: new THREE.Vector3( 0., 10., 0. ),
                    equilspringl: 4,
                    springk: 20,
                    platformthick: 0.1,
                    platformwid: 2.0,
                    springrad: 0.4,
                    springrots: 5,
                    springcircrad: 0.1,
                    ballrad: 0.75,
                    ballmass: 1,
                    dx0: -2.5,
                  };
    Object.assign( this.params, inparams );
    StandardScene.call( this, this.params );

    this.springk = this.params.springk;
    this.ballmass = this.params.ballmass;
    this.animcallback = function( t ) { self.update( t ); }

    this.ordinal = 0;
    this.colordex = 0;
    this.plotdt = 0.1;
    this.ydatasets = [];
    this.vdatasets = [];
    this.listhandles = [];
    this.curydataset = null;
    this.curvdataset = null;
    
    const springbottom = 0 - this.params.equilspringl - this.params.platformthick - this.params.ballrad;
    const wellwidth = 3 * this.params.platformwid;
    const floorthick = 0.5 * this.params.platformthick;
    const floorwidth = 4 * wellwidth;
    const wellwallheight = -springbottom - floorthick;
    
    this.spring = new PhysicsObjects.Spring( { length: this.params.equilspringl,
                                               rad: this.params.springrad,
                                               circrad: this.params.springcircrad,
                                               nrots: this.params.springrots,
                                             } );
    // My rx, ry, rz things are a disaster
    this.spring.visobj.rotateZ( Math.PI/2. );
    this.spring.position = { x: 0, y: springbottom, z: 0 };
    this.platform = new PhysicsObjects.Block( { rx: this.params.platformwid/2.,
                                                ry: this.params.platformthick/2.,
                                                rz: this.params.platformwid/2.,
                                                color: 0xc08000,
                                                name: "Spring Platform" } );
    this.platform.position = { x: 0, y: -this.params.platformthick/2. - this.params.ballrad, z: 0 };

    this.ball = new PhysicsObjects.Object( { rx: this.params.ballrad,
                                             ry: this.params.ballrad,
                                             rz: this.params.ballrad,
                                             name: "Ball",
                                             accelfunc: function( t, pos, vel, obj, dt) {
                                                 return self.ballaccel( t, pos, vel, obj, dt );
                                             },
                                           } );
    geom = new THREE.SphereGeometry( this.params.ballrad, 16, 12 );
    mat = new THREE.MeshLambertMaterial( { color: 0x2020c0 } );
    this.ball.visobj = new THREE.Mesh( geom, mat );
    this.ball.visobj.name = this.ball.name;


    // Add a floor at the bottom of the spring
    geom = new THREE.BoxGeometry( wellwidth, floorthick, wellwidth );
    mat = new THREE.MeshLambertMaterial( { color: 0x602000 } );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( 0., springbottom - floorthick/2., 0. );
    this.scene.scene.add( mesh );

    // Well walls are one-sided only looking in
    geom = new THREE.PlaneGeometry( wellwidth, wellwallheight, 1 );
    mat = new THREE.MeshLambertMaterial( { color: 0x602000 } );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( 0, springbottom + wellwallheight/2, -wellwidth/2 );
    this.scene.scene.add( mesh );
    mesh = new THREE.Mesh( geom, mat );
    mesh.rotateY( Math.PI );
    mesh.position.set( 0, springbottom + wellwallheight/2, wellwidth/2 );
    this.scene.scene.add( mesh );
    mesh = new THREE.Mesh( geom, mat );
    mesh.rotateY( Math.PI/2 );
    mesh.position.set( -wellwidth/2, springbottom + wellwallheight/2, 0 );
    this.scene.scene.add( mesh );
    mesh = new THREE.Mesh( geom, mat );
    mesh.rotateY( -Math.PI/2 );
    mesh.position.set( wellwidth/2, springbottom + wellwallheight/2, 0 );
    this.scene.scene.add( mesh );
    
    // Add a floor at y=0
    geom = new THREE.BoxGeometry( floorwidth/2 + wellwidth/2 , floorthick, floorwidth/2 - wellwidth/2 );
    mat = new THREE.MeshLambertMaterial( { color: 0x008000 } );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( floorwidth/4 - wellwidth/4 , -floorthick/2, floorwidth/4 + wellwidth/4 );
    this.scene.scene.add( mesh );
    mesh = new THREE.Mesh( geom, mat );
    mesh.position.set( -floorwidth/4 + wellwidth/4, -floorthick/2, -floorwidth/4 - wellwidth/4 );
    this.scene.scene.add( mesh );
    geom = new THREE.BoxGeometry( floorwidth/2 - wellwidth/2 , floorthick, floorwidth/2 + wellwidth/2 );
    mesh = new THREE.Mesh( geom, mat );    
    mesh.position.set( -floorwidth/4 - wellwidth/4 , -floorthick/2, floorwidth/4 - wellwidth/4 );
    this.scene.scene.add( mesh );
    mesh = new THREE.Mesh( geom, mat );    
    mesh.position.set( floorwidth/4 + wellwidth/4 , -floorthick/2, -floorwidth/4 + wellwidth/4 );
    this.scene.scene.add( mesh );
    
    
    this.scene.addObject( this.spring );
    this.scene.addObject( this.platform );
    this.scene.addObject( this.ball );

    // // for debugging purposes: a box at ground leve, a box at the bottom of the spring
    // const box1 = new THREE.Box3( new THREE.Vector3( -2, -0.05, -2 ),
    //                              new THREE.Vector3( 2, 0.05, 2 ) );
    // const box1obj = new THREE.Box3Helper( box1 );
    // this.scene.scene.add( box1obj );
    // const box2 = new THREE.Box3(
    //     new THREE.Vector3(-2, -this.params.equilspringl-this.params.platformthick-this.params.ballrad-0.05, -2 ),
    //     new THREE.Vector3( 2, -this.params.equilspringl-this.params.platformthick-this.params.ballrad+0.05, 2 ));
    // const box2obj = new THREE.Box3Helper( box2 );
    // this.scene.scene.add( box2obj );
        
}

VertSpringLaunch.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( VertSpringLaunch.prototype, "constructor",
                       {  value: VertSpringLaunch,
                          enumerable: false,
                          writeable: true } );

// **********************************************************************

VertSpringLaunch.colors = [ "#cc0000",
                            "#007000",
                            "#0000cc",
                            "#cc9900",
                            "#33cc00",
                            "#cc00cc" ];

VertSpringLaunch.colornames = [ "Red", "Green", "Blue", "Gold", "Lime", "Purple" ];

// **********************************************************************

VertSpringLaunch.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );

    var self = this;
    
    var controls = {
        type: "box",
        id: "controlsdiv",
        _class: "controlsdiv",
        spec: [
            { type: "box",
              id: "springkbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "springklabel",
                    text: "k: ",
                  },
                  { type: "input",
                    id: "springk",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.springk,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "kunits",
                    text: " N/m",
                  },
              ]
            },
            { type: "box",
              id: "massbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "masslabel",
                    text: "m: ",
                  },
                  { type: "input",
                    id: "mass",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.ballmass,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "massunits",
                    text: " kg",
                  },
              ]
            },
            { type: "box",
              id: "initybox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "initylabel",
                    text: "y₀: ",
                  },
                  { type: "input",
                    id: "inity",
                    width: "6ex",
                    _class: "grow",
                    initval: this.params.dx0,
                    changecallback: function() { self.initialize(); }
                  },
                  { type: "span",
                    id: "y0units",
                    text: " m",
                  },
              ]
            },
            { type: "box",
              id: "resetbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "resetbutton",
                    _class: "grow",
                    text: "Reset",
                    callback: function() { self.initialize(); }
                  }
              ]
            },
            { type: "box",
              id: "gobox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "gobutton",
                    _class: "grow",
                    text: "Go!",
                    callback: function() { self.go(); }
                  }
              ]
            },
            { type: "box",
              id: "stopbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "stopbutton",
                    _class: "grow",
                    text: "Stop!",
                    callback: function() { self.stop(); }
                  }
              ]
            },
            { type: "span",
              id: "spacer0",
              text: " "
            },
            { type: "listwidget",
              id: "datasetlist",
              multiselect: true
            },
            { type: "box",
              id: "deletesetsbuttonbox",
              _class: "hbox margintop",
              spec: [
                  { type: "button",
                    id: "deletesetsbutton",
                    _class: "grow",
                    text: "Delete Sets",
                    callback: function() { self.deleteSelectedSets(); }
                  }
              ]
            }
        ]
    };
    this.controls = InterfaceMaker( controls );
    this.setlist = this.controls[ "datasetlist" ];
    
    this.disablewhilego = [];
    this.disablewhilestop = [];
    for ( let it of [ "springk", "mass", "inity", "resetbutton", "gobutton", "deletesetsbutton" ] ) {
        this.disablewhilego.push( this.controls[ it ] );
    }
    for ( let it of [ "stopbutton" ] ) {
        this.disablewhilestop.push( this.controls[ it ] );
        this.controls[ it ].disabled = true;
    }
        
    this.maindiv.appendChild( this.controls[ "controlsdiv" ] );
    this.initialize();


    // Plots

    var plotwid = 800;
    var plothei = 600;

    var hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    document.body.appendChild( hbox );

    this.yplot = new SVGPlot.Plot( { divid: "yplotdiv",
                                     svgid: "yplotsvg",
                                     ytitle: "y (m)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   }
                                 );
    this.vplot = new SVGPlot.Plot( { divid: "vplotdiv",
                                     svgid: "vyplotdiv",
                                     ytitle: "v_y (m/s)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   }
                                 );
    hbox.appendChild( this.yplot.topdiv );
    hbox.appendChild( this.vplot.topdiv );
    this.yplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

VertSpringLaunch.prototype.go = function()
{
    for ( let widget of this.disablewhilego )
        widget.disabled = true;
    for ( let widget of this.disablewhilestop )
        widget.disabled = false;
    this.ball.initializeState( this.scene.t, this.ball.position, this.ball.velocity, this.ball.acceleration );
    this.scene.addAnimationCallback( this.animcallback );

    this.t0 = this.scene.t;
    this.ordinal += 1;
    this.curydataset = new SVGPlot.Dataset( { name: "Set " + this.ordinal + "(" +
                                                     VertSpringLaunch.colornames[ this.colordex ] + ")",
                                              color: VertSpringLaunch.colors[ this.colordex ],
                                            } );
    this.yplot.addDataset( this.curydataset );
    this.ydatasets.push( this.curydataset );
    this.curvdataset = new SVGPlot.Dataset( { name: "Set " + this.ordinal + "(" +
                                                     VertSpringLaunch.colornames[ this.colordex ] + ")",
                                              color: VertSpringLaunch.colors[ this.colordex ],
                                            } );
    this.vplot.addDataset( this.curvdataset );
    this.vdatasets.push( this.curvdataset );
    this.nextplot = -1e30;
    this.colordex += 1;
    if ( this.colordex >= VertSpringLaunch.colors.length ) this.colordex = 0;
    var handle = this.setlist.addItem( this.curydataset.name );
    this.listhandles.push( handle );
}

// **********************************************************************

VertSpringLaunch.prototype.stop = function()
{
    for ( let widget of this.disablewhilego )
        widget.disabled = false;
    for ( let widget of this.disablewhilestop )
        widget.disabled = true;
    this.scene.removeAnimationCallback( this.animcallback );
    this.yplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

VertSpringLaunch.prototype.ballaccel = function( t, pos, vel, obj, dt ) {
    var acc = { x: 0, y: -9.8, z: 0 };
    if ( pos.y < 0 )
        acc.y += ( this.springk / this.ballmass ) * ( -pos.y );
    // console.log( "Returning acc = " + acc.x + ", " + acc.y + ", " + acc.z );
    return acc;
}

// **********************************************************************

VertSpringLaunch.prototype.update = function( t ) {
    this.ball.updateState( t );
    this.updateSpring();
    if ( t > this.nextplot ) {
        this.curydataset.addPoint( t - this.t0, this.ball.y );
        this.curvdataset.addPoint( t - this.t0, this.ball.vy );
        this.nextplot = t + this.plotdt;
    }
}

// **********************************************************************

VertSpringLaunch.prototype.updateSpring = function() {
    if ( this.ball.y > 0 ) {
        this.spring.length = this.params.equilspringl;
        this.platform.y = -this.params.platformthick/2 - this.params.ballrad;
    }
    else {
        this.spring.length = this.params.equilspringl + this.ball.y;
        this.platform.y = this.ball.y - this.params.platformthick/2 - this.params.ballrad;
    }
}

// **********************************************************************

VertSpringLaunch.prototype.initialize = function() {
    var txt, val;

    txt = this.controls[ "springk" ].value;
    val = parseFloat( txt );
    if ( isNaN( val ) ) {
        window.alert( "Error parsing spring constant \"" + txt + "\" as a number" );
    }
    else {
        if ( val < 1 ) val = 1;
        if ( val > 1e6 ) val = 1e6;
        this.springk = val;
        this.controls[ "springk"].value = val;
    }

    txt = this.controls[ "mass" ].value;
    val = parseFloat( txt );
    if ( isNaN( val ) ) {
        window.alert( "Error parsing mass \"" + txt + "\" as a number" );
    }
    else {
        if ( val < 0.01 ) val = 0.01;
        if ( val > 1e3 ) val = 1e3;
        this.ballmass = val;
        this.controls[ "mass" ].value = val;
    }

    txt = this.controls[ "inity" ].value;
    val = parseFloat( txt );
    if ( isNaN( val ) ) {
        window.aert( "Error parsing y0 \"" + txt + "\" as a number" );
    }
    else {
        if ( val < -0.75 * this.params.equilspringl ) val = -0.75 * this.params.equilspringl;
        if ( val > 0. ) val = 0.;
        this.controls[ "inity" ].value = val;
        this.ball.y = val;
    }

    this.ball.vy = 0;
    this.ball.ay = 0;
    this.updateSpring();
}

// **********************************************************************

VertSpringLaunch.prototype.deleteSelectedSets = function() {
    var sel = this.setlist.getSelectedHandles();
    for ( let del of sel ) {
        let dex = this.listhandles.indexOf( del );
        if ( dex >= 0 ) {
            this.yplot.removeDatasetByIndex( dex );
            this.vplot.removeDatasetByIndex( dex );
            this.listhandles.splice( dex, 1 );
            this.ydatasets.splice( dex, 1 );
            this.vdatasets.splice( dex, 1 );
            this.setlist.removeItem( del );
        }
    }
    this.yplot.redraw();
    this.vplot.redraw();
}

// **********************************************************************

export { VertSpringLaunch };
