// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// Pass the parameter "columns" for the number of columns.  Defaults to 1
// If columns is 0, then it behaves like a list widget; passed/returned items are
// not arrays, but just text.
//
// Add the .table property of the returned object to your document somewhere

// Uses the following CSS classes:
//   tablewidget : main table.
//   tablewidget_unselected : unselected item
//   tablewidget_selected : selected item
// There are some defs for these in tablewidget.css

var TableWidget = function( inparams = {} )
{
    var params = { multiselect: false,
                   columns: 1,
                   headers: null,
                 };
    Object.assign( params, inparams );

    this.ncolumns = params.columns;
    this.islist = false;
    if ( this.ncolumns == 0 ) {
        this.islist = true;
        this.ncolumns = 1;
    }
    this.items = [];
    this.itemtrs = [];
    this.itemordinals = [];
    this.selectedords = [];
    this.ordinal = 0;
    this.multiselect = params.multiselect;

    this.table = document.createElement( "table" );
    this.table.setAttribute( "class", "tablewidget" )

    if ( params.headers != null ) {
        let tr = document.createElement( "tr" );
        tr.setAttribute( "class", "tablewidget_header" );
        this.table.appendChild( tr );
        for ( let i = 0 ; i < params.headers.length ; ++i ) {
            let th = document.createElement( "th" );
            tr.appendChild( th );
            th.appendChild( document.createTextNode( params.headers[i] ) );
        }
    }
    
    this.selectionevent = new Event( 'selectionchanged' );
}

TableWidget.prototype.getItemByHandle = function( ord )
{
    var dex = this.itemordinals.indexOf( ord );
    if ( dex >= 0 ) {
        if ( this.islist )
            return this.items[ dex ][ 0 ];
        else
            return this.items[ dex ];
    }
    else
        return null;
}

TableWidget.prototype.getSelectedHandles = function()
{
    return this.selectedords.slice();
}

TableWidget.prototype.getSelectedItems = function()
{
    var items = [];
    for ( var ord of this.selectedords ) {
        var dex = this.itemordinals.indexOf( ord );
        if ( dex < 0 )
            console.log( "TableWidget.getSelectedItems error: ord " + ord +
                         " in selectedords is not in itemordinals!!!!" );
        else {
            if ( this.islist ) {
                items.push( this.items[ dex ][ 0 ] );
            }
            else {
                items.push( this.items[ dex ] );
            }
        }
    }
    return items;
}

TableWidget.prototype.addItem = function( initem )
{
    var self = this;
    var tr = document.createElement( "tr" );
    var ord = this.ordinal;
    tr.setAttribute( "class", "tablewidget_unselected" );

    var item;
    if ( this.islist )
        item = [ initem ];
    else {
        item = [...initem];
        if ( item.length > this.ncolumns )
            item.splice( this.ncolumns );
        else if ( item.length < this.ncolumns ) {
            for ( let i = item.length ; i < this.ncolumns ; ++i )
                item.push( "" );
        }
    }

    for ( let i = 0 ; i < this.ncolumns ; ++i ) {
        let td = document.createElement( "td" );
        td.appendChild( document.createTextNode( item[i] ) );
        tr.appendChild( td );
    }
                        
    tr.addEventListener( "click", function( ev ) { self.toggleSelect( ev, ord ); } );
    this.table.appendChild( tr );
    this.items.push( item );
    this.itemtrs.push( tr );
    this.itemordinals.push( ord );
    this.ordinal += 1;
    return ord;
}

TableWidget.prototype.removeItem = function( ord )
{
    // Javascript is loosely typed, except when it isn't
    ord = parseInt( ord );
    var dex = this.itemordinals.indexOf( ord );
    if ( dex < 0 ) return;
    this.itemtrs[ dex ].remove();
    this.items.splice( dex, 1 );
    this.itemtrs.splice( dex, 1 );
    this.itemordinals.splice( dex, 1 );
    dex = this.selectedords.indexOf( ord );
    if ( dex >= 0) {
        this.selectedords.splice( dex, 1 );
        this.table.dispatchEvent( this.selectionevent );
    }
}

TableWidget.prototype.select = function( ord )
{
    var selord = this.selectedords.indexOf( ord );
    if ( selord >= 0 ) return;
    
    var changed = false;
    if ( ! this.multiselect ) {
        if ( this.selectedords.length > 0  ) {
            this.selectNone( true );
            changed = true;
        }
    }
    var dex = this.itemordinals.indexOf( ord );
    if ( dex < 0 )
        console.log( "TableWidget.select error: tried to select ord " + ord +
                     " which is not in the list." );
    else {
        this.selectedords.push( ord );
        this.itemtrs[ dex ].setAttribute( "class", "tablewidget_selected" );
        changed = true;
    }
    if ( changed )
        this.table.dispatchEvent( this.selectionevent );
}

TableWidget.prototype.selectNone = function( silent=false )
{
    if ( this.selectedords.length == 0 ) return;
    for ( let orddex = this.selectedords.length-1 ; orddex >= 0 ; orddex -= 1 ) {
        let dex = this.itemordinals.indexOf( this.selectedords[ orddex ] );
        if ( dex < 0 )
            console.log( "TableWidget.select error: ord " + this.selectedords[ orddex ] +
                         " in selectedords is not in itemordinals!!!!" );
        else {
            this.itemtrs[ dex ].setAttribute( "class", "tablewidget_unselected" );
            this.selectedords.splice( orddex, 1 );
        }
    }
    if ( ! silent )
        this.table.dispatchEvent( this.selectionevent );
}

TableWidget.prototype.toggleSelect = function( ev, ord )
{
    var changed = false;
    // Eventually I want to make shift select the range
    if ( ev.shiftKey || ev.ctrlKey ) {
        let seldex = this.selectedords.indexOf( ord )
        if ( seldex >= 0 ) {
            this.selectedords.splice( seldex, 1 );
            let dex = this.itemordinals.indexOf( ord );
            if ( dex >= 0 ) {
                this.itemtrs[ dex ].setAttribute( "class", "tablewidget_unselected" );
            }
            this.table.dispatchEvent( this.selectionevent );
        }
        else {
            this.select( ord );
        }
    }
    else {
        this.selectNone( true );
        this.select( ord );
    }
}

// **********************************************************************

export { TableWidget };
