import { ListWidget } from "./listwidget.js";
import { SVGPlot } from "./svgplot.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var ProjectilePlot = function( projsim, xplot, yplot, inparams )
{
    var self = this;
    this.xplot = xplot;
    this.yplot = yplot;
    
    var params = { "divid": "projectilesvgplotdiv",
                   "svgid": "projectilesvgplotsvg",
                   "linewid": 0,
                   "color": "#cc0000",
                   "marker": "dot",
                   "markercolor": null,
                   "markersize": 16,
                   "markerstrokewid": 0,
                 };
    Object.assign( params, inparams );

    if ( ! ( xplot in ProjectilePlot.plottables ) ) {
        window.alert( "Error, tried to create a projectileplot with unknown x variable." );
        return;
    }
    if ( ! ( yplot in ProjectilePlot.plottables ) ) {
        window.alert( "Error, tried to create a projectileplot with unknown y variable." );
        return;
    }
                                   
    this.projsim = projsim;
    this._svgplot = new SVGPlot.Plot( { "divid": params.divid,
                                        "svgid": params.svgid,
                                        "xtitle": ProjectilePlot.plottables[ xplot ][0],
                                        "minautoxrange": ProjectilePlot.plottables[ xplot ][2],
                                        "ytitle": ProjectilePlot.plottables[ yplot ][0],
                                        "minautoyrange": ProjectilePlot.plottables[ yplot ][2],
                                        "width": 1024,
                                        "height": 512
                                      } );
    this.dataset = new SVGPlot.Dataset( { "linewid": params.linewid,
                                          "color": params.color,
                                          "marker": params.marker,
                                          "markercolor": params.markercolor,
                                          "markersize": params.markersize,
                                          "markerwtrokewid": params.markerstrokewid
                                        } );
    this._svgplot.addDataset( this.dataset );

    this.datalistener = function( values ) { self.newData( values ); };
    this.projsim.addDataListener( this.datalistener );
}

Object.defineProperty( ProjectilePlot.prototype, "elem", {
    get() { return this._svgplot.topdiv }
} );

ProjectilePlot.plottables = {
    "time": [ "Flight Time (s)", "t (s)", 1.0 ],
    "x0": [ "Initial x (m)", "x₀ (m)", 1.0 ],
    "y0": [ "Initial y (m)", "y₀ (m)", 1.0 ],
    "z0": [ "Initial z (m)", "z₀ (m)", 1.0 ],
    "xf": [ "Final x (m)", "x (m)", 1.0 ],
    "yf": [ "Final y (m)", "y (m)", 1.0 ],
    "zf": [ "Final z (m)", "z (m)", 1.0 ],
    "ymax": [ "Max. y (m)", "ymax (m)", 1.0 ],
    "range": [ "Horizontal Range (m)", "range (m)", 1.0 ],
    "height": [ "Max. Height Above Launch (m)", "height (m)", 1.0 ],
    "theta": [ "Launch angle (⁰)", "θ (⁰)", 5.0 ],
    "phi": [ "Orientation (E=0, N=90) (⁰)", "φ (⁰)", 5.0 ],
    "thetaf": [ "Landing angle (⁰)", "θland (⁰)", 5.0 ],
    "phif": [ "Landing Orientation (N=0, E=90) (⁰)", "φland (⁰)", 5.0 ],
    "v0": [ "Muzzle Speed (m/s)", "v₀ (m/s)", 1.0 ],
    "v": [ "Final Speed (m/s)", "v (m/s)", 1.0 ]
};

// **********************************************************************

ProjectilePlot.extractValue = function( values, which )
{
    if ( which == "time" ) {
        return values.t;
    }
    else if ( which == "x0" ) {
        return values.x0;
    }
    else if ( which == "y0" ) {
        return values.y0;
    }
    else if ( which == "z0" ) {
        return values.z0;
    }
    else if ( which == "xf" ) {
        return values.x;
    }
    else if ( which=="yf" ) {
        return values.y;
    }
    else if ( which=="zf" ) {
        return values.z;
    }
    else if ( which == "ymax" ) {
        return values.ymax;
    }
    else if ( which == "range" ) {
        return Math.sqrt( (values.x - values.x0)**2 + (values.z - values.z0)**2 );
    }
    else if ( which == "height" ) {
        return values.ymax - values.y0;
    }
    else if ( which == "theta" ) {
        return 180/Math.PI * Math.atan2( values.vy0, Math.sqrt( values.vx0**2 + values.vz0**2 ) );
    }
    else if ( which == "phi" ) {
        return 180/Math.PI * Math.atan2( -values.vz0, values.vx0 );
    }
    else if ( which == "thetaf" ) {
        return 180/Math.PI * Math.atan2( values.vy, Math.sqrt( values.vx**2 + values.vz**2 ) );
    }
    else if ( which == "phif" ) {
        return 180/Math.PI * Math.atan2( -values.vz, values.vx );
    }
    else if ( which == "v0" ) {
        return Math.sqrt( values.vx0**2 + values.vy0**2 + values.vz0**2 );
    }
    else if ( which == "v") {
        return Math.sqrt( values.vx**2 + values.vy**2 + values.vz**2 );
    }
}

// **********************************************************************

ProjectilePlot.prototype.redraw = function()
{
    this._svgplot.redraw();
}

// **********************************************************************

ProjectilePlot.prototype.newData = function( values )
{
    var x = ProjectilePlot.extractValue( values, this.xplot );
    var y = ProjectilePlot.extractValue( values, this.yplot );
    console.log( "Adding point ( " + x + ", " + y + ") to plot." );
    this.dataset.addPoint( x, y, values.name );
    this._svgplot.redraw();
}

// **********************************************************************

ProjectilePlot.prototype.removeData = function( names )
{
    for ( var name of names )
        this.dataset.removePoint( name );
    this._svgplot.redraw();
}

// **********************************************************************

export { ProjectilePlot };
