// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// Uses the following CSS classes:
//   listwidget : main div.
//       hardcodes display, flex-direction, and overflow
//   listwidget_unselected : unselected item
//   listwidget_selected : selected item
// There are some defs for these in listwidget.css

var ListWidget = function( inparams = {} )
{
    var params = { multiselect: false };
    Object.assign( params, inparams );

    this.items = [];
    this.itemdivs = [];
    this.itemordinals = [];
    this.selectedords = [];
    this.ordinal = 0;
    this.multiselect = params.multiselect;

    this.div = document.createElement( "div" );
    this.div.setAttribute( "class", "listwidget" )
    this.div.style.display = "flex";
    this.div.style["flex-direction"] = "column";
    this.div.style["overflow"] = "auto";

    this.selectionevent = new Event( 'selectionchanged' );
}

ListWidget.prototype.getItemByHandle = function( ord )
{
    var dex = this.itemordinals.indexOf( ord );
    if ( dex >= 0 )
        return this.items[ dex ];
    else
        return null;
}

ListWidget.prototype.getSelectedHandles = function()
{
    return this.selectedords.slice();
}

ListWidget.prototype.getSelectedItems = function()
{
    var items = [];
    for ( var ord of this.selectedords ) {
        var dex = this.itemordinals.indexOf( ord );
        if ( dex < 0 )
            console.log( "ListWidget.getSelectedItems error: ord " + ord +
                         " in selectedords is not in itemordinals!!!!" );
        else
            items.push( this.items[ dex ] );
    }
    return items;
}

ListWidget.prototype.addItem = function( item )
{
    var self = this;
    var div = document.createElement( "div" );
    var ord = this.ordinal;
    div.setAttribute( "class", "listwidget_unselected" );
    div.appendChild( document.createTextNode( item ) );
    div.addEventListener( "click", function( ev ) { self.toggleSelect( ev, ord ); } );
    this.div.appendChild( div );
    this.items.push( item );
    this.itemdivs.push( div );
    this.itemordinals.push( ord );
    this.ordinal += 1;
    return ord;
}

ListWidget.prototype.removeItem = function( ord )
{
    // Javascript is loosely typed, except when it isn't
    ord = parseInt( ord );
    var dex = this.itemordinals.indexOf( ord );
    if ( dex < 0 ) return;
    this.itemdivs[ dex ].remove();
    this.items.splice( dex, 1 );
    this.itemdivs.splice( dex, 1 );
    this.itemordinals.splice( dex, 1 );
    dex = this.selectedords.indexOf( ord );
    if ( dex >= 0) {
        this.selectedords.splice( dex, 1 );
        this.div.dispatchEvent( this.selectionevent );
    }
}

ListWidget.prototype.select = function( ord )
{
    var selord = this.selectedords.indexOf( ord );
    if ( selord >= 0 ) return;
    
    var changed = false;
    if ( ! this.multiselect ) {
        if ( this.selectedords.length > 0  ) {
            this.selectNone( true );
            changed = true;
        }
    }
    var dex = this.itemordinals.indexOf( ord );
    if ( dex < 0 )
        console.log( "ListWidget.select error: tried to select ord " + ord +
                     " which is not in the list." );
    else {
        this.selectedords.push( ord );
        this.itemdivs[ dex ].setAttribute( "class", "listwidget_selected" );
        changed = true;
    }
    if ( changed )
        this.div.dispatchEvent( this.selectionevent );
}

ListWidget.prototype.selectNone = function( silent=false )
{
    if ( this.selectedords.length == 0 ) return;
    for ( let orddex = this.selectedords.length-1 ; orddex >= 0 ; orddex -= 1 ) {
        let dex = this.itemordinals.indexOf( this.selectedords[ orddex ] );
        if ( dex < 0 )
            console.log( "ListWidget.select error: ord " + this.selectedords[ orddex ] +
                         " in selectedords is not in itemordinals!!!!" );
        else {
            this.itemdivs[ dex ].setAttribute( "class", "listwidget_unselected" );
            this.selectedords.splice( orddex, 1 );
        }
    }
    if ( ! silent )
        this.div.dispatchEvent( this.selectionevent );
}

ListWidget.prototype.toggleSelect = function( ev, ord )
{
    var changed = false;
    // Eventually I want to make shift select the range
    if ( ev.shiftKey || ev.ctrlKey ) {
        let seldex = this.selectedords.indexOf( ord )
        if ( seldex >= 0 ) {
            this.selectedords.splice( seldex, 1 );
            let dex = this.itemordinals.indexOf( ord );
            if ( dex >= 0 ) {
                this.itemdivs[ dex ].setAttribute( "class", "listwidget_unselected" );
            }
            this.div.dispatchEvent( this.selectionevent );
        }
        else {
            this.select( ord );
        }
    }
    else {
        this.selectNone( true );
        this.select( ord );
    }
}

// **********************************************************************

export { ListWidget };
