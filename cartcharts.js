import { SVGPlot } from  "./svgplot.js"

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var CartCharts = {};

CartCharts.colors = [ "#cc0000",
                      "#007000",
                      "#0000cc",
                      "#cc9900",
                      "#33cc00",
                      "#cc00cc" ];


// **********************************************************************
// **********************************************************************
// **********************************************************************
// I make the assumption that nobody is going to add datasets to my charts
//   other than me, so that I can just keep track myself of indexes without
//   having to go and ask the charts what datasets they have in what order.

CartCharts.ChartCollection = function( poschart=true, velchart=true, accchart=true )
{
    this.hasposchart = poschart;
    this.hasvelchart = velchart;
    this.hasaccchart = accchart;
    if ( ! ( poschart || velchart || accchart ) ) {
        window.alert( "What is the point of making a ChartCollection with no charts?" );
        return;
    }
    if ( poschart )
        this.poschart = new CartCharts.chart( "position" );
    if ( velchart )
        this.velchart = new CartCharts.chart( "velocity" );
    if ( accchart )
        this.accchart = new CartCharts.chart( "acceleration" );

    this.datasets = [];
    this.colordex = 0;
    this.ordinal = 0;
}

// **********************************************************************

CartCharts.ChartCollection.prototype.redraw = function()
{
    if ( this.hasposchart )
        this.poschart.redraw();
    if ( this.hasvelchart )
        this.velchart.redraw();
    if ( this.hasaccchart )
        this.accchart.redraw();
}

// **********************************************************************

CartCharts.ChartCollection.prototype.addDataSet = function( cart, color=undefined, name=undefined )
{
    this.ordinal += 1;

    if ( color == undefined ) {
        color = CartCharts.colors[ this.colordex ];
        this.colordex += 1;
        if ( this.colordex >= CartCharts.colors.length )
            this.colordex = 0;
    }

    if ( name == undefined )
        name = "Set " + this.ordinal;
    
    var posset = null;
    var velset = null;
    var accset = null;
    if ( this.hasposchart )
        posset = this.poschart.addDataSet( cart, { "color": color, "name": name } );
    if ( this.hasvelchart )
        velset = this.velchart.addDataSet( cart, { "color": color, "name": name } );
    if ( this.hasaccchart )
        accset = this.accchart.addDataSet( cart, { "color": color, "name": name } );

    var datasetinfo = { "name": name,
                        "color": color,
                        "posset": posset,
                        "velset": velset,
                        "accset": accset };
    this.datasets.push( datasetinfo );
    return datasetinfo;
}

// **********************************************************************

CartCharts.ChartCollection.prototype.removeDataSet = function( dex )
{
    if ( this.hasposchart )
        this.poschart.removeDatasetByIndex( dex );
    if ( this.hasvelchart )
        this.velchart.removeDatasetByIndex( dex );
    if ( this.hasaccchart )
        this.accchart.removeDatasetByIndex( dex );
    this.datasets.splice( dex, 1 );
}

// **********************************************************************

CartCharts.ChartCollection.prototype.startRecording = function( cart, color=undefined, name=undefined )
{
    var dataset = this.addDataSet( cart, color, name );
    if ( this.hasposchart )
        this.poschart.startRecording( dataset.posset );
    if ( this.hasvelchart )
        this.velchart.startRecording( dataset.velset );
    if ( this.hasaccchart )
        this.accchart.startRecording( dataset.accset );
}

// **********************************************************************

CartCharts.ChartCollection.prototype.stopRecording = function()
{
    if ( this.hasposchart )
        this.poschart.stopRecording();
    if ( this.hasvelchart )
        this.velchart.stopRecording();
    if ( this.hasaccchart )
        this.accchart.stopRecording();
}


// **********************************************************************
// **********************************************************************
// **********************************************************************

CartCharts.chart = function( type="position" )
{
    var self = this;

    var chartparams = { "divid": type + "-chart",
                        "svgid": type + "-svg",
                        "xtitle": "t (seconds)",
                        "width": 800,
                        "height": 600,
                        "left": 100,
                        "right": 10,
                        "axislabelsize": 24,
                        "axistitlesize": 28,
                        "titlesize": 28,
                      };

    this.ispos = false;
    this.isvel = false
    this.isacc = false;
    
    if ( type == "position" ) {
        chartparams.title = "Cart Position On Track";
        chartparams.ytitle = "x (cm)";
        this.ispos = true;
    }
    else if ( type == "velocity" ) {
        chartparams.title = "Cart Velocity Along Track";
        chartparams.ytitle = "v (cm/s)";
        this.isvel = true;
    }
    else if ( type == "acceleration" ) {
        chartparams.title = "Cart Acceleration Along Track";
        chartparams.ytitle = "a (cm/s²)";
        this.isacc = true;
    }
    else {
        window.alert( "Error, unknown chart type " + type );
        return null;
    }
    
    this.chart = new SVGPlot.Plot( chartparams  );
    this.datasets = [];

    this.ordinal = 0;
    this.colordex = 0;

    this.recordingdt = 0.1;
    this.recording = false;
    this.recordingsets = [];
    this.t0 = 0;
}

// **********************************************************************

CartCharts.chart.prototype.addDataSet = function( cart, inparams )
{
    var self = this;

    this.ordinal += 1;

    var params = { "color": null,
                   "name": null };
    Object.assign( params, inparams );
    var color = params.color;
    if ( color == null ) {
        color = CartCharts.colors[ this.colordex ];
        this.colordex += 1;
        if ( this.colordex >= CartCharts.colors.length ) {
            this.colordex = 0;
        }
    }
    var name = params.name;
    if ( name == null ) {
        name = "Set " + this.ordinal;
    }
        
        
    var dataset = new SVGPlot.Dataset( { "name": name, "color": color } );
    this.chart.addDataset( dataset );
    var datasetobject = { "cart": cart,
                          "dataset": dataset };
    this.datasets.push( datasetobject );
    return datasetobject;
}

// **********************************************************************

CartCharts.chart.prototype.removeDatasetByIndex = function( dex )
{
    this.chart.removeDatasetByIndex( dex );
}

// **********************************************************************

CartCharts.chart.prototype.startRecording = function( datasetobject )
{
    var self = this;
    var cart = datasetobject.cart;
    
    console.log( "Starting to record with t0 = " + cart.parent.t );
    this.t0 = cart.parent.t;
    this.recording = true;
    this.recordingsets.push( datasetobject );
    this.timeout = window.setTimeout( function() { self.record() }, self.recordingdt*1000 );
}

// **********************************************************************

CartCharts.chart.prototype.stopRecording = function()
{
    this.recording = false;
    this.recordingsets = [];
    window.clearTimeout( this.timeout );
    this.redraw();
}

CartCharts.chart.prototype.redraw = function()
{
    this.chart.redraw();
}

// **********************************************************************

CartCharts.chart.prototype.record = function()
{
    var self = this;

    for ( var setobj of this.recordingsets )
    {
        var t = setobj.cart.t - this.t0;
        var val = 0;
        if ( this.ispos )
            val = setobj.cart.x;
        else if ( this.isvel )
            val = setobj.cart.vx;
        else if ( this.isacc )
            val = setobj.cart.ax;
        else {
            console.log( "Unknown chart type " + this.type + ", not plotting." );
            return;
        }
        setobj.dataset.addPoint( t, val );
    }

    this.timeout = window.setTimeout( function() { self.record() }, self.recordingdt*1000 );
}


// **********************************************************************

export { CartCharts };
