import * as THREE from "./lib/three.module.js";
import { GLTFLoader } from './lib/GLTFLoader.js';

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// console.log( "Loading PhysicsObjects" );

var PhysicsObjects = {};

PhysicsObjects.baseurl = "/physicsobjects_test/";
// PhysicsObjects.baseurl = "/physicsobjects/";

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Object
//
// A base physics object has the following properties:
//   parent  — parent PhysicsObjects.Object.  Note that the object will NOT be added to the parent you must do that.
//   visobj — visualization object, a three.js object
//   visobjfac — multiply position by this factor to get relative visobj position
//   visoboffset — add this to visobjfac * position to get visobj position; has properties x, y, z
//     WARNING : visobjfac and visobjoffset are currently horribly broken
//   name — a String
//   mass — single number
//   bbox — { x0: <num>, y0: <num>, z0: <num>, x1: <num>, y1: <num>, z1: <num> }
//      bounding box **relative to center of object** (so the 0's will usually be negative)
//   rx, ry, rz — if bbox isn't specified (or is null), these are "radii", and the bbox will
//      be these distances from the center.
//   position — sub properties .x, .y, .z, kept synced with visobj.position
//   velocity — sub properties .x, .y, .z
//   acceleration — sub properties .x, .y, .z
//   x, y, z — gets position.x, .y, and .z
//   vx, vy, vz — gets velocity.x, .y., and .z
//   ax, ay, az — gets acceleration.x, .y, and .z
//   t — Time ( s ) of position, velocity, acceleration
//
//   accelfunc - f( t, position, velocity, this, dt ), returns a vector a (with .x, .y, and .z components)
//     Used with the Runge Kutte solver in updateState
//
//   oldpos — a previous position ( .x, .y, .z )
//   oldvel — a previous velocity ( .x, .y, .z )
//   oldt — time of oldpos, oldvel
//
// PhysicsObjects.Object can have child instances of
// PhysicsObject.Object.  If anything is going to be shown, at the top
// level there needs to be a PhysicsObjects.Scene.


PhysicsObjects._objdex = 0;

PhysicsObjects.Object = function( inprops = { } )
{
    var props = {
        "name": null,
        "parent": null,
        "visobj": null,
        "mass": 1,
        "x": 0,
        "y": 0,
        "z": 0,
        "vx": 0,
        "vy": 0,
        "vz": 0,
        "ax": 0,
        "ay": 0,
        "az": 0,
        "visobjfac": 1,
        "visobjoffset": { x: 0, y: 0, z: 0 },
        "rx": 1,
        "ry": 1,
        "rz": 1,
        "bbox": null,
        "xlow": null,        
        "xlow": null,
        "xhigh": null,
        "ylow": null,
        "yhigh": null,
        "zlow": null,
        "zhigh": null,
        "accelfunc": null
    };
    Object.assign( props, inprops );

    this.name = props.name;;
    if ( this.name == null )
        this.name = "Object " + PhysicsObjects._objdex;
    PhysicsObjects._objdex += 1;

    this.visobj = props.visobj;
    if ( this.visobj != null )
        this.visobj.name = this.name;

    this.parent = props.parent;
    this.visobjfac = props.visobjfac;
    this.visobjoffset = props.visobjoffset
    this._position = { x: props.x, y: props.y, z: props.z };
    this._velocity = { x: props.vx, y: props.vy, z: props.vz };
    this._acceleration = { x: props.ax, y: props.ay, z: props.az };
    this._accelfunc = props.accelfunc;
    this._mass = props.mass;
    
    if ( props.bbox == null ) {
        this._bbox = { x0: -props.rx,
                       x1:  props.rx,
                       y0: -props.ry,
                       y1:  props.ry,
                       z0: -props.rz,
                       z1:  props.rz }
    }
    else {
        this._bbox = {};
        Object.assign( this._bbox, props.bbox );
    }
    
    this.masslisteners = [];
    this.bboxlisteners = [];
    
    if ( this.visobj != null )
        this.position = this._position;
    
    //  if ( this.parent != null )
    //     this.parent.addObject( this );

    this.oldpos = { x: this._position.x, y: this._position.y, z: this._position.z };
    this.oldvel = { x: this._velocity.x, y: this._velocity.y, z: this._velocity.z };
    if ( this.parent != null )
        this._t = this.parent.t;
    else
        this._t = 0.;
    this.oldt = this._t;

    this.children = [];
};

// ****************************************

Object.defineProperty( PhysicsObjects.Object.prototype, "t", {
    get() { return this._t },
    set( t ) { this._t = t }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "position", {
    get( ) { return this._position },
    set( pos ) {
        // console.log( " In position setter; pos = " + pos.x + ", " + pos.y + ", " + pos.z );
        // console.log( " this.visobjfac = " + this.visobjfac );
        // console.log( " this.visobjoffset = " + this.visobjoffset.x + ", " +
        //              this.visobjoffset.y + ", " + this.visobjoffset.z );
        this._position.x = pos.x;
        this._position.y = pos.y;
        this._position.z = pos.z;
        if ( this.visobj != null )
        {
            this.visobj.position.x = this.visobjfac * pos.x + this.visobjoffset.x;
            this.visobj.position.y = this.visobjfac * pos.y + this.visobjoffset.y;
            this.visobj.position.z = this.visobjfac * pos.z + this.visobjoffset.z;
            // console.log( "Set position of " + this.name + " to " +
            //              this.visobj.position.x + ", " + this.visobj.position.y + ", " + this.visobj.position.z );
        }
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "x", {
    get( ) { return this._position.x; },
    set( x ) {
        this._position.x = x;
        if ( this.visobj != null )
            this.visobj.position.x = this.visobjfac * x + this.visobjoffset.x;
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "y", {
    get( ) { return this._position.y; },
    set( y ) {
        this._position.y = y;
        if ( this.visobj != null )
            this.visobj.position.y = this.visobjfac * y + this.visobjoffset.y;
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "z", {
    get( ) { return this._position.z; },
    set( z ) {
        this._position.z = z;
        if ( this.visobj != null )
            this.visobj.position.z = this.visobjfac * z + this.visobjoffset.z;
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "velocity", {
    get() { return this._velocity },
    set( vel ) {
        this._velocity.x = vel.x
        this._velocity.y = vel.y;
        this._velocity.z = vel.z;
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "vx", {
    get( ) { return this._velocity.x; },
    set( vx ) { this._velocity.x = vx; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "vy", {
    get( ) { return this._velocity.y; },
    set( vy ) { this._velocity.y = vy; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "vz", {
    get( ) { return this._velocity.z; },
    set( vz ) { this._velocity.z = vz; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "acceleration", {
    get() { return this._acceleration },
    set( acc ) {
        this._acceleration.x = acc.x
        this._acceleration.y = acc.y;
        this._acceleration.z = acc.z;
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "ax", {
    get( ) { return this._acceleration.x; },
    set( ax ) { this._acceleration.x = ax; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "ay", {
    get( ) { return this._acceleration.y; },
    set( ay ) { this._acceleration.y = ay; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "az", {
    get( ) { return this._acceleration.z; },
    set( az ) { this._acceleration.z = az; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "accelfunc", {
    get( ) { return this._accelfunc; },
    set( func ) { this._accelfunc = func; }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "mass", {
    get( ) { return this._mass; },
    set( val ) {
        this._mass = val;
        for ( func of this.masslisteners )
            func( this, this._mass );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "bbox", {
    get( ) { return this._bbox },
    set( val ) {
        this._bbox.x0 = val.x0;
        this._bbox.x1 = val.x1;
        this._bbox.y0 = val.y0;
        this._bbox.y1 = val.y1;
        this._bbox.z0 = val.z0;
        this._bbox.z1 = val.z1;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "x0", {
    get( ) { return this._bbox.x0 },
    set( val ) {
        this._bbox.x0 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "x1", {
    get( ) { return this._bbox.x1 },
    set( val ) {
        this._bbox.x1 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "y0", {
    get( ) { return this._bbox.y0 },
    set( val ) {
        this._bbox.y0 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "y1", {
    get( ) { return this._bbox.y1 },
    set( val ) {
        this._bbox.y1 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "z0", {
    get( ) { return this._bbox.z0 },
    set( val ) {
        this._bbox.z0 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );

Object.defineProperty( PhysicsObjects.Object.prototype, "z1", {
    get( ) { return this._bbox.z1 },
    set( val ) {
        this._bbox.z1 = val;
        for ( func of this.bboxlisteners )
            func( this, this._bbox );
    }
} );


// ****************************************
// Override this to have something called whenever the object is added as a child to another object

PhysicsObjects.Object.prototype.onAdd = function()
{
}

// ****************************************
// Override this to have something called whenever the object is removed from a child from another ogject

PhysicsObjects.Object.prototype.onRemove = function()
{
}

// ****************************************

PhysicsObjects.Object.prototype.addMassListener = function( func )
{
    if ( ! this.masslisteners.includes( func ) )
        this.masslisteners.push( func );
}

PhysicsObjects.Object.prototype.removeMassListener = function( func )
{
    var dex = this.masslisteners.indexOf( func );
    if ( dex >= 0 )
        this.masslisteners.splice( dex, 1 );
}

PhysicsObjects.Object.prototype.addBBoxListener = function( func )
{
    if ( ! this.bboxlisteners.includes( func ) )
        this.bboxlisteners.push( func );
}

PhysicsObjects.Object.prototype.removeBBoxListener = function( func )
{
    var dex = this.bboxlisteners.indexOf( func );
    if ( dex >= 0 )
        this.bboxlisteners.splice( dex, 1 );
}

// ****************************************
// Add a child object

PhysicsObjects.Object.prototype.addObject = function( obj )
{
    // Only add it if it's not already here
    if ( this.children.indexOf( obj ) < 0 )
    {
        if ( obj.parent != null )
            obj.parent.removeObject( obj );
        obj.parent = this;
        this.children.push( obj );
        if ( ( this.visobj != null ) && ( obj.visobj != null ) )
        {
            this.visobj.add( obj.visobj );
        }
        obj.t = this.t;
        obj.onAdd();
        return true;
    }
    else return false;
};

// ****************************************
// Remove a child object

PhysicsObjects.Object.prototype.removeObject = function( obj )
{
    var i = this.children.indexOf( obj );
    if ( i >= 0 )
    {
        obj.onRemove();
        obj.parent = null;
        this.children.splice( i, 1 );
        if ( ( this.visobj != null ) && ( obj.visobj != null ) )
        {
            this.visobj.remove( obj.visobj );
        }
        return true;
    }
    else return false;
}

// ****************************************
// Orient the natural y-axis of the object along the given vector

PhysicsObjects.Object.prototype.setYAxisDirection = function( x, y, z )
{
    var theta = 0;
    var phi = 0;
    var mag = Math.sqrt( x*x + y*y + z*z );
    if ( mag !=0 ) {
        theta = Math.acos( y / mag );
        phi = Math.atan2( x, z );
    }
    var euler = new THREE.Euler( theta, phi, 0., "YXZ" );
    this.visobj.setRotationFromEuler( euler );
}


// ****************************************
// Initialize state of object
// pos, vel, acc all need to have properties x, y, z

PhysicsObjects.Object.prototype.initializeState = function( t, pos, vel, acc )
{
    this.t = t;
    for ( let prop of [ "x", "y", "z" ] ) {
        this.position[prop] = pos[prop];
        this.velocity[prop] = vel[prop];
        this.acceleration[prop] = acc[prop];
    };
    this.curStateToOld();
}

// ****************************************
// Copy current and old state from another object, maybe with a position offset

PhysicsObjects.Object.prototype.copyState = function( obj, offset={ x:0, y:0, z:0 } )
{
    this.position.x = obj.position.x + offset.x;
    this.position.y = obj.position.y + offset.y;
    this.position.z = obj.position.z + offset.z;
    this.velocity = obj.velocity;
    this.acceleration = obj.acceleration;
    this._t = obj.t;
    this.oldpos.x = obj.oldpos.x + offset.x;
    this.oldpos.y = obj.oldpos.y + offset.y;
    this.oldpos.z = obj.oldpos.z + offset.z;
    Object.assign( this.oldvel, obj.oldvel )
    this.oldt = obj.oldt;
}

// ****************************************
// Push current state to old

PhysicsObjects.Object.prototype.curStateToOld = function()
{
    Object.assign( this.oldpos, this._position );
    Object.assign( this.oldvel, this._velocity );
    this.oldt = this._t;
};

// ****************************************
// Update state with RK4 using supplied accel func.
//
// If the object's parent is a World, call the
// world's edge detection function

PhysicsObjects.Object.prototype.updateState = function( t=null )
{
    var self = this;
    this.curStateToOld();

    if ( t == null ) {
        if ( this.parent == null )
            t = new Date().getTime() / 1000;
        else
            t = this.parent.t
    }

    var dt = t - this.oldt;

    if ( this.mass <= 0 || this._accelfunc == null ) {
        // Not gonna do general relativity nor condensed dark energy
        this.position.x = this.oldpos.x + this.velocity.x * dt;
        this.position.y = this.oldpos.y + this.velocity.y * dt;
        this.position.z = this.oldpos.z + this.velocity.z * dt;
        return;
    }
    
    var a, v, x

    var accelfunc = this._accelfunc;
    // console.log( "Object: " + this.name );
    // console.log( "Parent: " + this.parent.name );
    // console.log( "Is PhysicsObjects.Object: " + (this.parent instanceof PhysicsObjects.Object).toString() );
    // console.log( "Is PhysicsObjects.World: " + (this.parent instanceof PhysicsObjects.World).toString() );
    // console.log( "Is PhysicsObjects.Scene: " + (this.parent instanceof PhysicsObjects.Scene).toString() );
    if ( this.parent instanceof PhysicsObjects.World ) {
        accelfunc = function( t, pos, vel, obj, dt ) {
            let acc = self._accelfunc( t, pos, vel, self, dt );
            let addacc = self.parent.edgeAccel( pos, self )
            acc.x += addacc.x;
            acc.y += addacc.y;
            acc.z += addacc.z;
            return acc;
        }
    }
    
    var c1v = accelfunc( this.oldt, this.oldpos, this.oldvel, this, dt );
    var c1x = { x: this.oldvel.x , y: this.oldvel.y , z: this.oldvel.z  };
    v = { x: this.oldvel.x + c1v.x * dt/2. ,
          y: this.oldvel.y + c1v.y * dt/2. ,
          z: this.oldvel.z + c1v.z * dt/2. };
    x = { x: this.oldpos.x + c1x.x * dt/2. ,
          y: this.oldpos.y + c1x.y * dt/2. ,
          z: this.oldpos.z + c1x.z * dt/2. };
    var c2v = accelfunc( this.oldt + dt/2., x, v, this, dt );
    var c2x = { x: v.x , y: v.y , z: v.z  };
    v = { x: this.oldvel.x + c2v.x * dt/2. ,
          y: this.oldvel.y + c2v.y * dt/2. ,
          z: this.oldvel.z + c2v.z * dt/2. };
    x = { x: this.oldpos.x + c2x.x * dt/2. ,
          y: this.oldpos.y + c2x.y * dt/2. ,
          z: this.oldpos.z + c2x.z * dt/2. };
    var c3v = accelfunc( this.oldt + dt/2., x, v, this, dt );
    var c3x = { x: v.x , y: v.y , z: v.z  };
    v = { x: this.oldvel.x + c3v.x * dt ,
          y: this.oldvel.y + c3v.y * dt ,
          z: this.oldvel.z + c3v.z * dt };
    x = { x: this.oldpos.x + c3x.x * dt ,
          y: this.oldpos.y + c3x.y * dt ,
          z: this.oldpos.z + c3x.z * dt };
    var c4v = accelfunc( this.oldt + dt, x, v, this, dt );
    var c4x = { x: v.x , y: v.y , z: v.z  };

    this.x  = this.oldpos.x + dt/6. * ( c1x.x + 2*c2x.x + 2*c3x.x + c4x.x );
    this.y  = this.oldpos.y + dt/6. * ( c1x.y + 2*c2x.y + 2*c3x.y + c4x.y );
    this.z  = this.oldpos.z + dt/6. * ( c1x.z + 2*c2x.z + 2*c3x.z + c4x.z );
    this.vx = this.oldvel.x + dt/6. * ( c1v.x + 2*c2v.x + 2*c3v.x + c4v.x );
    this.vy = this.oldvel.y + dt/6. * ( c1v.y + 2*c2v.y + 2*c3v.y + c4v.y );
    this.vz = this.oldvel.z + dt/6. * ( c1v.z + 2*c2v.z + 2*c3v.z + c4v.z );

    // Since we store acceleration as state, gotta evaluate this again
    // If I were a good boy, I would cache this to avoid the extra evaluation.
    a = accelfunc( t, this.position, this.velocity, this.mass, dt );
      
    this.acceleration.x = a.x;
    this.acceleration.y = a.y;
    this.acceleration.z = a.z;
    this._t = t;

    // Special case handling for the edge.  The PhysicsWorld object
    //   should not do anything here if it did something above
    //   when we called edgeAccel
    // console.log( "in UpdateState for object "  + this.name + "; Parent is " + this.parent );
    if ( this.parent instanceof PhysicsObjects.World ) {
        // console.log( "Running edgeDetect" );
        this.parent.edgeDetect( this )
    }
}

// ****************************************
// This is for manually moving the object.
//   It tries to figure out the velocity
//   and acceleration if it's been long
//   enough since the last time
//   we saved positon and velocity to old

PhysicsObjects.Object.prototype.startManualMove = function( t )
{
    this.oldmanualpos = { x: this.x, y: this.y, z: this.z };
    this.oldmanualvel = { x: this.vx, y: this.vy, z: this.vz };
    this.oldmanualt = t;
}

PhysicsObjects.Object.prototype.moveTo = function( t, x, y, z )
{
    // console.log("Moving " + this.name + " to (" + x + ", " + y + ", " + z + ") at t = " + t);
    const mindt = 0.01;
    
    if ( this.parent != null )
    {
        if ( x + this._bbox.x0 < this.parent.x0 ) x = this.parent.x0 - this._bbox.x0;
        if ( x + this._bbox.x1 > this.parent.x1 ) x = this.parent.x1 - this._bbox.x1;
        if ( y + this._bbox.y0 < this.parent.y0 ) y = this.parent.y0 - this._bbox.y0;
        if ( y + this._bbox.x1 > this.parent.y1 ) y = this.parent.y1 - this._bbox.y1;
        if ( z + this._bbox.z0 < this.parent.z0 ) z = this.parent.z0 - this._bbox.z0;
        if ( z + this._bbox.z1 > this.parent.z1 ) z = this.parent.z1 - this._bbox.z1;
    }
    
    this.position = { x: x, y: y, z: z };
    this._t = t;

    var dt = t - this.oldmanualt;
    if ( dt > mindt )
    {
        // console.log( "Calculation: " + ( ( x - this.oldmanualpos.x ) / dt ) );
        this.velocity.x = ( x - this.oldmanualpos.x ) / dt;
        // console.log( "this.velocity.x: " + this.velocity.x );
        this.velocity.y = ( y - this.oldmanualpos.y ) / dt;
        this.velocity.z = ( z - this.oldmanualpos.z ) / dt;
        this.acceleration.x = ( this.velocity.x - this.oldmanualvel.x ) / dt;
        this.acceleration.y = ( this.velocity.y - this.oldmanualvel.y ) / dt;
        this.acceleration.z = ( this.velocity.z - this.oldmanualvel.z ) / dt;
        
        // console.log( "Old manual position = " + this.oldmanualpos.x  );
        // console.log( "Updating state with dt = " + dt + " to pos=" + x + ", vel=(" + this.velocity.x );

        this.curStateToOld();
        Object.assign( this.oldmanualpos, this._position );
        Object.assign( this.oldmanualvel, this._velocity );
        this.oldmanualt = t;
    }
    // console.log(name + " position = (" + this.x + ", " + this.y + ", " + this.z + ")");
    // console.log("  visobj position = (" + this.visobj.position.x + ", "
    //             + this.visobj.position.y + ", " + this.visobj.position.z + ")");
};

// **********************************************************************
// **********************************************************************
// **********************************************************************
// World
//
// A PhysicsObjects.Object that has boundaries and handles edge detection
//    at the world's bbox
//
// Properties:
//   objects — objects in world
//   .periodic — three elements, .x, .y, .z, each boolean ( default false )
//   .t — current time of world, in seconds.  Defaults to new Date( ).getTime( )/1000
//
//   stick — boolenan, true = stick, false = elastic bounce, properties .x0, .x1, .y0, .y1, .z0, .z1
//
// Methods:
//   getAccel( t, x, y, z, props ) — return acceleration ( cm/s ) of object at x, y, z with properties props
//                                   default world has ay = -980.  Returned object has properties .x, .y, .z

PhysicsObjects.World = function( passprops = { } )
{
    PhysicsObjects.Object.call( this, passprops );
    var props = { edgek: null,
                  springedge: true
                };
    Object.assign( props, passprops )
    this.edgek = props.edgek;
    this.springedge = props.springedge;
    this.periodic = { x0: false, y0: false, z0: false, x1: false, y1: false, z1: false };
    this.stick = { x0: false, y0: false, z0: false, x1: false, y1: false, z1: false };
    this.g = 980;
};

PhysicsObjects.World.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.World.prototype, 'constructor',
                       {
                           value: PhysicsObjects.World,
                           enumerable: false,
                           writable: true } );

// ****************************************

PhysicsObjects.World.prototype.getAccel = function( t, x, y, z )
{
    return { x: 0, y: -this.g, z: 0 };
}

// ****************************************
// If we bounce off of the edge of a world
//   treat it as a spring at the edge

PhysicsObjects.World.prototype.edgeAccel = function( pos, obj )
{
    var a = { x: 0, y: 0, z: 0 }

    if ( pos.x + obj.x0 < this.x0 )
        a.x = this.doEdgeAccel( pos, obj, "x", "x0" );
    else if ( pos.x + obj.x1 > this.x1 )
        a.x = this.doEdgeAccel( pos, obj, "x", "x1" );

    if ( pos.y + obj.y0 < this.y0 )
        a.y = this.doEdgeAccel( pos, obj, "y", "y0" );
    else if ( pos.y + obj.y1 > this.y1 )
        a.y = this.doEdgeAccel( pos, obj, "y", "y1" );

    if ( pos.z + obj.z0 < this.z0 )
        a.z = this.doEdgeAccel( pos, obj, "z", "z0" );
    else if ( pos.z + obj.y1 > this.z1 )
        a.z = this.doEdgeAccel( pos, obj, "z", "z1" );

    return a;
}

PhysicsObjects.World.prototype.doEdgeAccel = function( pos, obj, axis, side )
{
    if ( this.periodic[ side ] || this.stick[ side ] ) return 0.;
    if ( ! this.springedge ) return 0.
        
    // Treat the edge as a spring with k/m = 2e4 m / s^-2 / ( world x size )
    // I should probably worry about stiff springs and the fact that
    // I'm using RK4, but, oh well.
    let edgek = this.edgek;
    if ( edgek == null )
        edgek = 2e4 * obj.mass / ( this.x1 - this.x0 );
    let dispin = pos[ axis ] + obj.bbox[ side ] - this[ side ];
    return -edgek * dispin / obj.mass;
}

// ****************************************
// Special case for when the parent object is a World.  Assumes that the
// object obj is a rectangular solid with position = center + bbox
// Allows the child object to bounce, stick, or wrap.

PhysicsObjects.World.prototype.edgeDetect = function( obj )
{
    var retval = undefined;
    if ( obj.x + obj.x0 < this.x0 )
        retval = this.edgeUpdate( obj, "x", "x0", "x1", 1 );
    else if ( obj.x + obj.x1 > this.x1 )
        retval = this.edgeUpdate( obj, "x", "x1", "x0", -1 );
    if ( retval != undefined )
    {
        obj.x = retval.pos;
        obj.vx = retval.vel;
        obj.ax = retval.acc;
    }

    var retval = undefined;
    if ( obj.y + obj.y0 < this.y0 )
        retval = this.edgeUpdate( obj, "y", "y0", "y1", 1 );
    else if ( obj.y + obj.y1 > this.y1 )
        retval = this.edgeUpdate( obj, "y", "y1", "y0", -1 );
    if ( retval != undefined )
    {
        obj.y = retval.pos;
        obj.vy = retval.vel;
        obj.ay = retval.acc;
    }

    var retval = undefined;
    if ( obj.z + obj.z0 < this.z0 )
        retval = this.edgeUpdate( obj, "z", "z0", "z1", 1 );
    else if ( obj.z + obj.z1 > this.z1 )
        retval = this.edgeUpdate( obj, "z", "z1", "z0", -1 );
    if ( retval != undefined )
    {
        obj.z = retval.pos;
        obj.vz = retval.vel;
        obj.az = retval.acc;
    }

};

// **********************************************************************
// Bounce, stick, or wrap around an edge.  Assumes that the object is
//   a rectangular solid oriented along cardinal axes in its world.
//
// HACK ALERT -- I've decided that t < 1e-8 seconds is 0, and a < 1e-8 m/s^2 is 0.  Arbitrary!

PhysicsObjects.World.prototype.edgeUpdate = function( obj, axis, side, farside, dirsign )
{
    // console.log( "edgeUpdate: axis = " + axis + ", side = " + side
    //              + ", farside =  " + farside + ", dirsign = " + dirsign );
    // console.log( "   pos = " + pos + ", vel = " + vel + ", a = " + a );

    var dt = obj.t - obj.oldt;

    if ( this.stick[side] )
    {
        if ( dt <= 1e-8 )
            return { pos: this[side] - obj.bbox[side], vel: 0, acc: 0 }
        else
            return { pos: this[side] - obj.bbox[side], vel: 0, acc: - obj.oldvel[axis] / dt }
    }
    else if ( this.periodic[side] )
    {
        // Make sure we should actually flip
        if ( ( dirsign * obj.position[axis] ) > ( dirsign * this[side] ) )
        {
            return { pos: obj.position[axis], vel: obj.velocity[axis], acc: obj.acceleration[axis] };
        }
        else
        {
            return { pos: this[farside] + ( obj.position[axis] - this[side] ),
                     vel: obj.velocity[axis], acc: obj.acceleration[axis] };
        }
    }
    else
    {
        if ( this.springedge ) {
            // We already handled this in doEdgeAccel, so do nothing.
        }
        else {
            // Bounce: if velocity is going in further, then flip it, otherwise do nothing
            if ( dirsign * obj.velocity[axis] < 0 )
                return { pos: obj.position[axis], vel: -obj.velocity[axis],
                         acc: -( obj.velocity[axis] - obj.oldvel[axis] ) / dt }
            else
                return { pos: obj.position[axis], vel: obj.velocity[axis], acc: obj.acceleration[axis] }
        }
    }
};

// **********************************************************************
// **********************************************************************
// **********************************************************************
// A Scene is a special PhysicsObject that includes a THREE.js scene with it.
// It needs to have a View to be seen
//
// It must have its visobj = null
//
// The design currently only allows one view per scene.
// Key properties:
//   scene — the three.js scene
//   t — a current "world time".  Starts at 0 when the
//       scene is created.  Updates at real time / timedilation.
//       Doesn't update while scene is paused.
//

PhysicsObjects.Scene = function( passparams = {} )
{
    var params = { alpha: false,
                   lights: [
                       { type: "directional",
                         pos: [ 4400, 4400, 4400 ],
                         color: 0xffffff,
                         intensity: 1.5 
                       },
                       { type: "directional",
                         pos: [ -4400, -2200, 4400 ],
                         color: 0xffffff,
                         intensity: 1.0 
                       },
                       { type: "directional",
                         pos: [ 0, 100, -4400 ],
                         color: 0xffffff,
                         intensity: 1.4
                       }
                   ],
                   mindupdatet: 1/120,         // Don't update object positions more often than this
                   timedilation: 1
                 }
    Object.assign( params, passparams );
    params.visobj = null;
    params.parent = null;
    PhysicsObjects.Object.call( this, params );

    // Set this to true to have a sphere at camlook
    this._debugfocus = false;
    this._debugfocuscolor = 0xffff00;
    this._debugfocusemit = 0x000000;
    this._debugfocusscale = 1;

    this._alpha = params.alpha;
    this._timedilation = params.timedilation;
    
    this._t0 = new Date().getTime() / 1000;
    this._t = 0;
    this._lastupdatet = this._t0;
    this._mindupdatet = params.mindupdatet;
    
    // console.log( "Creating scene..." );
    var self = this;

    this.scene = new THREE.Scene();

    this.lights = [];
    for ( let lightspec of params.lights ) {
        let color = 0xffffff;
        if ( lightspec.hasOwnProperty( "color" ) ) color = lightspec.color;
        let intensity = 1.;
        if ( lightspec.hasOwnProperty( "intensity" ) ) intensity = lightspec.intensity;
        if ( lightspec.type == "directional" ) {
            let light = new THREE.DirectionalLight( color, intensity );
            light.position.set( lightspec.pos[0], lightspec.pos[1], lightspec.pos[2] );
            this.scene.add( light );
        }
        else if ( lightspec.type == "point" ) {
            let distance = 0.;
            if ( lightspec.hasOwnProperty( "distance" ) ) intensity = lightspec.distance;
            let decay = 1.;
            if ( lightspec.hasOwnProperty( "decay" ) ) decay = lightspec.decay;
            let light = new THREE.PointLight( color, intensity, distance, decay );
            light.position.set( lightspec.pos[0], lightspec.pos[1], lightspec.pos[2] );
            this.scene.add( light );
        }
        else {
            console.log( "WARNING: Unknown light type \"" + lightspec.type + "\"" );
        }
    }
    
    // For debugging purposes....
    var spheregeom = new THREE.SphereGeometry( 1, 16, 8 );
    this.focusmat = new THREE.MeshLambertMaterial( { color: this._debugfocuscolor } );
    this.focusobj = new THREE.Mesh( spheregeom, this.focusmat );
    this.focusobj.scale.set( this._debugfocusscale, this._debugfocusscale, this._debugfocusscale );
    if ( this._debugfocus ) this.scene.add( this.focusobj );
    
    this.animationcallbacks = [];
    this.views = [];
}

// Javascript inheritance is wacky
PhysicsObjects.Scene.prototype = Object.create(PhysicsObjects.Scene.prototype);
Object.defineProperty(PhysicsObjects.Scene.prototype, 'constructor',
                      {
                          value: PhysicsObjects.Scene,
                          enumerable: false,
                          writeable: true
                      });

// ****************************************
// A scene's time must be explicitly set; no automatic Date().getTime()/1000 here.
// (Except at initial object creation.)

Object.defineProperty( PhysicsObjects.Scene.prototype, "t", {
    writeable: true,
    enumerable: true,
    get( ) { return this._t; },
    set( t ) { this._t = t; }
} );

// ****************************************
// I try to change t0 so that if you change
//  time dilation, the current scene time doesn't change

Object.defineProperty( PhysicsObjects.Scene.prototype, "timedilation", {
    writeable: true,
    enumerable: true,
    get() { return this._timedilation; },
    set( val ) {
        var curclocktime = this.t * this._timedilation + this._t0;
        this._t0 = curclocktime - this.t * val;
        this._timedilation = val
        // console.log( "Timedilation set to " + val + " ; t0 set to " + this._t0 );
    }
} );


// ****************************************

Object.defineProperty( PhysicsObjects.Scene.prototype, "debugfocus", {
    writeable: true,
    enumerable: true,
    get( ) { return this._debugfocus; },
    set( val ) {
        if ( val ) {
            if ( ! this._debugfocus ) {
                this._debugfocus = true;
                this.scene.add( this.focusobj );
            }
        } else {
            if ( this._debugfocus ) {
                this._debugfocus = false;
                this.scene.remove( this.focusobj );
            }
        }
    }
} );

Object.defineProperty( PhysicsObjects.Scene.prototype, "debugfocuscolor", {
    writeable: true,
    enumerable: true,
    get( ) { return this._debugfocuscolor; },
    set( val ) {
        this._debugfocuscolor = val;
        this.focusmat.color = new THREE.Color( val );
    }
} );

Object.defineProperty( PhysicsObjects.Scene.prototype, "debugfocusemit", {
    writeable: true,
    enumerable: true,
    get( ) { return this._debugfocusemit; },
    set( val ) {
        this._debugfocusemit = val;
        this.focusmat.emissive = new THREE.Color( val );
    }
} );

Object.defineProperty( PhysicsObjects.Scene.prototype, "debugfocusscale", {
    writeable: true,
    enumerable: true,
    get( ) { return this._debugfocusscale; },
    set( val ) {
        this._debugfocusscale = val;
        this.focusobj.scale.x = val;
        this.focusobj.scale.y = val;
        this.focusobj.scale.z = val;
    }
} );

// ****************************************

PhysicsObjects.Scene.prototype.addView = function( view )
{
    var dex = this.views.indexOf( view );
    if ( dex < 0 )
        this.views.push( view );
}

PhysicsObjects.Scene.prototype.removeView = function( view )
{
    var dex;
    while ( ( dex = this.views.indexOf( view ) ) >= 0 )
        this.views.splice( dex, 1 );
}

// ****************************************

PhysicsObjects.Scene.prototype.addAnimationCallback = function( callback )
{
    var dex = this.animationcallbacks.indexOf( callback );
    if ( dex < 0 )
        this.animationcallbacks.push( callback );
}

PhysicsObjects.Scene.prototype.removeAnimationCallback = function( callback )
{
    var dex;
    while ( ( dex = this.animationcallbacks.indexOf( callback ) ) >= 0 )
        this.animationcallbacks.splice( dex, 1 );
}

// ****************************************
// Scenes have to do a bit of extra work when adding an object

PhysicsObjects.Scene.prototype.addObject = function( obj )
{
    if ( PhysicsObjects.Object.prototype.addObject.call( this, obj ) )
    {
        if ( obj.visobj != null )
        {
            this.scene.add( obj.visobj );
        }
    }
};

PhysicsObjects.Scene.prototype.removeObject = function( obj )
{
    if ( PhysicsObjects.Object.prototype.removeObject.call( this, obj ) )
    {
        if ( obj.visobj != null )
        {
            this.scene.remove( obj.visobj );
        }
    }
};


// **********************************************************************
// PauseDynamics will stop the calling of all animation callbacks.  It
//   will also pause the andvance of the Scene's t.

PhysicsObjects.Scene.prototype.pauseDynamics = function( from_visibility = false )
{
    if ( ! from_visibility ) this.explicitly_paused = true;
    if ( ! this.paused ) {
        // console.log( "Pausing dynamics." );
        this.paused = true;
        for ( let view of this.views ) {
            if ( view.elem.parentNode != null )
                view.elem.parentNode.appendChild( view.pausenotice );
        }
        this.pausetime = new Date().getTime() / 1000;
    }
}

PhysicsObjects.Scene.prototype.resumeDynamics = function( from_visibility = false )
{
    if ( ! from_visibility ) this.explicitly_paused = false;
    if ( this.paused && ! this.explicitly_paused ) {
        // console.log( "Resuming dynamics." );
        this.paused = false;
        this._t0 += new Date().getTime() / 1000 - this.pausetime;
        for ( let view of this.views )
            view.pausenotice.remove();
    }
}

// **********************************************************************
// This should only be called from within a View

PhysicsObjects.Scene.prototype.animate = function( )
{
    var self = this;

    // Because multiple views may be calling, only update
    //  at most once every mindt

    var t = new Date( ).getTime( ) / 1000.;
    if ( t - this._lastupdatet >= this._mindupdatet ) {
        this._lastupdatet = t;
        if ( ! this.paused ) {
            this.t = (t - this._t0) / this._timedilation;
            // console.log( "Calling animation callbacks with t=" + this.t );
            for ( var callback of this.animationcallbacks ) {
                callback( this.t );
            }
        }
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// A View is a window into a scene
//
// It must be put in a div with position: relatie, as it will
//   use absolute positioning for text rendering on top of the
//   canvas.  There must also be a "pausetext" CSS entry that
//
// Key Elements:
//   camera — the three.js camera (a PerspectiveCamera)
//   raycaster — a raycaster using the camera that can be used within one function elsewhere (not re-entrant)
//   elem — the DOM element of the canvas
//
// Constructor arguments
//   campos — initial position of the camera
//   camlook — initial target of the camera
//   aspect — height/width of the canvas
//   styleprops — More CSS attributes for object element (border, margin, padding, etc.)

PhysicsObjects.View = function( scene, passparams = {} )
{
    var params = { campos: new THREE.Vector3( 0., 0., 100. ),
                   camfov: 60.,
                   camlook: new THREE.Vector3( 0., 0., 0. ),
                   aspect: 0.5,
                   printfpsevery: 0
                 }
    Object.assign( params, passparams );
    this.scene = scene;
    this.scene.addView( this );

    this.camera = new THREE.PerspectiveCamera( params.camfov, 1/params.aspect, 0.1, 1000 );
    this.renderer = new THREE.WebGLRenderer( { antialias: true, alpha: this.scene._alpha } );
    // I don't know if I do this once or every single time I update
    // I'm not sure it's doing anything.
    // var context = this.renderer.getContext();
    // var sync = context.fenceSync( context.SYNC_GPU_COMMANDS_COMPLETE, 0 );
    this.raycaster = new THREE.Raycaster();

    var self = this;
    this.aspect = params.aspect;

    var elem = this.renderer.domElement;
    this.elem = elem;
    this.elem.setAttribute( "class", "scenecanvas" );
    
    // Gotta listen for resizes to make sure the camera knows the deal
    if ( ! ( "ResizeObserver" in window ) ) {
        // window.alert( "Warning: the 3d view may be blurry, the wrong size, or stretched unpleasantly. " +
        //               "Press \"Camera Controls\" and then \"Reset View\" " +
        //               "or resize the browser window a smidge to fix it. Newer browsers (Chrome>=64, Firefox>=69) " +
        //               "shouldn't have this issue." );
        consoel.log( "ResizeObserver not found, falling back to watching window resize events." );
        document.defaultView.addEventListener("resize", function(event) { self.windowResized( event ) });
        var mutobs = new MutationObserver( function( records, obs ) {
            if ( document.body.contains( self.elem ) ) {
                self.windowResized( undefined );
                obs.disconnect();
            }
        } );
        mutobs.observe( document.body, { subtree: true, childlist: true, attributes: true } );
    }
    else {
        const resizeobs = new ResizeObserver( function( entries, obsever ) { self.resize( entries ); } );
        resizeobs.observe( this.elem );
    }

    this.campos = new THREE.Vector3();
    this.homecampos = new THREE.Vector3();
    this.campos.copy( params.campos );
    this.homecampos.copy( params.campos );
    this.camlook = new THREE.Vector3();
    this.homecamlook = new THREE.Vector3();
    this.camlook.copy( params.camlook );
    this.homecamlook.copy( params.camlook );
    this.camera.position.copy(this.campos);
    this.camera.lookAt(this.camlook);
    this.moving = false;
    this.rotating = false;
    this.zooming = false;
    this.initdist = 1.;
    this.initoffset = new THREE.Vector3();
    this.initmousex = 0.;
    this.initmousey = 0.;
    this.lastmousex = 0.;
    this.lastmousey = 0.;
    this.movestep = 1.;
    this.rotabsstep = 0.1;
    this.rotstep = 0.2;

    this.zoomstep = 1.;
    this.rotxaxis = new THREE.Vector3(1, 0, 0);
    this.rotyaxis = new THREE.Vector3(0, 1, 0);
    this.downcallback = function(event) { self.mousedown(event); };
    this.movecallback = function(event) { self.mousemoved(event); };
    this.upcallback = function(event) { self.mouseup(event); };

    this._absoluterotate = true;
    
    this.elem.addEventListener("mousedown", this.downcallback);

    this.pausenotice = document.createElement( "div" );
    this.pausenotice.setAttribute( "class", "pausetext" );
    this.pausenotice.appendChild( document.createTextNode( "Dynamics Paused" ) );
    this.paused = false;
    this.explicitlypaused = false;

    this.framecount = 0;
    this.lastframet = window.performance.now();
    this.printfpsevery = params.printfpsevery;

    this.cameralisteners = [];

    document.addEventListener( "visibilitychange", function() { self.visibilityChanged() } );
}

// ****************************************
// absoluterotate true means that left-right rotates about the y-axis,
//   and up-down rotates the y-axis about the initial horizontal
// false = always rotate about an axis that is horizontal or vertical
//   on the current view

Object.defineProperty( PhysicsObjects.View.prototype, "absoluterotate", {
    writeable: true,
    enumerable: true,
    get() { return this._absoluterotate; },
    set( val ) { if (val) this._absoluterotate = true;
                 else this._absoluterotate = false;
               }
} );

// ****************************************

PhysicsObjects.View.prototype.addCameraListener = function( callback )
{
    var dex = this.cameralisteners.indexOf( callback );
    if ( dex < 0 )
        this.cameralisteners.push( callback );
}

// ****************************************

PhysicsObjects.View.prototype.removeCameraListener = function( callback )
{
    var dex;
    while ( ( dex = this.cameralisteners.indexOf( callback ) ) >= 0 )
        this.cameralisteners.splice( dex, 1 );
}

// ****************************************

PhysicsObjects.View.prototype.resize = function( entries )
{
    console.log( "In PhysicsObjects.View.resize; there are " + entries.length + " entries." );
    for ( var entry of entries ) {
        // console.log( "entry.rarget = " + entry.target );
        if ( entry.target != this.elem ) continue;
        // console.log( "PhysicsObjects.View.resize" );
        var width = ( entry.contentRect.width ).toFixed(0);
        var height = entry.contentRect.height;
        var height = ( this.aspect * width ).toFixed(0);
        // console.log( "Setting PhysicsObjects.View.elem dimensions to " + width + " × " + height );
        this.renderer.setSize( width, height );
        this.camera.aspect = width/height;
        this.camera.updateProjectionMatrix();
    }
}

PhysicsObjects.View.prototype.windowResized = function( entries )
{
    console.log( "PhysicsObjects.View.windowResized" );
    var parent = this.elem.parentElement;
    if ( parent != null ) {
        // console.log( "Parent element class is " + parent.getAttribute( "class" ) );
        // console.log( "available size is " + parent.scrollWidth + " × " + parent.scrollHeight );
        var mystyle = window.getComputedStyle( this.elem );
        var blw = parseFloat( mystyle.getPropertyValue( "border-left-width" ) );
        var brw = parseFloat( mystyle.getPropertyValue( "border-right-width" ) );
        var plw = parseFloat( mystyle.getPropertyValue( "padding-left" ) );
        var prw = parseFloat( mystyle.getPropertyValue( "padding-right" ) );
        var width = parent.scrollWidth - blw - plw - prw - brw;
        // console.log( "Setting width to " + parent.scrollWidth  + " - " + blw + " - " +
        //              plw + "- " + prw + " - " + brw + " = " + width );
        // var btw = parseFloat( mystyle.getPropertyValue( "border-top-width" ) );
        // var bbw = parseFloat( mystyle.getPropertyValue( "border-bottom-width" ) );
        // var ptw = parseFloat( mystyle.getPropertyValue( "padding-top" ) );
        // var pbw = parseFloat( mystyle.getPropertyValue( "padding-bottom" ) );
        // var height = parent.scrollHeight - btw - ptw - pbw - bbw;
        // console.log( "Setting height to " + parent.scrollHeight  + " - " + btw + " - " +
        //              ptw + "- " + pbw + " - " + bbw + " = " + height );
        var height = this.aspect * width;
        // console.log ( "Aspect = " + this.aspect + ", so setting height to " + height );
        this.renderer.setSize( width, height );
        this.camera.aspect = width/height;
        this.camera.updateProjectionMatrix();
    }
}

// **********************************************************************

PhysicsObjects.View.prototype.mousedown = function(event)
{
    var self = this;
    var elem = this.renderer.domElement

    // console.log("Mouse down at x = " + event.offsetX + ", y = " + event.offsetY);
    if (event.button !=0) return;

    var canvasdims = new THREE.Vector2(0., 0.);
    this.renderer.getSize(canvasdims);
    var canvassize = Math.max(canvasdims.x, canvasdims.y);

    this.rotxaxis.x = 1.; this.rotxaxis.y = 0.; this.rotxaxis.z = 0.;
    this.rotyaxis.x = 0.; this.rotyaxis.y = 1.; this.rotyaxis.z = 0.;
    var camquat = this.camera.quaternion;
    // console.log("Camera quaternion: (" + camquat.x + ", " + camquat.y + ", " + camquat.z + ", " + camquat.w + ")");
    this.rotxaxis.applyQuaternion(camquat);
    this.rotyaxis.applyQuaternion(camquat);
    if (event.shiftKey && event.ctrlKey)
    {
        this.zooming = true;
        this.initoffset.copy(this.campos);
        this.initoffset.sub(this.camlook);
        this.initdist = this.initoffset.length();
        this.zoomstep = 3 / (canvasdims.y/2.);
    }
    else if (event.shiftKey)
    {
        this.rotating = true;
        this.rotabsstep = Math.PI / canvassize;
        this.rotstep = 2. * Math.PI / canvassize;
    }
    else if (event.ctrlKey)
    {
        // Want to try to make it so that something at the camlook
        //   position moves 1:1 with the mouse
        // focusvect = campos - camlook
        // |focusvect| * tan( fov ) = world size at camlook
        this.moving = true;
        var focusvect = new THREE.Vector3();
        this.initcamlook = new THREE.Vector3();
        this.initcamlook.copy( this.camlook );
        focusvect.copy( this.campos );
        focusvect.sub( this.camlook );
        var focuslength = focusvect.length();
        var fov = this.camera.fov * Math.PI / 180.;
        this.movestep = focuslength * Math.tan( fov/2 ) / ( canvasdims.y / 2 );
        // console.log("focuslength = " + focuslength + " canvassize = " + canvassize +
        //             " movestep = " + this.movestep);
        // console.log("this.movestep = " + this.movestep);
    }
    else return;

    this.initmousex = event.offsetX;
    this.initmousey = event.offsetY;
    this.lastmousex = this.initmousex;
    this.lastmousey = this.initmousey;
    elem.addEventListener("mousemove", this.movecallback);
    elem.addEventListener("mouseup", this.upcallback);
};


PhysicsObjects.View.prototype.domousemove = function(event)
{
    var transx = (this.initmousex - event.offsetX) * this.movestep;
    var transy = (event.offsetY - this.initmousey) * this.movestep;
    var xtrans = new THREE.Vector3();
    xtrans.copy(this.rotxaxis);
    xtrans.multiplyScalar(transx);
    var ytrans = new THREE.Vector3();
    ytrans.copy(this.rotyaxis);
    ytrans.multiplyScalar(transy);
    xtrans.add(ytrans);
    
    var newpos = new THREE.Vector3();
    newpos.copy(this.campos);
    newpos.add(xtrans);
    this.camera.position.x = newpos.x;
    this.camera.position.y = newpos.y;
    this.camera.position.z = newpos.z;
    newpos.copy( this.initcamlook );
    newpos.add( xtrans );
    this.camlook.copy( newpos );
    // this.camera.lookAt(newpos);

    for ( var callback in this.cameralisteners ) callback( this.camera );

    this.lastmousex = event.offsetX;
    this.lastmousey = event.offsetY;
    
    return newpos;
}


PhysicsObjects.View.prototype.domouserotate = function(event)
{
    if ( this._absoluterotate ) {
        var camvec = new THREE.Vector3();
        camvec.copy(this.campos);
        camvec.sub(this.camlook);
        var xangle = (this.initmousex - event.offsetX) * this.rotabsstep;
        var yangle = (this.initmousey - event.offsetY) * this.rotabsstep;
        var q = new THREE.Quaternion();
        q.setFromAxisAngle( this.rotyaxis, xangle );
        camvec.applyQuaternion( q );
        // this.camera.applyQuaternion( q );
        q.setFromAxisAngle( this.rotxaxis, yangle );
        camvec.applyQuaternion( q );
        // this.camera.applyQuaternion( q );
        camvec.add(this.camlook);
        this.camera.position.copy(camvec);
        // I'm not completely sure how I want camera rotation to work.
        // This gives a "gimbal lock" thing when looking up/down the y-axis
        this.camera.lookAt(this.camlook);
    }
    else {
        var camvec = new THREE.Vector3();
        camvec.copy( this.camera.position );
        camvec.sub( this.camlook );
        var camdist = camvec.length();
        var camhat = new THREE.Vector3();
        camhat.copy( camvec );
        camhat.normalize();
        var camquat = this.camera.quaternion;
        var rotxaxis = new THREE.Vector3( 1, 0, 0 );
        var rotyaxis = new THREE.Vector3( 0, 1, 0 );
        rotxaxis.applyQuaternion( camquat );
        rotyaxis.applyQuaternion( camquat );
        var xangle = ( this.lastmousex - event.offsetX ) * this.rotstep;
        var yangle = ( this.lastmousey - event.offsetY ) * this.rotstep;
        var q = new THREE.Quaternion();
        q.setFromAxisAngle( rotyaxis, xangle );
        camhat.applyQuaternion( q );
        this.camera.applyQuaternion( q );
        q.setFromAxisAngle( rotxaxis, yangle );
        camhat.applyQuaternion( q );
        this.camera.applyQuaternion( q );
        camvec.copy( camhat );
        camvec.multiplyScalar( camdist );
        camvec.add( this.camlook );
        this.camera.position.copy( camvec );
        this.campos.copy( camvec );
        this.lastmousex = event.offsetX;
        this.lastmousey = event.offsetY;
    }

    for ( var callback in this.cameralisteners ) callback( this.camera );
}


PhysicsObjects.View.prototype.domousezoom = function(event)
{
    var powfac = this.zoomstep * (event.offsetY - this.initmousey);
    var dist = this.initdist * Math.pow(2., powfac);
    var fac = dist / this.initdist;
    var offset = new THREE.Vector3();
    offset.copy(this.initoffset);
    offset.multiplyScalar(fac);
    // console.log("initoffset = " + this.initoffset.x + ", " + this.initoffset.y + ", " + this.initoffset.z + ")");
    // console.log("initdist = " + this.initdist + ", dist = " + dist + ", fac = " + fac);
    // console.log("powfac = " + powfac + ", offset = (" + offset.x + ", " + offset.y + ", " + offset.z + ")")
    offset.add(this.camlook);
    this.camera.position.set(offset.x, offset.y, offset.z);
    this.lastmousex = event.offsetX;
    this.lastmousey = event.offsetY;
    
    for ( var callback in this.cameralisteners ) callback( this.camera );
}


PhysicsObjects.View.prototype.mousemoved = function(event)
{
    if (this.moving)
    {
        this.domousemove(event);
    }
    else if (this.rotating)
    {
        this.domouserotate(event);
    }
    else if (this.zooming)
    {
        this.domousezoom(event);
    }
}

PhysicsObjects.View.prototype.mouseup = function(event)
{
    var self = this;
    
    this.elem.removeEventListener("mousemove", this.movecallback);
    this.elem.removeEventListener("mouseup", this.upcallback);

    if (this.moving)
    {
        var newlook = this.domousemove(event);
        this.campos.copy(this.camera.position);
        this.camlook.copy(newlook);
    }
    else if (this.rotating)
    {
        this.domouserotate(event);
        this.campos.copy(this.camera.position);
    }
    else if (this.zooming)
    {
        this.domousezoom(event);
        this.campos.copy(this.camera.position);
    }

    this.moving = false;
    this.rotating = false;
    this.zooming = false;

    for ( var callback in this.cameralisteners ) callback( this.camera );
}

// **********************************************************************
// This orients the camera such that the y axis is (as much as possible) up

PhysicsObjects.View.prototype.setCamera = function( campos, camlook )
{
    this.campos.copy( campos );
    this.camlook.copy( camlook );
    this.camera.position.set( this.campos.x, this.campos.y, this.campos.z )
    this.camera.lookAt(this.camlook);

    for ( var callback in this.cameralisteners ) callback( this.camera );
    
}

// **********************************************************************

PhysicsObjects.View.prototype.camerayup = function()
{
    this.setCamera( this.campos, this.camlook );
}

// **********************************************************************

PhysicsObjects.View.prototype.cameraToAxis = function( axis )
{
    var camvec = new THREE.Vector3();
    camvec.copy( this.camera.position );
    camvec.sub( this.camlook );
    var camdist = camvec.length();

    if ( axis == "x" )
        camvec.set( camdist, 0, 0 )
    else if ( axis == "y" )
        camvec.set( 0, camdist, 0 )
    else if ( axis == "z" )
        camvec.set( 0, 0, camdist )
    else if ( axis == "-x" )
        camvec.set( -camdist, 0, 0 )
    else if ( axis == "-y" )
        camvec.set( 0, -camdist, 0 )
    else if ( axis == "-z" )
        camvec.set( 0, 0, -camdist )
    else
        return;

    this.setCamera( camvec, this.camlook );
}

// **********************************************************************

PhysicsObjects.View.prototype.camerahome = function()
{
    this.setCamera( this.homecampos, this.homecamlook );
    if ( ! ( "ResizeObserver" in window ) ) {
        this.windowResized( event );
    }
}

// **********************************************************************

PhysicsObjects.View.prototype.cameraZoomAll = function()
{
    var vec = new THREE.Vector3();
    // I'm not sure why this next line is necessary, but somebody on stackoverflow said
    //   that this made the camera remember where it was... or something....
    //   Without it, the Vector3.project() using the camera was returning NaN
    // Almost certainly were relying on a side effect of the function, which is scary.
    this.camera.getWorldPosition( vec );
    
    var zhat = new THREE.Vector3( 0, 0, -1 );
    zhat.applyQuaternion( this.camera.quaternion );
    var ndc = new THREE.Vector3();
    var tmpvec = new THREE.Vector3();
    var deltacamz = -1e32;

    var box = new THREE.Box3();
    box.setFromObject( this.scene.scene );
    vec.copy( box.max );
    vec.project( this.camera );
    for ( let x of [ box.min.x, box.max.x ] ) {
        for ( let y of [ box.min.y, box.max.y ] ) {
            for (let z of [ box.min.z, box.max.z ] ) {
                ndc.set( x, y, z );
                ndc.project( this.camera );
                
                vec.set( x, y, z );
                vec.sub( this.camera.position );
                let z0 = vec.dot( zhat );
                // console.log( "z0 = " + z0 );
                let z1y = Math.abs(ndc.y) * Math.abs(z0);
                let z1x = Math.abs(ndc.x) * Math.abs(z0);
                let deltacamzx = z1x - z0;
                let deltacamzy = z1y - z0;

                if ( deltacamzx > deltacamz ) deltacamz = deltacamzx;
                if ( deltacamzy > deltacamz ) deltacamz = deltacamzy;
            }
        }
    }
    vec.copy( zhat );
    vec.multiplyScalar( -deltacamz );
    vec.add( this.camera.position );
    this.camera.position.copy( vec );
    this.campos.copy( vec );
}

// **********************************************************************
// Callback for change of page visibility

PhysicsObjects.View.prototype.visibilityChanged = function()
{
    // console.log( "visibilitychanged event, visibility = " + document.visibilityState );
    if ( document.visibilityState == "hidden" )
        this.scene.pauseDynamics( true );
    else if ( document.visibilityState == "visible" )
        this.scene.resumeDynamics( true );
}

// **********************************************************************
// Call this to start the view animating.  Must be called for each view

PhysicsObjects.View.prototype.animate = function()
{
    var self = this;

    // I don't know if I do this once or every single time I update
    // I'm not sure it's doing anything.
    // var context = this.renderer.getContext();
    // var sync = context.fenceSync( context.SYNC_GPU_COMMANDS_COMPLETE, 0 );

    requestAnimationFrame( function() { self.animate( ) } );
    // console.log( "View animating." );
    this.scene.animate();
    this.framecount += 1;
    if ( this.printfpsevery > 0 )
    {
        if ( this.framecount >= this.printfpsevery )
        {
            var t = window.performance.now();
            console.log( " fps: " + 1000 * this.framecount / ( t - this.lastframet ) );
            this.framecount = 0;
            this.lastframet = t;
            // var info = this.renderer.info;
            // console.log( "info.memory.geometries= " + info.memory.geometries );
            // console.log( "           .textures  = " + info.memory.textures );
            // console.log( "info.render.calls     = " + info.render.calls );
            // console.log( "           .frame     = " + info.render.frame );
            // console.log( "           .lines     = " + info.render.lines );
            // console.log( "           .points    = " + info.render.points );
            // console.log( "           .triangles = " + info.render.triangles );
        }
    }
    if ( this.scene.debugfocus )
        this.scene.focusobj.position.copy( this.camlook );
    this.renderer.render( this.scene.scene, this.camera );    
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Some standard objects

// **********************************************************************
// A block of mass m
// Note that rx, ry, rz, are *half*-lengths (because "radius")

PhysicsObjects.Block = function( inparams={} )
{
    var params = { rx: 1.,
                   ry: 0.5,
                   rz: 0.75,
                   color: 0xff3333,
                   name: "Block of Mass m"
                 };
    Object.assign( params, inparams );
    PhysicsObjects.Object.call( this, params );

    var geom = new THREE.BoxGeometry( 2*params.rx, 2*params.ry, 2*params.rz );
    var mat = new THREE.MeshLambertMaterial( { color: params.color } );
    this.visobj = new THREE.Mesh( geom, mat );
    this.visobj.name = this.name;
    // Hack to force visobj to the right position
    this.position = this._position;
}

PhysicsObjects.Block.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.Block.prototype, 'constructor',
                       {
                           value: PhysicsObjects.Block,
                           enumerable: false,
                           writeable: true } );


// **********************************************************************
// **********************************************************************
// **********************************************************************
// A Helix.
//
// Pass in params:
//   legnth: length of the spring.  Its position is one end of the spring
//   rad: radius of the helix
//   circrad: radius of the circular cross-section
//   nrots: number of repetiations of the tilted-circle
//   pointsperrot: divide each rotation into this many points
//   ncirc: the number or points around the cross-section
//
// You can dynamically update length, and it will regenerate
//   all of the points.

PhysicsObjects.Spring = function( inparams )
{
    var params = { length: 2,
                   rad: 0.5,
                   circrad: 0.1,
                   nrots: 5,
                   pointsperrot: 12,
                   ncirc: 6,
                   name: "Spring",
                   color: 0xc08000,
                   emissive: null,
                   emissiveIntensity: 0,
                   metalness: 0.4,
                   roughness: 0.2
                 };
    Object.assign( params, inparams );
    params.rx = params.length / 2. + 2*params.circrad;
    params.ry = params.rad + params.circrad;
    params.rz = params.rad + params.circrad;

    this._helixlength = params.length;
    this._helixrad = params.rad;
    this._helixcircrad = params.circrad;
    this._helixnrots = params.nrots;
    this._helixpointsperrot = params.pointsperrot
    this._helixncirc = params.ncirc;
    
    if ( ( params.emissiveIntensity > 0 ) && ( params.emissive != null ) )
        params.emissive = params.color;
    else
        params.emissive = 0x000000;

    PhysicsObjects.Object.call( this, params );

    this._npoints = this._helixnrots * this._helixpointsperrot + 1;
    this._vertices = new Float32Array( this._npoints * this._helixncirc * 3 );
    this._normals = new Float32Array( this._npoints * this._helixncirc * 3 );
    
    var ret = this.getPoints();
    var geom = new THREE.BufferGeometry();
    geom.setIndex( this.getIndices() );
    geom.setAttribute( 'position', new THREE.Float32BufferAttribute( this._vertices, 3 ) );
    geom.setAttribute( 'normal', new THREE.Float32BufferAttribute( this._normals, 3 ) );
    var mat = new THREE.MeshStandardMaterial( { color: params.color,
                                                emissive: params.emissive,
                                                emissiveIntensity: params.emissiveIntensity,
                                                metalness: params.metalness,
                                                roughness: params.roughness } );
    this.visobj = new THREE.Mesh( geom, mat );
}

PhysicsObjects.Spring.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.Spring.prototype, 'constructor',
                       {  value: PhysicsObjects.Spring,
                          enumerate: false,
                          writeable: true } );

// **********************************************************************

Object.defineProperty( PhysicsObjects.Spring.prototype, "length", {
    get() { return this._helixlength; },
    set( val ) {
        this._helixlength = val;
        this.getPoints();
        this.visobj.geometry.getAttribute( 'position' ).set( this._vertices );
        this.visobj.geometry.getAttribute( 'position' ).needsUpdate = true;
        this.visobj.geometry.getAttribute( 'normal' ).set( this._normals );
        this.visobj.geometry.getAttribute( 'normal' ).needsUpdate = true;
        // Set the bounding box to null so that things that look for it
        //   will know to ask for a recompute
        this.visobj.geometry.boundingBox = null;
    }
} );

// **********************************************************************

PhysicsObjects.Spring.prototype.getIndices = function()
{
    var i, j;
    var indices = [];
    const npoints = this._npoints;
    for ( i = 1 ; i < this._npoints-1 ; ++i ) {
        for ( j = 1 ; j < this._helixncirc ; ++ j ) {
            indices.push( (i-1)*this._helixncirc + (j-1), (i-1)*this._helixncirc + j, i*this._helixncirc + j );
            indices.push( (i-1)*this._helixncirc + (j-1), i*this._helixncirc + j, i*this._helixncirc + (j-1) );
        }
        indices.push( (i-1)*this._helixncirc + (this._helixncirc-1), (i-1)*this._helixncirc, i*this._helixncirc );
        indices.push( (i-1)*this._helixncirc + (this._helixncirc-1), i*this._helixncirc,
                      i*this._helixncirc + (this._helixncirc-1) );
    }
    for ( j = 1 ; j < this._helixncirc ; ++j ) {
        indices.push( (npoints-2)*this._helixncirc + (j-1),
                      (npoints-2)*this._helixncirc + j,
                      (npoints-1)*this._helixncirc + j );
        indices.push( (npoints-2)*this._helixncirc + (j-1),
                      (npoints-1)*this._helixncirc + j,
                      (npoints-1)*this._helixncirc + (j-1) );
    }
    indices.push( (npoints-2)*this._helixncirc + (this._helixncirc-1),
                  (npoints-2)*this._helixncirc,
                  (npoints-1)*this._helixncirc );
    indices.push( (npoints-2)*this._helixncirc + (this._helixncirc-1),
                  (npoints-1)*this._helixncirc,
                  (npoints-1)*this._helixncirc + (this._helixncirc-1) );

    return indices;
}

// **********************************************************************

PhysicsObjects.Spring.prototype.getPoints = function()
{
    var i, j, x2, y2, z2, x1, y1, z1, x0, y0, z0, phase, mag;
    const npoints = this._npoints;
    var axis = new THREE.Vector3( 0, 0, 0 );
    var edgeoff = new THREE.Vector3( 0, 0, 0 );
    
    // First point
    x1 = 0;
    y1 = 0;
    z1 = this._helixrad;
    phase = 2*Math.PI * this._helixnrots / (npoints-1);
    x2 = this._helixlength / (npoints-1);
    y2 = this._helixrad * Math.sin( phase );
    z2 = this._helixrad * Math.cos( phase );
    for ( j = 0 ; j < this._helixncirc ; ++j ) {
        this._vertices[ j*3 + 0 ] = x1 + this._helixcircrad * Math.cos( 2*Math.PI * j / this._helixncirc );
        this._vertices[ j*3 + 1 ] = y1;
        this._vertices[ j*3 + 2 ] = z1 - this._helixcircrad * Math.sin( 2*Math.PI * j / this._helixncirc );
        this._normals[ j*3 + 0 ] = Math.cos( 2*Math.PI * j / this._helixncirc );
        this._normals[ j*3 + 1 ] = 0;
        this._normals[ j*3 + 2 ] = -Math.sin( 2*Math.PI * j / this._helixncirc );
    }

    for ( i = 1 ; i < npoints-1 ; ++i ) {
        x0 = x1; y0 = y1; z0 = z1;
        x1 = x2; y1 = y2; z1 = z2;
        phase = 2 * Math.PI * this._helixnrots * (i+1) / (npoints-1);
        x2 = this._helixlength * (i+1) / (npoints-1);
        y2 = this._helixrad * Math.sin( phase );
        z2 = this._helixrad * Math.cos( phase );
        axis.x = ( x2 - x0 ) / 2.;
        axis.y = ( y2 - y0 ) / 2.;
        axis.z = ( z2 - z0 ) / 2.;
        mag = Math.sqrt( axis.x**2 + axis.y**2 + axis.z**2 );
        axis.x /= mag;
        axis.y /= mag;
        axis.z /= mag;
        edgeoff.x = -y1 * axis.z + z1 * axis.y;
        edgeoff.y = -z1 * axis.x;
        edgeoff.z = y1 * axis.x;
        mag = Math.sqrt( edgeoff.x**2 + edgeoff.y**2 + edgeoff.z**2 );
        edgeoff.x *= this._helixcircrad / mag;
        edgeoff.y *= this._helixcircrad / mag;
        edgeoff.z *= this._helixcircrad / mag;
        this._vertices[ ( i * this._helixncirc ) * 3 + 0 ] = x1 + edgeoff.x;
        this._vertices[ ( i * this._helixncirc ) * 3 + 1 ] = y1 + edgeoff.y;
        this._vertices[ ( i * this._helixncirc ) * 3 + 2 ] = z1 + edgeoff.z;
        this._normals[ ( i * this._helixncirc ) * 3 + 0 ] = edgeoff.x;
        this._normals[ ( i * this._helixncirc ) * 3 + 1 ] = edgeoff.y;
        this._normals[ ( i * this._helixncirc ) * 3 + 2 ] = edgeoff.z;
        phase = 2*Math.PI / this._helixncirc;
        for ( j = 1 ; j < this._helixncirc ; ++j ) {
            edgeoff.applyAxisAngle( axis, phase );
            this._vertices[ ( i * this._helixncirc + j ) * 3 + 0 ] = x1 + edgeoff.x;
            this._vertices[ ( i * this._helixncirc + j ) * 3 + 1 ] = y1 + edgeoff.y;
            this._vertices[ ( i * this._helixncirc + j ) * 3 + 2 ] = z1 + edgeoff.z;
            this._normals[ ( i * this._helixncirc + j ) * 3 + 0 ] = edgeoff.x;
            this._normals[ ( i * this._helixncirc + j ) * 3 + 1 ] = edgeoff.y;
            this._normals[ ( i * this._helixncirc + j ) * 3 + 2 ] = edgeoff.z;
        }
    }

    // Last point
    x1 = this._helixlength;
    y1 = 0.;
    z1 = this._helixrad;
    i = npoints-1;
    for ( j = 0 ; j < this._helixncirc ; ++j ) {
        this._vertices[ ( i * this._helixncirc + j ) * 3 + 0 ] = ( x1 + this._helixcircrad *
                                                                   Math.cos( 2*Math.PI * j / this._helixncirc ) );
        this._vertices[ ( i * this._helixncirc + j ) * 3 + 1 ] = y1;
        this._vertices[ ( i * this._helixncirc + j ) * 3 + 2 ] = ( z1 - this._helixcircrad *
                                                                   Math.sin( 2*Math.PI * j / this._helixncirc ) );
        this._normals[ ( i * this._helixncirc + j ) * 3 + 0 ] = Math.cos( 2*Math.PI * j / this._helixncirc );
        this._normals[ ( i * this._helixncirc + j ) * 3 + 1 ] = 0;
        this._normals[ ( i * this._helixncirc + j ) * 3 + 2 ] = -Math.sin( 2*Math.PI * j / this._helixncirc );
    }
}
    

// **********************************************************************
// Standard RGB axies

PhysicsObjects.Axes = function( length = 20, emit = 0 )
{
    var widfrac = 0.05;
    var conebasefrac = 0.1;
    var conelenfrac = 0.2;

    PhysicsObjects.Object.call( this, { rx: (1+conelenfrac) * length,
                                        ry: (1+conelenfrac) * length,
                                        rz: (1+conelenfrac) * length,
                                        name: "Axes"
                                      } );
    
    var cylgeom = new THREE.CylinderGeometry( widfrac*length, widfrac*length, length );
    var arrowgeom = new THREE.CylinderGeometry( 0., conebasefrac*length, conelenfrac*length );

    arrowgeom.mergeVertices();
    arrowgeom.computeVertexNormals();

    var xcylmat = new THREE.MeshLambertMaterial( { color: 0xcc0000, emissive: emit * 0xcc0000 } );
    var xcyl = new THREE.Mesh( cylgeom, xcylmat );
    xcyl.position.set( length/2., 0., 0. );
    xcyl.setRotationFromAxisAngle( new THREE.Vector3(0., 0., -1.), Math.PI/2. );
    var xarrow = new THREE.Mesh( arrowgeom, xcylmat);
    xarrow.position.set(length + conelenfrac/2*length, 0., 0.);
    xarrow.setRotationFromAxisAngle( new THREE.Vector3(0., 0., -1.), Math.PI/2. );
    var xaxis = new THREE.Group();
    xaxis.name = "X-Axis";
    xaxis.add( xcyl );
    xaxis.add( xarrow );

    var ycylmat = new THREE.MeshLambertMaterial( { color: 0x00cc00, emissive: emit * 0x00cc00 } );
    var ycyl = new THREE.Mesh( cylgeom, ycylmat );
    ycyl.position.set( 0., length/2., 0. );
    var yarrow = new THREE.Mesh( arrowgeom, ycylmat);
    yarrow.position.set( 0., length + conelenfrac/2*length, 0. );
    var yaxis = new THREE.Group();
    yaxis.name = "Y-axis";
    yaxis.add( ycyl );
    yaxis.add( yarrow );

    var zcylmat = new THREE.MeshLambertMaterial( { color: 0x0000cc, emissive: emit * 0x0000cc } );
    var zcyl = new THREE.Mesh( cylgeom, zcylmat );
    zcyl.position.set( 0., 0., length/2. );
    zcyl.setRotationFromAxisAngle( new THREE.Vector3( 1., 0., 0. ), Math.PI/2. );
    var zarrow = new THREE.Mesh( arrowgeom, zcylmat);
    zarrow.position.set( 0., 0., length + conelenfrac/2*length );
    zarrow.setRotationFromAxisAngle( new THREE.Vector3( 1., 0., 0. ), Math.PI/2. );
    var zaxis = new THREE.Group();
    zaxis.name = "Z-axis";
    zaxis.add( zcyl );
    zaxis.add( zarrow );
    
    this.visobj = new THREE.Group();
    this.visobj.name = "Axes";
    this.visobj.add( xaxis );
    this.visobj.add( yaxis );
    this.visobj.add( zaxis );

    // Hack to force visobj to the right position
    this.position = this._position;
}

PhysicsObjects.Axes.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.Axes.prototype, 'constructor',
                       {
                           value: PhysicsObjects.Axes,
                           enumerable: false,
                           writeable: true } );

// **********************************************************************
// Big wooden table

PhysicsObjects.Table = function( length = 240, width = 60, height = 1 )
{
    var self = this;

    PhysicsObjects.Object.call( this, { rx: length/2, ry: height/2, rz: width/2, name: "Table" } );
    
    var geom = new THREE.BoxGeometry( length, height, width );

    // I hate that I'm loading the same image three times,
    //  but I haven't had success laoding one image and
    //  then creating the textures in the onload function
    
    var textureloader = new THREE.TextureLoader();
    var texturetop = textureloader.load( PhysicsObjects.baseurl + "assets/Willow.png" ); // BogOak.png" );
    texturetop.wrapT = THREE.RepeatWrapping;
    texturetop.wrapS = THREE.RepeatWrapping;
    texturetop.repeat.set( length/width , 1 );
    texturetop.updateMatrix();
    var textureside = new textureloader.load( PhysicsObjects.baseurl + "assets/Willow.png" ); // BogOak.png" );
    textureside.wrapT = THREE.RepeatWrapping;
    textureside.wrapS = THREE.RepeatWrapping;
    textureside.repeat.set( length/width , height/width );
    textureside.updateMatrix();
    var textureend = new textureloader.load( PhysicsObjects.baseurl + "assets/Willow.png" ); // BogOak.png" );
    textureend.wrapT = THREE.RepeatWrapping;
    textureend.wrapS = THREE.RepeatWrapping;
    textureend.repeat.set( 1 , height/width );
    textureend.updateMatrix();
    
    var topmat = new THREE.MeshLambertMaterial( { color: 0x663300, map: texturetop } );
    var endmat = new THREE.MeshLambertMaterial( { color: 0x663300, map: textureend } );
    var sidemat = new THREE.MeshLambertMaterial( { color: 0x663300, map: textureside } );

    var mats = [ endmat, endmat, topmat, topmat, sidemat, sidemat ];

    this.visobj = new THREE.Mesh( geom, mats );
    this.visobj.name = "Table";

    // Hack to force visobj to the right position
    this.position = this._position;
}

PhysicsObjects.Table.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.Table.prototype, 'constructor',
                       {
                           value: PhysicsObjects.Table,
                           enumerable: false,
                           writeable: true } );

// **********************************************************************
// Big grassy ground

PhysicsObjects.Ground = function( inparams )
{
    var params = { length: 200,
                   width: 200,
                   texrep: 40 };
    Object.assign( params, inparams );

    PhysicsObjects.Object.call( this, { rx: params.length/2, ry: 0, rz: params.width/2, name: "Ground" } );

    var geom = new THREE.PlaneGeometry( params.length, params.width );
    var textureloader = new THREE.TextureLoader();
    var texture = textureloader.load( PhysicsObjects.baseurl + "assets/grass-field-seamless-texture-720x720.jpg" );
    texture.wrapT = THREE.RepeatWrapping;
    texture.wrapS = THREE.RepeatWrapping;
    if ( params.length > params.width )
        texture.repeat.set( params.texrep, params.length/params.width * params.texrep );
    else
        texture.repeat.set( params.width/params.length * params.texrep, params.texrep );
    texture.updateMatrix();
    var mat = new THREE.MeshLambertMaterial( { color: 0x339933, map: texture } );

    this.visobj = new THREE.Mesh( geom, mat );
    this.visobj.rotateX( -Math.PI / 2. );
    this.visobj.name = this.name;

    // Hack to force visobj to the right position
    this.position = this._position;
}

PhysicsObjects.Ground.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.Ground.prototype, 'constructor',
                       {
                           value: PhysicsObjects.Ground,
                           enumerable: false,
                           writeable: true } );


// **********************************************************************
// Small Rocket, oriented in +y
// Nose is at +3.466
// Bottom rim is at -2.6361
// Exhaust cone goes from -2.5037 (r=0.28074) to -3.4693 (r=0.65809)

PhysicsObjects.SmallRocket = function( inparams )
{
    var self = this;
    
    var params = {
        rx: 1.05,
        ry: 3.47,
        rz: 1.05,
        mass: 1000,
        name: "Small Rocket",
        loadfunction: null
    };
    Object.assign( params, inparams );
    PhysicsObjects.Object.call( this, params );

    this.nosey = 3.466;
    this.bottomrim = -2.6361;
    this.bottomcone = -3.4693;
    this.bottomconerad = 0.65809;
    
    this.ready = false;
    this.error = false;
    this.visobj = null;

    var loader = new GLTFLoader();
    loader.load( PhysicsObjects.baseurl + "assets/smallrocket.glb",
                 function( gltf ) { self.loaded( gltf, params.loadfunction ) },
                 function( xhdr ) { console.log( "Small rocket loaded fraction:" + xhdr.loaded / xhdr.total ); },
                 function( err ) {
                     self.ready = false;
                     self.error = true;
                     console.log( "Error loading small rocket: " + err );
                 } );
}
    
PhysicsObjects.SmallRocket.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.SmallRocket.prototype, 'constructor',
                       {  value: PhysicsObjects.SmallRocket,
                          enumerate: false,
                          writeable: true } );
    
PhysicsObjects.SmallRocket.prototype.loaded = function( gltf, loadfunc )
{
    this.ready = true;
    this.visobj = gltf.scene;
    this.visobj.name = this.name;
    // Hack to force visobj to the right position
    this.position = this._position;
    if ( loadfunc != null )
        loadfunc( this );
}

// **********************************************************************
// Rocket plume via particles.  Its position is its base, it points in the -x
//   direction.

PhysicsObjects.RocketPlume = function( inparams )
{
    var params = { n: 1000,
                   period: 2,
                   ry: 0.5,
                   rz: 0.5,
                   lxctr: 2.0,
                   lxedge: 1.0,
                   colorbase: [ 1.0, 1.0, 0.0],
                   colorend: [ 0.3, 0., 0.],
                   pointsize: 0.08,
                   name: "RocketPlume"
                 }
    Object.assign( params, inparams );

    this.colorbase = [...params.colorbase];
    this.colorend = [...params.colorend];
    this.period = 2;

    params.rx = params.lxctr/2.;
    PhysicsObjects.Object.call( this, params );

    this.points = [];
    var positions = new Float32Array( params.n * 3 );
    var colors = new Float32Array( params.n * 3 );
    for ( var i = 0 ; i < params.n ; ++i ) {
        let phi = Math.random() * 2. * Math.PI;
        let r = Math.random();
        let y = r * params.ry * Math.cos( phi );
        let z = r * params.rz * Math.sin( phi )
        let xend = params.lxctr + ( params.lxedge - params.lxctr ) * r;
        let phase = Math.random();
        let x = -xend * phase;
        let red   = this.colorbase[0] + ( this.colorend[0] - this.colorbase[0] ) * phase;
        let green = this.colorbase[1] + ( this.colorend[1] - this.colorbase[1] ) * phase;
        let blue  = this.colorbase[2] + ( this.colorend[2] - this.colorbase[2] ) * phase;

        this.points.push( { r: r, y: y, z: z, xend: xend, phase: phase } );
        positions[ 3*i + 0 ] = x;
        positions[ 3*i + 1 ] = y;
        positions[ 3*i + 2 ] = z;
        colors[ 3*i + 0 ] = red;
        colors[ 3*i + 1 ] = green;
        colors[ 3*i + 2 ] = blue;
    }
        
    var geom = new THREE.BufferGeometry();
    geom.setAttribute( "position", new THREE.Float32BufferAttribute( positions, 3 ) );
    geom.setAttribute( "color", new THREE.Float32BufferAttribute( colors, 3 ) );
    geom.computeBoundingSphere();

    var textureloader = new THREE.TextureLoader();
    var texture = textureloader.load( PhysicsObjects.baseurl + "assets/blurball.png" );

    // Sadly, alpha stacking in OpenGL is a nightmare.  If I want
    //   it to work right, I have to sort all my points
    //   before each render, which is effort.
    var mat = new THREE.PointsMaterial( { size: params.pointsize,
                                          vertexColors: true,
                                          // alphaMap: texture,
                                          // transparent: true,
                                          // blending: THREE.CustomBlending,
                                          // blendEquation: THREE.AddEquation,
                                          // blendSrc: THREE.SrcAlphaFactor,
                                          // blendDst: THREE.OneFactor,
                                          // premultipliedAlpha: true
                                        } );
    this.visobj = new THREE.Points( geom, mat );
    this.visobj.name = this.name;

    var self = this;
    this.animationscene = null;
    this.animationt0 = 0;
    this.animationcallback = function( t ) { self.updatePoints( t ); };

    // Hack to force visobj to the right position
    this.position = this._position;
}

PhysicsObjects.RocketPlume.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsObjects.RocketPlume.prototype, 'constructor',
                       {
                           value: PhysicsObjects.RocketPlume,
                           enumerable: false,
                           writeable: true } );



PhysicsObjects.RocketPlume.prototype.animate = function()
{
    var scene = this;
    while ( (scene != undefined ) && ( scene != null ) && ( ! ( scene instanceof PhysicsObjects.Scene ) ) )
        scene = scene.parent;
    if ( scene == undefined || scene == null ) return;

    this.animationscene = scene;
    this.animationt0 = scene.t;
    this.animationscene.addAnimationCallback( this.animationcallback );
}

PhysicsObjects.RocketPlume.prototype.stopAnimate = function()
{
    if ( this.animationscene == null ) return;
    this.animationscene.removeAnimationCallback( this.animationcallback );
}

PhysicsObjects.RocketPlume.prototype.updatePoints = function( t )
{
    var dt = t - this.animationt0;
    var positions = this.visobj.geometry.attributes.position.array;
    var colors = this.visobj.geometry.attributes.color.array;
    for ( var i = 0 ; i < this.points.length ; ++i ) {
        var phase = this.points[i].phase + dt / this.period
        phase -= Math.trunc( phase );
        positions[ 3*i ] = -this.points[i].xend * phase;
        colors[ 3*i + 0 ] = this.colorbase[0] + ( this.colorend[0] - this.colorbase[0] ) * phase;
        colors[ 3*i + 1 ] = this.colorbase[1] + ( this.colorend[1] - this.colorbase[1] ) * phase;
        colors[ 3*i + 2 ] = this.colorbase[2] + ( this.colorend[2] - this.colorbase[2] ) * phase;
    }
    this.visobj.geometry.attributes.position.needsUpdate = true;
    this.visobj.geometry.attributes.color.needsUpdate = true;
    this.visobj.geometry.verticesNeedUpdate = true;
    this.visobj.geometry.colorsNeedUpdate = true
}

PhysicsObjects.RocketPlume.prototype.onRemove = function()
{
    this.stopAnimate();
}

// **********************************************************************

export { PhysicsObjects };
