// import * as THREE from 'https://unpkg.com/three@120/build/three.module.js';
import * as THREE from "https://brahms.phys.westminster.edu/physicsobjects/lib/three.module.js"


var TestPerf = function()
{
    var self = this;
    
    this.scene = new THREE.Scene();
    this.aspect = 0.75;
    
    var light = new THREE.DirectionalLight( 0xffffff, 1.5 );
    light.position.set( 4400, 4400, 4400 );
    this.scene.add( light );
    var light = new THREE.DirectionalLight( 0xffffff, 1.0 );
    light.position.set( -440, -2200, 4400 );
    this.scene.add( light );
    var light = new THREE.DirectionalLight( 0xffffff, 1.4 );
    light.position.set( 0, 100, -4400 );
    this.scene.add( light );

    var loader = new THREE.TextureLoader();
    var bgtexture = loader.load( "https://brahms.phys.westminster.edu/physicsobjects/assets/grid_single.png" );
    bgtexture.wrapT = THREE.RepeatWrapping;
    bgtexture.wrapS = THREE.RepeatWrapping;
    bgtexture.repeat.set( 10, 10 * this.aspect );
    this.scene.background = bgtexture;

    var spheregeom = new THREE.SphereGeometry( 1, 128, 64 );
    var spheremat = new THREE.MeshLambertMaterial( { color: 0xc04040 } );
    var sphere = new THREE.Mesh( spheregeom, spheremat );
    this.scene.add( sphere );

    this.camera = new THREE.PerspectiveCamera( 60., 1/this.apsect, 0.1, 1000 );
    this.camera.position.set( 0., 2., 10. );
    this.camera.lookAt( new THREE.Vector3( 0, 0, 0 ) );
    this.renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    var div = document.getElementById( "testperfenclosingdiv" );
    div.appendChild( this.renderer.domElement );
    this.renderer.domElement.setAttribute( "class", "glcanvas" );
    
    const resizeobs = new ResizeObserver( function( entries, obsever ) { self.resize( entries ); } );
    resizeobs.observe( this.renderer.domElement );

    this.lastframet = window.performance.now();
    this.framecount = 0;
    this.printfpsevery = 60;
}

TestPerf.prototype.resize = function( entries )
{
    console.log( "In resize" );
    for ( let entry of entries ) {
        if ( entry.target != this.renderer.domElement ) continue;
        var width = ( entry.contentRect.width ).toFixed( 0 );
        var height = ( this.aspect * width ).toFixed( 0 );
        this.renderer.setSize( width, height );
        this.camera.aspect = width/height;
        this.camera.updateProjectionMatrix();
    }
}

TestPerf.prototype.animate = function()
{
    var self = this;
    requestAnimationFrame( function() { self.animate() } );
    
    this.framecount += 1;
    if ( this.framecount >= this.printfpsevery ) {
        var t = window.performance.now();
        console.log( "fps: " + 1000 * this.framecount / ( t = this.lastframet) );
        this.framecount = 0;
        this.lastframet = t;
        // var info = this.renderer.info;
        // console.log( "info.memory.geometries= " + info.memory.geometries );
        // console.log( "           .textures  = " + info.memory.textures );
        // console.log( "info.render.calls     = " + info.render.calls );
        // console.log( "           .frame     = " + info.render.frame );
        // console.log( "           .lines     = " + info.render.lines );
        // console.log( "           .points    = " + info.render.points );
        // console.log( "           .triangles = " + info.render.triangles );
    }

    this.renderer.render( this.scene, this.camera );
}


export { TestPerf };
