import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { GLTFLoader } from './lib/GLTFLoader.js';


// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// **********************************************************************
// Utility

var dumpkids = function( obj, prefix )
{
    console.log( prefix + obj.name );
    for ( var kid of obj.children ) {
        dumpkids( kid, prefix + "...." );
    }
}


// **********************************************************************
// **********************************************************************
// **********************************************************************
// The cannon should start at a position of (0, 0, 0.6), with its wheels touching the ground.
// The vector from center of cannon to ball launch point is [Lx, Ly] = [ 1.34, 0.1744 ]
// If the cannon is at an angle of θ, then the ball launch point is at
//    [ Lx cos(θ) - Ly sin(θ)  ,  Lx sin(θ) + Ly cos(θ)  ,  0 ]
//// Bore diameter is about 0.153

var Cannon = function( inparams )
{
    var self = this;

    var params = { "loadfunction": null,
                   name: "cannon",
                   x: 0, y: 0, z: 0,
                   scale: 0,
                   azimuth: 0,
                   altitude: 0,
                   mass: 1 };
    Object.assign( params, inparams );
    PhysicsObjects.Object.call( this,  { name: params.name, mass: params.mass } );

    this.ready = false;
    this.error = false;
    this.visobj = null;

    this.nominalx = params.x;
    this.nominaly = params.y;
    this.nominalz = params.z;

    this.objaboveground = 0.6;    // Height of object position above the ground
    this.Lx = 1.34;               // Vector from object position to end of cannon barrel
    this.Ly = 0.1744;
    this.Lz = 0.;
    this.ballrad = 0.076;
    
    this._azimuth = 0.;
    this._altitude = 0.;
    
    var loader = new GLTFLoader();
    loader.load( PhysicsObjects.baseurl + "assets/cannon.glb",
                 function( gltf ) { self.loaded( gltf, params.loadfunction ) },
                 function( xhdr ) { console.log( "Cannon loaded fraction:" + xhdr.loaded / xhdr.total ); },
                 function( err ) {
                     self.ready = false;
                     self.error = true
                     console.log( "Error loading cannon: " + err );
                 } );
}

Cannon.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( Cannon.prototype, 'constructor',
                       {
                           value: Cannon,
                           enumerate: false,
                           writeable: true } );

Object.defineProperty( Cannon.prototype, "altitude", {
    get() { return this._altitude; },
    set( val ) {
        this._altitude = val;
        this.updateModel();
    }
} );
    
Object.defineProperty( Cannon.prototype, "azimuth", {
    get() { return this._azimuth; },
    set( val ) {
        this._azimuth = val;
        this.updateModel();
    }
} );

// **********************************************************************

Cannon.prototype.loaded = function( gltf, loadfunction )
{
    this.ready = true;
    this.visobj = gltf.scene;
    this.updateModel();
    if ( loadfunction != null ) {
        loadfunction( this );
    }

    // console.log( "Loaded object: " + this.visobj.name );
    // dumpkids( this.visobj, "...." );
}

// **********************************************************************

Cannon.prototype.updateModel = function()
{
    if ( this.visobj == null ) return;

    // console.log( "Setting cannon visobj position to ( " +
    //              this._position.x.toFixed(2) + ", " +
    //              this._position.y.toFixed(2) + ", " +
    //              this._position.z.toFixed(2) + ")" );
    this.visobj.position.x = this._position.x;
    this.visobj.position.y = this._position.y;
    this.visobj.position.z = this._position.z;
    var obj = this.visobj.getObjectByName( "Cannon_Barrel" );
    if ( obj != null && obj != undefined ) {
        // console.log( "Setting altitude to " + this._altitude + " and azimuth to " + this._azimuth );
        obj.setRotationFromAxisAngle( new THREE.Vector3( 0, 0, 1), this._altitude );
        this.visobj.setRotationFromAxisAngle( new THREE.Vector3( 0, 1, 0), this._azimuth );
    }
    else {
        console.log( "ERROR, coudn't find object \"Cannon Barrel\"" );
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// The asset is 200×200×2.  Scaling it in x and z will do weird things, so don't
//   set length and width to other than the default.

var Defilade = function( cannon, inparams )
{
    var params = { loadfunction: null,
                   length: 200,
                   width: 200,
                   texrep: 40 };
    Object.assign( params, inparams );

    // ry is wrog....
    PhysicsObjects.Object.call( this, { rx: params.length/2, ry: 0, rz: params.width/2, name: "Defilade" } );

    var self = this;
    
    this.cannon = cannon;
    this.error = false;
    this.visobj = null;

    this.fulldepth = 2.00;
    this.length = params.length;
    this.width = params.width;
    this.texrep = params.texrep;
    
    var loader = new GLTFLoader();
    loader.load( PhysicsObjects.baseurl + "assets/defilade.glb",
                 function( gltf ) { self.loaded( gltf, params.loadfunction ); },
                 function( xhdr) { console.log( "Defilade load fraction:" + xhdr.loaded / xhdr.total ); },
                 function( err ) {
                     self.error = true;
                     console.log( "Error loading defilade: " + err );
                 } );
}

// **********************************************************************

Defilade.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( Defilade.prototype, 'constructor',
                       {
                           value: Defilade,
                           enumerate: false,
                           writeable: true } );



// **********************************************************************

Defilade.prototype.loaded = function( gltf, loadfunction )
{
    this.visobj = gltf.scene;
    this.setCannonAngle( );

    var obj = this.visobj.getObjectByName( "Ground_with_Dip" );
    if ( obj == null || obj == undefined ) {
        console.log( "ERROR, couldn't find object \"Ground_with_Dip\"" );
        return;
    }

    this.visobj.scale.x = this.width / 200.;
    this.visobj.scale.z = this.length / 200.;
    
    var textureloader = new THREE.TextureLoader();
    var texture = textureloader.load( PhysicsObjects.baseurl + "assets/grass-field-seamless-texture-720x720.jpg" );
    texture.wrapT = THREE.RepeatWrapping;
    texture.wrapS = THREE.RepeatWrapping;
    if ( this.length > this.width )
        texture.repeat.set( this.texrep, this.length/this.width * this.texrep );
    else
        texture.repeat.set( this.width/this.length * this.texrep, this.texrep );
    texture.updateMatrix();

    obj.material = new THREE.MeshLambertMaterial( { color: "#339933", map: texture } );

    if ( loadfunction != null )
        loadfunction( this );

    // console.log( "Loaded object: " + this.visobj.name );
    // dumpkids( this.visobj, "...." );
}

// **********************************************************************

Defilade.prototype.setCannonAngle = function( disable=false )
{
    // console.log( "Setting cannon angle with diable=" + disable );
    if ( disable ) {
        this.cannon.x = this.cannon.nominalx;
        this.cannon.y = this.cannon.nominaly;
        this.cannon.z = this.cannon.nominalz;
        this.visobj.scale.y = 0.01;      // 0 causes it to crap out
    }
    else {
        var alt = this.cannon.altitude;
        var az = this.cannon.azimuth;
        var launchaboveobj = ( this.cannon.Lx * Math.sin( alt ) + this.cannon.Ly * Math.cos( alt ) );
        var launchlateral  = ( this.cannon.Lx * Math.cos( alt ) - this.cannon.Ly * Math.sin( alt ) );
        this.cannon.y = this.cannon.nominaly - this.cannon.objaboveground - launchaboveobj + this.cannon.ballrad;
        this.cannon.x = this.cannon.nominalx - launchlateral * Math.cos( az );
        this.cannon.z = this.cannon.nominalz + launchlateral * Math.sin( az );
        if ( this.visobj != null ) {
            let scale = -this.cannon.y / this.fulldepth;
            if ( scale < 0.01 ) scale = 0.01;
            this.visobj.scale.y = scale;
        }
    }
    this.cannon.updateModel();
}


// **********************************************************************
// **********************************************************************
// **********************************************************************

var CannonBall = function( rad, pos, vel, scene, inparams )
{
    var self = this;

    var params = { trailcolor: null,
                   traildt: 0.01,
                   rx: rad,
                   ry: rad,
                   rz: rad,
                   focusrad: 2*rad,
                   focuslen: 40*rad,
                   focusoffset: 10*rad,
                   focuscolor: 0xff4040,
                   name: null,
                   world: scene,
                   accelfunc: function() { return self.accel() },
                   datalisteners: []
                 };
    Object.assign( params, inparams );
                   
                   
    PhysicsObjects.Object.call( this, params )

    this.rad = rad;
    this.scene = scene;
    this.focusrad = params.focusrad;
    this.focuslen = params.focuslen;
    this.focusoffset = params.focusoffset;
    this.focuscolor = params.focuscolor;
    this.focused = false;

    if ( params.name == null ) {
        CannonBall.globalordinal += 1;
        params.name = "CannonBall " + CannonBall.globalordinal;
    }
    this.name = params.name;
    
    var spheregeom = new THREE.SphereGeometry( rad );
    var spheremat = new THREE.MeshStandardMaterial( { color: 0x666666,
                                                      metalness: 0.5,
                                                      roughness: 0.2 } );
    this.visobj = new THREE.Mesh( spheregeom, spheremat );
    this.scene.addObject( this );
    this.position = pos;
    this.velocity = vel;
    this.r0 = { x: pos.x, y: pos.y, z: pos.z };
    this.v0 = { x: vel.x, y: vel.y, z: vel.z };
    this.ymax = pos.y;
    this.t0 = this.t;
    
    this.callback = function() { self.update() };
    this.scene.addAnimationCallback( this.callback );

    this.trail = false;
    if ( params.trailcolor != null ) {
        this.trail = true;
        this.trailgeo = new THREE.CylinderGeometry( rad/2, rad/2, 1, 6 );
        this.trailmat = new THREE.MeshLambertMaterial( { color: params.trailcolor } );
        this.last2trailpos = { x: pos.x, y: pos.y, z: pos.z };
        this.lasttrailpos = { x: pos.x, y: pos.y, z: pos.z };
        this.lasttrailt = this.t;
        this.trailcyls = [];
        this.traildt = params.traildt;
    }

    // Make a focus object for finding the ball
    
    var shaftgeom = new THREE.CylinderGeometry( this.focusrad, this.focusrad, this.focuslen, 16 );
    var conegeom = new THREE.ConeGeometry( 2*this.focusrad, 4*this.focusrad, 16 );
    var focusmat = new THREE.MeshLambertMaterial( { color: this.focuscolor } );
    // I am surprised this is necessary for ConeGeometry
    conegeom.mergeVertices();
    conegeom.computeVertexNormals();

    this.focusobj = new THREE.Group();
    var shaft = new THREE.Mesh( shaftgeom, focusmat );
    shaft.position.y = 4*this.focusrad + this.focuslen/2;
    this.focusobj.add( shaft );
    var head = new THREE.Mesh( conegeom, focusmat );
    head.rotateX( Math.PI );
    head.position.y = 2*this.focusrad;
    this.focusobj.add( head );
    this.focusobj.position.set( 0, this.focusoffset, 0 );

    this.movelisteners = [];
    this.datalisteners = params.datalisteners;
}


CannonBall.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( CannonBall.prototype, 'constructor',
                       {
                           value: CannonBall,
                           enumerate: false,
                           writeable: true } );


CannonBall.globalordinal = 0;

// **********************************************************************

CannonBall.prototype.addMoveListener = function( callback )
{
    if ( this.movelisteners.indexOf( callback ) < 0 )
        this.movelisteners.push( callback )
}

// **********************************************************************

CannonBall.prototype.removeMoveListener = function( callback )
{
    var dex = this.movelisteners.indexOf( callback );
    this.movelisteners.splice( dex, 1 );
}

// **********************************************************************

CannonBall.prototype.accel = function() {
    return { x: 0, y: -9.8, z: 0 };
}

// **********************************************************************

CannonBall.prototype.update = function()
{
    this.updateState();
    if ( this.y > this.ymax) this.ymax = this.y;
    
    // Handle trail

    if ( this.trail && ( this.t - this.lasttrailt > this.traildt ) )
        this.addToTrail();
    
    // See if we've landed
    
    if ( this.y <= this.rad ) {
        console.log( "Cannonball has landed!  t = " + this.t );
        // cheat and put it above ground
        var dy = this.rad - this.y;
        var dt = (-dy) / this.vy;
        this.y = this.rad;
        this.x += dy / this.vy * this.vx;
        this.z += dy / this.vy * this.vz;
        // also try to correct the time and velocity, with a similar linear approximation
        this.t -= dt;
        this.vx -= this.ax * dt;
        this.vy -= this.ay * dt;
        this.vz -= this.az * dt;
        if ( this.trail ) {
            //Replace last cylinder of trail
            this.scene.scene.remove( this.trailcyls[ this.trailcyls.length-1 ] );
            this.trailcyls.splice( this.trailcyls.length-1 );
            this.lasttrailpos.x = this.last2trailpos.x;
            this.lasttrailpos.y = this.last2trailpos.y;
            this.lasttrailpos.z = this.last2trailpos.z;
            this.addToTrail();
        }        
        this.scene.removeAnimationCallback( this.callback );
        if ( this.datalisteners.length > 0 ) {
            var send = { t: this.t - this.t0,
                         x0: this.r0.x,
                         y0: this.r0.y,
                         z0: this.r0.z,
                         x: this.x,
                         y: this.y,
                         z: this.z,
                         vx0: this.v0.x,
                         vy0: this.v0.y,
                         vz0: this.v0.z,
                         vx: this.vx,
                         vy: this.vy,
                         vz: this.vz,
                         ymax: this.ymax,
                         name: this.name
                       };
            for ( var func of this.datalisteners ) {
                func( send );
            }
        }
    }

    for ( var callback of this.movelisteners ) {
        callback( this );
    }
}

// **********************************************************************

CannonBall.prototype.addToTrail = function()
{
    var cyl = new THREE.Mesh( this.trailgeo, this.trailmat );
    var dx = this.x - this.lasttrailpos.x;
    var dy = this.y - this.lasttrailpos.y;
    var dz = this.z - this.lasttrailpos.z;
    var ds = Math.sqrt( dx*dx + dy*dy + dz*dz );
    var theta = Math.acos( dy / ds );
    var phi = Math.atan2( -dz , dx );
    cyl.rotateY( phi );
    cyl.rotateZ( -theta );
    cyl.scale.y = ds;
    cyl.position.x = this.lasttrailpos.x + dx/2;
    cyl.position.y = this.lasttrailpos.y + dy/2;
    cyl.position.z = this.lasttrailpos.z + dz/2;

    this.trailcyls.push( cyl );
    this.scene.scene.add( cyl );

    this.lasttrailt = this.t;
    this.last2trailpos.x = this.lasttrailpos.x;
    this.last2trailpos.y = this.lasttrailpos.y;
    this.last2trailpos.z = this.lasttrailpos.z;
    this.lasttrailpos.x = this.x;
    this.lasttrailpos.y = this.y;
    this.lasttrailpos.z = this.z;
}


// **********************************************************************

CannonBall.prototype.focus = function()
{
    if ( this.focused ) return
    this.visobj.add( this.focusobj );
    this.focused = true;
}

// **********************************************************************

CannonBall.prototype.clearFocus = function()
{
    if ( ! this.focused ) return;
    this.visobj.remove( this.focusobj );
    this.focused = false;
}

// **********************************************************************

CannonBall.prototype.die = function()
{
    this.movelisteners = [];
    this.datalisteners = [];
    this.scene.removeAnimationCallback( this.callback );
    for ( var cyl of this.trailcyls )
        this.scene.scene.remove( cyl );
    this.trailcyls = [];
    this.scene.removeObject( this );
}

// **********************************************************************

export { Cannon, CannonBall, Defilade };
