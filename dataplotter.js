import { InterfaceMaker } from "./interfacemaker.js";
import { SVGPlot } from "./svgplot.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var DataPlotter = function( inparams = {} )
{
    var self = this;
    
    var params = { idbase: null,
                   title: "",
                   xtitle: "",
                   ytitle: "",
                   datax: [],
                   datay: [],
                   divid: "dataplotterdiv",
                   svgid: "dataplottersvg",
                   parent: document.body
                 };
    Object.assign( params, inparams );

    this.idbase = params.idbase;
    if ( this.idbase == null ) {
        let hex = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' ];
        this.idbase = "";
        for ( let i = 0 ; i < 6 ; ++i ) {
            this.idbase += hex[ Math.floor( Math.random()*16 ) ];
        }
    }
    
    this.datax = [...params.datax];
    this.datay = [...params.datay];
    if ( this.datax.length != this.datay.length ) {
        window.alert( "Error!  Tried to create a DataPlotter with " + this.datax.length +
                      " x values and " + this.datay.length + " y values; they must match." );
        return;
    }

    // I'm sure I'm overdoing this, but I think I need
    //   all this nesting to get resizing and flex working right
    this.bodydiv = document.createElement( "div" );
    this.bodydiv.setAttribute( "class", "bodydiv" );
    params.parent.appendChild( this.bodydiv );
    
    this.topdiv = document.createElement( "div" );
    this.topdiv.setAttribute( "class", "maindiv" );
    this.bodydiv.appendChild( this.topdiv );

    this.svgplot = new SVGPlot.Plot( { divid: params.divid,
                                       svgid: params.svgid,
                                       title: params.title,
                                       xtitle: params.xtitle,
                                       ytitle: params.ytitle,
                                       width: 800,
                                       height: 600
                                     } );

    this.dataset = new SVGPlot.Dataset();
    this.svgplot.addDataset( this.dataset );

    this.topdiv.appendChild( this.svgplot.topdiv );

    var div = document.createElement( "div" );
    div.setAttribute( "class", "growingspacer" );
    this.svgplot.topdiv.appendChild( div );
    
    var controls = {
        type: "box",
        _class: "controlsdiv",
        id: this.idbase + "controlsdiv",
        spec: [
            { type: "span",
              id: this.idbase + "title_label",
              text: "Title:"
            },
            { type: "input",
              width: "16ex",
              id: this.idbase + "title",
              initval: params.title,
              changecallback: function() {
                  self.svgplot.title = self.controls[ self.idbase + "title" ].value;
              }
            },
            { type: "span",
              id: this.idbase + "title_xlabel",
              text: "X title:"
            },
            { type: "input",
              width: "16ex",
              id: this.idbase + "xtitle",
              initval: params.xtitle,
              changecallback: function() {
                  self.svgplot.xtitle = self.controls[ self.idbase + "xtitle" ].value;
              }
            },
            { type: "span",
              id: this.idbase + "title_ylabel",
              text: "Y title:"
            },
            { type: "input",
              width: "16ex",
              id: this.idbase + "ytitle",
              initval: params.ytitle,
              changecallback: function() {
                  self.svgplot.ytitle = self.controls[ self.idbase + "ytitle" ].value;
              }
            },
            { type: "span",      // hack alert
              text: " ",
              id: this.idbase + "spacer2",
            },
            { type: "span",
              id: this.idbase + "adddatalabel",
              text: "Add Point:"
            },
            { type: "box",
              _class: "_hbox",
              id: this.idbase + "xdatabox",
              spec: [
                  { type: "span",
                    id: this.idbase + "newxlabel",
                    text: "x:",
                  },
                  { type: "input",
                    id: this.idbase + "newx",
                    size: 8,
                  }
              ]
            },
            { type: "box",
              _class: "_hbox",
              id: this.idbase + "ydatabox",
              spec: [
                  { type: "span",
                    id: this.idbase + "newylabel",
                    text: "y:",
                  },
                  { type: "input",
                    id: this.idbase + "newy",
                    size: 8,
                  }
              ]
            },
            { type: "button",
              id: this.idbase + "addpointbutton",
              text: "Add Point",
              callback: function() { self.addPoint(); }
            },
            { type: "span",      // hack alert
              text: " ",
              id: this.idbase + "spacer1",
            },
            { type: "tablewidget",
              headers: [ "x", "y" ],
              id: this.idbase + "datatable",
              multiselect: true,
              columns: 2
            },
            { type: "button",
              id: this.idbase + "deletedatabutton",
              _class: "marginblock",
              text: "Delete Selected Data",
              callback: function() { self.deleteSelected(); }
            },
        ]
    };

    this.controls = InterfaceMaker( controls );
    
    this.topdiv.appendChild( this.controls[ this.idbase + "controlsdiv" ] );

    this.svgplot.redraw();
}

// **********************************************************************

DataPlotter.prototype.addPoint = function()
{
    var xstr = this.controls[ this.idbase + "newx" ].value;
    var ystr = this.controls[ this.idbase + "newy" ].value
    var x = parseFloat( xstr  );
    var y = parseFloat( ystr );
    if ( isNaN(x) || isNaN(y) ) {
        window.alert( "Either x=\"" + xstr + "\" or y=\"" + ystr + "\" isn't a number." );
        return;
    }
    var ord = this.controls[ this.idbase + "datatable" ].addItem( [ x, y ] );
    this.dataset.addPoint( x, y, ord );
    this.svgplot.redraw();
    this.controls[ this.idbase + "newx" ].value = "";
    this.controls[ this.idbase + "newy" ].value = "";
    
}

// **********************************************************************

DataPlotter.prototype.deleteSelected = function()
{
    var selected = this.controls[ this.idbase + "datatable" ].getSelectedHandles();
    for ( let sel of selected ) {
        this.dataset.removePoint( sel );
        this.controls[ this.idbase + "datatable" ].removeItem( sel );
    }
    this.svgplot.redraw();
}

// **********************************************************************

export { DataPlotter };
