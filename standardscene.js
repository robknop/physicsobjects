import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// On instantiation, creates a PhysicsObjects Scene, one or more
// PhsyicsObjects Views.
//
// Call layout(), it will create a bunch of DOM elements and add
// them to the document body.  These DOM elements are properties
// of the StandardScene object:
//
//   maindiv — (class maindiv) The div that is appended to the document body
//
//   leftbigbox — (class leftbigbox) A child of maindiv in which the
//                views wil go.  Exists so that you can set maindiv as a
//                row flexbox, and add something else to maindiv
//                alongside the scenes
//
//   viewcontainer  — (class viewcontainer) A child of leftbigbox in
//                    which the views will go.  Exists because there
//                    is the option of having a "growingspacer" div
//                    at the bottom of leftbigbox that (with standard
//                    CSS properties) will push viewcontainer to the top
//   
//   viewdivs[n] — inside viewcontainer, id "viewparentdivn" and class
//                 "viewparentdiv viewparentdivn" (where n is replaced
//                 by the number, counting from 0). the div for the nth
//                 scene.  This will have two children; the first is a
//                 camera controls button that opens up an absolutely
//                 positioned div (a popup menu), and the actual div in
//                 which the view DOM element goes (id "viewdivn" and
//                 class "viewdiv viewdivn", where again n is replaced
//                 by the 0-offset number)
//  
// Other key properties include:
//     views — Array of PhysicObject Views

var StandardScene = function( inparams )
{
    var self = this;

    this.params = { name: "PhysicsOjbects Scene",
                    lights: [
                        { type: "directional",
                          pos: [ 4400, 4400, 4400 ],
                          color: 0xffffff,
                          intensity: 1.5 
                        },
                        { type: "directional",
                          pos: [ -4400, -2200, 4400 ],
                          color: 0xffffff,
                          intensity: 1.0 
                        },
                        { type: "directional",
                          pos: [ 0, 100, -4400 ],
                          color: 0xffffff,
                          intensity: 1.4
                        }
                    ],
                    alpha: false,
                    absoluterotate: true,
                    rotfac: 2.,
                    gridbg: true,
                    gridbghorizcount: 12,
                    bgimage: null,
                    bgcolor: 0x000000,
                    axeslen: 2,
                    axesemit: 0,
                    showaxes: false,
                    debugfocusscale: 0.25,
                    debugfocuscolor: 0xffff00,
                    debugfocusemit: 0x000000,
                    printfpsevery: 0,
                    growingspacer: true,
                    bboxoption: false,
                    dumpcameraoption: false,
                    nviews: 1,
                    aspect: 0.5,                                     // These things are default
                    campos: new THREE.Vector3( 0., 2., 10. ),
                    camfov: 60.,
                    camlook: new THREE.Vector3( 0., 0., 0. ),
                    // aspect0: 0.5,                                    // Override default for specific views
                    // campos0: new THREE.Vector3( 0., 2., 10. ),       
                    // camfov0: 60.,
                    // camlook0: new THREE.Vector3( 0., 0., 0. ),
                    // aspect1: 0.5,
                    // campos1: new THREE.Vector3( 0., 2., 10. ),
                    // camfov1: 60.,
                    // camlook1: new THREE.Vector3( 0., 0., 0. ),
                  };
    Object.assign( this.params, inparams );
    
    this.scene = new PhysicsObjects.Scene( this.params );
    this.views = [];
    this.viewdivs = [];
    this.viewlayouts = [];
    for ( let i = 0 ; i < this.params.nviews ; ++i ) {
        let viewparams = {};
        Object.assign( viewparams, this.params );
        for ( let param of [ "aspect", "campos", "camfov", "camlook" ] ) {
            if ( viewparams.hasOwnProperty( param + i ) )
                viewparams[ param ] = viewparams[ param + i ];
        }
        var view = new PhysicsObjects.View( this.scene, viewparams );
        view.absoluterotate = this.params.absoluterotate;
        this.views.push( view );
    }

    var loader = new THREE.TextureLoader();
    if ( this.params.gridbg ) {
        let bgtexture = loader.load( PhysicsObjects.baseurl + "assets/grid_single.png" );
        bgtexture.wrapT = THREE.RepeatWrapping;
        bgtexture.wrapS = THREE.RepeatWrapping;
        bgtexture.repeat.set( this.params.gridbghorizcount, this.params.aspect * this.params.gridbghorizcount );
        this.scene.scene.background = bgtexture;
    }
    else if ( this.params.bgimage != null ) {
        let bgtexture = loader.load( this.params.bgimage );
        this.scene.scene.background = bgtexture;
    }
    else {
        this.scene.scene.background = new THREE.Color( this.params.bgcolor );
    }

    this.scene.debugfocuscolor = this.params.debugfocuscolor;
    this.scene.debugfocusscale = this.params.debugfocusscale;
    this.scene.debugfocusemit = this.params.debugfocusemit;

    this.axesobj = new PhysicsObjects.Axes( this.params.axeslen, this.params.axesemit );
    if ( this.params.showaxes )
        this.scene.addObject( this.axesobj );

    this.bboxshown = false;
}

// **********************************************************************

Object.defineProperty( StandardScene.prototype, "axes", {
    writeable: true,
    enumerable: true,
    get() { return this.params.showaxes; },
    set( val ) {
        if ( this.params.showaxes && ! val ) {
            this.scene.removeObject( this.axesobj );
            this.params.showaxes = false;
            for ( let viewlayout of this.viewlayouts )
                viewlayout.axescheckbox.checked = false;
        }
        else if ( val && ! this.params.showaxes ) {
            this.scene.addObject( this.axesobj );
            this.params.showaxes = true;
            for ( let viewlayout of this.viewlayouts )
                viewlayout.axescheckbox.checked = true;
        }
    }
} );

Object.defineProperty( StandardScene.prototype, "debugfocus", {
    writeable: true,
    enumerable: true,
    get() { return this.scene.debugfocus; },
    set( val ) {
        if ( this.scene.debugfocus && ! val ) {
            this.scene.debugfocus = false;
            for ( let viewlayout of this.viewlayouts )
                viewlayout.debugfocuscheckbox.checked = false;
        }
        else if ( val && ! this.params.showaxes ) {
            this.scene.debugfocus = true;
            for ( let viewlayout of this.viewlayouts )
                viewlayout.debugfocuscheckbox.checked = true;
        }
    }
} );

Object.defineProperty( StandardScene.prototype, "showbbox", {
    writeable: true,
    enumerable: true,
    get() { return this.bboxshown },
    set( val ) {
        if ( ! val ) {
            if ( this.bboxshown ) {
                this.scene.scene.remove( this.bbox );
                this.bboxshown = false;
                this.bbox = null;
                for ( let viewlayout of this.viewlayouts )
                    viewlayout.bboxcheckbox.checked = false;
            }
        }
        else {
            if ( this.bboxshown ) this.scene.scene.remove( this.bbox );
            // let box = new THREE.Box3();
            // box.setFromObject( this.scene.scene );
            // console.log( "box: " +
            //              box.min.x + ":" + box.max.x + " , ",
            //              box.min.y + ":" + box.max.y + " , ",
            //              box.min.z + ":" + box.max.z );
            // let boxgeom = new THREE.BoxGeometry( box.max.x-box.min.x , box.max.y-box.min.y , box.max.z-box.min.z );
            // boxgeom.translate( ( box.min.x + box.max.x ) / 2.,
            //                    ( box.min.y + box.max.y ) / 2.,
            //                    ( box.min.z + box.max.z ) / 2. );
            // let wire = new THREE.WireframeGeometry( boxgeom );
            // this.bbox = new THREE.LineSegments( wire );
            this.bbox = new THREE.BoxHelper( this.scene.scene );
            this.scene.scene.add( this.bbox );
            this.bboxshown = true;
            for ( let viewlayout of this.viewlayouts )
                viewlayout.bboxcheckbox.checked = true;
        }
    }
} );

// **********************************************************************

StandardScene.prototype.layout = function()
{
    var hbox, vbox, span, br, div, viewcontainer, button, hr;
    var self = this;

    var body = document.body;

    this.maindiv = document.createElement( "div" );
    this.maindiv.setAttribute( "class", "maindiv" );
    body.appendChild( this.maindiv );

    this.leftbigbox = document.createElement( "div" );
    this.leftbigbox.setAttribute( "class", "leftbigbox" );
    this.maindiv.appendChild( this.leftbigbox );

    viewcontainer = document.createElement( "div" );
    viewcontainer.setAttribute( "class", "viewcontainer" );
    this.leftbigbox.appendChild( viewcontainer );

    for ( let i = 0 ; i < this.views.length ; ++i ) {
        div = document.createElement( "div" );
        div.setAttribute( "id", "viewparentdiv" + i );
        div.setAttribute( "class", "viewparentdiv viewparentdiv" + i );
        viewcontainer.appendChild( div );
        let viewlayout = new StandardScene.ViewLayout( this, this.views[i], i, div );
        this.viewdivs.push( div );
        this.viewlayouts.push( viewlayout );
    }
    
    if ( this.params.growingspacer ) {
        this.growingspacer = document.createElement( "div" );
        this.growingspacer.setAttribute( "class", "growingspacer" );
        this.leftbigbox.appendChild( this.growingspacer );
    }
}

// **********************************************************************

StandardScene.prototype.animate = function()
{
    for ( let view of this.views )
        view.animate();
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

StandardScene.ViewLayout = function( standardscene, view, num, parentdiv )
{
    this.stdscene = standardscene;
    this.num = num;
    this.view = view;
    this.parentdiv = parentdiv;

    var hbox, button, div;
    var self = this;
    
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    this.parentdiv.appendChild( hbox );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Camera Controls" ) );
    hbox.appendChild( button );
    button.addEventListener( "click", function() { self.cameracontrolsdiv.style.display = "flex" } );
    
    this.cameracontrolsdiv = document.createElement( "div" );
    this.cameracontrolsdiv.setAttribute( "class", "cameracontrolsdiv" );
    this.parentdiv.appendChild( this.cameracontrolsdiv );
    this.cameracontrolsdiv.addEventListener( "mouseleave",
                                             function() { self.cameracontrolsdiv.style.display = "none" } );

    
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Reset View" ) );
    this.cameracontrolsdiv.appendChild( button );
    button.addEventListener( "click", function() { self.view.camerahome() } );

    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Zoom—Show All" ) );
    this.cameracontrolsdiv.appendChild( button );
    button.addEventListener( "click", function() { self.view.cameraZoomAll() } );

    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox margintop" );
    this.cameracontrolsdiv.appendChild( hbox );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "+X" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "x" ); } );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "-X" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "-x" ); } );
    
    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox" );
    this.cameracontrolsdiv.appendChild( hbox );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "+Y" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "y" ); } );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "-Y" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "-y" ); } );
    
    hbox = document.createElement( "hbox" );
    hbox.setAttribute( "class", "hbox" );
    this.cameracontrolsdiv.appendChild( hbox );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "+Z" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "z" ); } );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "-Z" ) );
    hbox.appendChild( button );
    button.addEventListener( "click",function() { self.view.cameraToAxis( "-z" ); } );
    
    div = document.createElement( "div" );
    this.cameracontrolsdiv.appendChild( div );
    div.setAttribute( "class", "margintop" );
    div.appendChild( document.createTextNode( "Shift+LMB: Rotate") );
    div = document.createElement( "div" );
    this.cameracontrolsdiv.appendChild( div );
    div.appendChild( document.createTextNode( "CTRL+LMB: Move" ) );
    div = document.createElement( "div" );
    this.cameracontrolsdiv.appendChild( div );
    div.appendChild( document.createTextNode( "Shift+CTRL+LMB: Zoom" ) );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox margintop" );
    this.cameracontrolsdiv.appendChild( hbox );
    this.axescheckbox = document.createElement( "input" );
    this.axescheckbox.setAttribute( "type", "checkbox" );
    this.axescheckbox.checked = this.stdscene.axes;
    hbox.appendChild( this.axescheckbox );
    hbox.appendChild( document.createTextNode( "axes" ) );
    this.axescheckbox.addEventListener( "change", function() {
        self.stdscene.axes = self.axescheckbox.checked;
    } );
    
    if ( this.stdscene.params.bboxoption ) {
        hbox = document.createElement( "div" );
        hbox.setAttribute( "class", "hbox" );
        this.cameracontrolsdiv.appendChild( hbox );
        this.bboxcheckbox = document.createElement( "input" );
        this.bboxcheckbox.setAttribute( "type", "checkbox" );
        this.bboxcheckbox.checked = this.stdscene.showbbox;
        hbox.appendChild( this.bboxcheckbox );
        hbox.appendChild( document.createTextNode( "bounding box" ) );
        this.bboxcheckbox.addEventListener( "change", function() {
            self.stdscene.showbbox = self.bboxcheckbox.checked;
        } );
    }
    
    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    this.cameracontrolsdiv.appendChild( hbox );
    this.debugfocuscheckbox = document.createElement( "input" );
    this.debugfocuscheckbox.setAttribute( "type", "checkbox" );
    this.debugfocuscheckbox.checked = this.stdscene.debugfocus;
    hbox.appendChild( this.debugfocuscheckbox );
    hbox.appendChild( document.createTextNode( "cam. target" ) );
    this.debugfocuscheckbox.addEventListener( "change", function() {
        self.stdscene.debugfocus = self.debugfocuscheckbox.checked;
    } );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    this.cameracontrolsdiv.appendChild( hbox );
    this.absrotcheckbox = document.createElement( "input" );
    this.absrotcheckbox.setAttribute( "type", "checkbox" );
    this.absrotcheckbox.checked = this.view.absoluterotate;
    hbox.appendChild( this.absrotcheckbox );
    hbox.appendChild( document.createTextNode( "Keep y up" ) );
    this.absrotcheckbox.addEventListener( "change", function() {
        self.view.absoluterotate = self.absrotcheckbox.checked;
        if ( self.view.absoluterotate ) self.view.camerayup();
    } );

    if ( this.stdscene.params.dumpcameraoption ) {
        button = document.createElement( "button" );
        button.appendChild( document.createTextNode( "Dump Camera" ) );
        button.setAttribute( "class", "margintop" );
        this.cameracontrolsdiv.appendChild( button );
        button.addEventListener( "click", function() {
            console.log( "campos: " + self.view.campos.x + ", " + self.view.campos.y + ", " + self.view.campos.z );
            console.log( "camlook: " + self.view.camlook.x + ", " + self.view.camlook.y + ", " + self.view.camlook.z );
            console.log( "near: " + self.view.camera.near + " ; far: " + self.view.camera.far +
                         " ; fov: " + self.view.camera.fov );
            console.log( "position: " + self.view.camera.position.x + ", " + self.view.camera.position.y +
                         ", " + self.view.camera.position.z );
            console.log( "rotation: " + self.view.camera.rotation.x + ", " + self.view.camera.rotation.y +
                         ", " + self.view.camera.rotation.z );
        } );
    }
                        
    
    div = document.createElement( "div" );
    div.setAttribute( "id", "viewdiv" + this.num );
    div.setAttribute( "class", "viewdiv viewdiv" + this.num );
    div.appendChild( this.view.elem );
    this.parentdiv.appendChild( div );
}

export { StandardScene };
