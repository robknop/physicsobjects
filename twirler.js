import * as THREE from "./lib/three.module.js";
import { GLTFLoader } from './lib/GLTFLoader.js';
import { PhysicsObjects } from "./physicsobjects.js";
import { StandardScene } from "./standardscene.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { SVGPlot } from "./svgplot.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var Twirler = function( inparams )
{
    var self = this;

    var params = { "name": "Twirler",
                   "pegh": 0.1,
                   "pegr": 0.05,
                   "pegcolor": 0xff4040,
                   "aspect": 0.5,
                   "campos": new THREE.Vector3( 0., 0.5, 2. ),
                   "absoluterotate": true,
                   "gridbg": true,
                   "debugfocusscale": 0.2,
                   "showaxes": false,
                   "axeslen": 1.2,
                   "plotdt": 0.1,
                   "mu_s": 0.6,
                   "mu_k": 0.3,
                   "showmus": true,
                   "showomega1": true
                 };
    Object.assign( params, inparams );

    StandardScene.call( this, params );
    
    this.turntable = new Twirler.Turntable(
        { loadfunction: function() { self.scene.addObject( self.turntable );  } }
    );
    this.turntablerad = ( this.turntable.x1 - this.turntable.x0 ) / 2.;
    
    var cylgeom = new THREE.CylinderGeometry( params.pegr, params.pegr, params.pegh, 16, 1 );
    var cylmat = new THREE.MeshLambertMaterial( { color: params.pegcolor } );
    var cyl = new THREE.Mesh( cylgeom, cylmat );
    this.peg = new PhysicsObjects.Object( { "name": "Peg",
                                            "parent": this.scene,
                                            "mass": 0.2,
                                            "visobj": cyl,
                                            "x": this.turntablerad - 1.5 * params.pegr,
                                            "y": this.turntable.topy + 0.5 * params.pegh,
                                            "z": 0,
                                            "rx": params.pegr,
                                            "ry": params.pegh/2.,
                                            "rz": params.pegr,
                                            "accelfunc": function( t, pos, vel, obj, dt)
                                            {
                                                return self.pegAccel( t, pos, vel, obj, dt );
                                            }
                                          } );
    this.scene.addObject( this.peg );
    this.scene.debugfocusscale = 0.2;
    
    this.twirlcallback = function( t ) { self.updateStuff( t ); }

    this.plotdt = params.plotdt;
    this.showmus = params.showmus;
    this.showomega1 = params.showomega1;
    this.colordex = 0;
    this.g = 9.8;
    
    this.omega0 = 0.5;
    this.omega1 = 0.5;
    this.deltat = 1;
    this.mu_s = params.mu_s;
    this.mu_k = params.mu_k;
    this.r0 = 0.9;
    this.gluepeg = false;
    this.recorddata = false;
    
    this.pegbasey = this.turntable.topy - this.peg.y0;
    this.peg.position = { x: this.r0, y: this.pegbasey, z: 0 };
    this.peg.velocity = { x: 0, y: 0, z: -this.omega0 * this.r0 };
    
    this.omega = this.omega0;
    this.t0 = 0;
    this.lastt = this.t0;
    this.lastomega = this.omega;
    this.firstcallofcurrentmotion = false;
    this.issliding = false;
}

// **********************************************************************

Twirler.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( Twirler.prototype, "constructor",
                       { value: Twirler,
                         enumerable: false,
                         writeable: true } );

// **********************************************************************

Twirler.colors = [ "#cc0000",
                   "#007000",
                   "#0000cc",
                   "#cc9900",
                   "#33cc00",
                   "#cc00cc" ];

// **********************************************************************

Twirler.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );

    var self = this;
    
    var controls = {
        type: "box",
        id: "controlsdiv",
        _class: "controlsdiv",
        spec: [
            { type: "box",
              id: "omega0box",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "omega0label",
                    text: "ω0: ",
                  },
                  { type: "input",
                    id: "omega0",
                    width: "6ex",
                    initval: this.omega0,
                  },
                  { type: "span",
                    id: "omega0units",
                    text: " s¯¹",
                  },
              ]
            },
            { type: "box",
              id: "omega1box",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "omega1label",
                    text: "ω1: "
                  },
                  { type: "input",
                    id: "omega1",
                    width: "6ex",
                    initval: this.omega1,
                  },
                  { type: "span",
                    id: "omega1units",
                    text: " s¯¹",
                  },
              ]
            },
            { type: "box",
              id: "dtbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "dtlabel",
                    text: "Δt: "
                  },
                  { type: "input",
                    id: "deltat",
                    width: "6ex",
                    initval: this.deltat,
                  },
                  { type: "span",
                    id: "dtunits",
                    text: " s¯¹",
                  },
              ]
            },
            { type: "box",
              id: "r0box",
              _class: "hbox margintop",
              spec: [
                  { type: "span",
                    id: "r0label",
                    text: "r0: "
                  },
                  { type: "input",
                    id: "r0",
                    width: "6ex",
                    initval: this.r0,
                  },
                  { type: "span",
                    id: "runits",
                    text: " m",
                  },
              ]
            },
            { type: "box",
              id: "gluebox",
              _class: "hbox",
              spec: [
                  { type: "checkbox",
                    id: "gluedcheckbox",
                    checked: this.gluepeg,
                  },
                  { type: "label",
                    id: "gluedcheckboxlabel",
                    _for: "gluedcheckbox",
                    text: "Glue Peg?"
                  },
              ]
            },
            { type: "box",
              id: "musbox",
              _class: "hbox margintop",
              spec: [
                  { type: "span",
                    id: "mustext",
                    text: "μ_s: "
                  },
                  { type: "input",
                    id: "mu_s",
                    width: "6ex",
                    initval: this.mu_s,
                    changecallback: function() { self.verifyFrictionCoefficients( "mu_s" ); }
                  }
              ]
            },
            { type: "box",
              id: "mukbox",
              _class: "hbox",
              spec: [
                  { type: "span",
                    id: "muktext",
                    text: "μ_k: ",
                  },
                  { type: "input",
                    id: "mu_k",
                    width: "6ex",
                    initval: this.mu_k,
                    changecallback: function() { self.verifyFrictionCoefficients( "mu_k" ); }
                  }
              ]
            },
            { type: "box",
              id: "recordbox",
              _class: "hbox margintop",
              spec: [
                  { type: "checkbox",
                    id: "recordcheckbox",
                    checked: this.recorddata,
                  },
                  { type: "label",
                    id: "recordcheckboxlabel",
                    _for: "recordcheckbox",
                    text: "Record Data?"
                  },
              ]
            },
            { type: "button",
              id: "startbutton",
              _class: "margintop",
              text: "Start",
              callback: function() { self.startMotion(); }
            },
            { type: "button",
              id: "stopbutton",
              text: "Stop",
              callback: function() { self.stopMotion(); }
            },
            { type: "button",
              id: "resetbutton",
              text: "Reset",
              callback: function() { self.reset(); }
            }
        ]
    };

    this.controls = InterfaceMaker( controls );

    if ( ! this.showmus ) {
        this.controls[ "musbox" ].style[ "display" ] = "none";
        this.controls[ "mukbox" ].style[ "display" ] = "none";
    }
    if ( ! this.showomega1 ) {
        this.controls[ "omega1box" ].style[ "display" ] = "none";
        this.controls[ "dtbox" ].style[ "display" ] = "none";
    }
    
    this.maindiv.appendChild( this.controls[ "controlsdiv" ] );

    this.disablewhenstopped = [ this.controls[ "stopbutton" ] ];
    this.disablewhenmoving = [ this.controls[ "omega0" ],
                               this.controls[ "omega1" ],
                               this.controls[ "deltat" ],
                               this.controls[ "r0" ],
                               this.controls[ "gluedcheckbox" ],
                               this.controls[ "mu_s" ],
                               this.controls[ "mu_k" ],
                               this.controls[ "recordcheckbox" ],
                               this.controls[ "startbutton" ],
                               this.controls[ "resetbutton" ]
                             ];

    for ( let tostop of this.disablewhenstopped ) {
        tostop.disabled = true;
    }


    // Plots

    var plotwid = 800;
    var plothei = 600;
    
    var hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    document.body.appendChild( hbox );

    this.rplot = new SVGPlot.Plot( { divid: "rplotdiv",
                                     svgid: "rplotsvg",
                                     ytitle: "r (m)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   } );
    this.vplot = new SVGPlot.Plot( { divid: "vplotdiv",
                                     svgid: "vplotsvg",
                                     ytitle: "v (m/s)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   } );

    hbox.appendChild( this.rplot.topdiv );
    hbox.appendChild( this.vplot.topdiv );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    document.body.appendChild( hbox );

    this.aplot = new SVGPlot.Plot( { divid: "aplotdiv",
                                     svgid: "aplotsvg",
                                     ytitle: "a (m/s²)",
                                     xtitle: "t (s)",
                                     width: plotwid,
                                     height: plothei
                                   } );
    this.xyplot = new SVGPlot.Plot( { divid: "xyplotdiv",
                                      svgid: "xyplotsvg",
                                      ytitle: "-z (m)",
                                      xtitle: "x (m)",
                                      width: plotwid,
                                      height: plothei,
                                      equalaspect: true
                                    } );

    hbox.appendChild( this.aplot.topdiv );
    hbox.appendChild( this.xyplot.topdiv );
    this.rplot.redraw();
    this.vplot.redraw();
    this.aplot.redraw();
    this.xyplot.redraw();
}

// **********************************************************************

Twirler.prototype.verifyFrictionCoefficients = function( whichtokeep )
{
    var mu_k_str = this.controls[ "mu_k" ].value;
    var mu_s_str = this.controls[ "mu_s" ].value;
    var mu_k = parseFloat( mu_k_str );
    var mu_s = parseFloat( mu_s_str );

    if ( isNaN( mu_k ) || isNaN( mu_s ) ) {
        window.alert( "Error parsing either μ_k=\"" + mu_k_str + "\" or μ_s=\"" + mu_s_str + "\" as a number." );
        return;
    }

    if ( mu_k > mu_s ) {
        if ( whichtokeep == "mu_k" )
            this.controls[ "mu_s" ].value = mu_k;
        else
            this.controls[ "mu_k" ].value = mu_s;
    }
}

// **********************************************************************

Twirler.prototype.updateStuff = function( t )
{
    if ( this.firstcallofcurrentmotion ) {
        this.t0 = t;
        this.t = t
        this.lastt = t;
        this.omega = this.omega0;
        this.lastomega = this.omega0;
        this.firstcallofcurrentmotion = false;
        this.peg.t = t;
        this.peg.curStateToOld();
        this.nextrecord = this.t0;
    }
    else {
        this.t = t;
        if ( this.t - this.t0 < this.deltat )
            this.omega = this.omega0 + ( this.omega1 - this.omega0 ) * ( this.t - this.t0 ) / this.deltat;
        else
            this.omega = this.omega1;
        
        this.turntable.visobj.rotateY( this.omega * ( this.t - this.lastt ) );

        this.peg.updateState( t );

        this.lastt = t;
        this.lastomega = this.omega;
    }

    if ( this.recorddata && t >= this.nextrecord ) {
        var r = Math.sqrt( this.peg.x**2 + this.peg.z**2 );
        var v = Math.sqrt( this.peg.vx**2 + this.peg.vy**2 + this.peg.vz**2 );
        var a = Math.sqrt( this.peg.ax**2 + this.peg.ay**2 + this.peg.az**2 );
        this.rdataset.addPoint( t - this.t0, r );
        this.vdataset.addPoint( t - this.t0, v );
        this.adataset.addPoint( t - this.t0, a );
        this.xydataset.addPoint( this.peg.x, -this.peg.z );
        this.nextrecord += this.plotdt;
    }
}

// **********************************************************************

Twirler.prototype.pegAccel = function( t, pos, vel, obj, dt )
{
    var tinyvrelsq = 1e-10;
    
    if ( pos.y <= this.turntable.boty - this.peg.y0 ) {
        this.stopMotion();
        return { x: -vel.x/dt, y: -vel.y/dt, z: -vel.z/dt }
    }

    var acc = { x: 0, y: 0, z: 0 };

    var r = Math.sqrt( pos.x**2 + pos.z**2 );
    var phi = Math.atan2( -pos.z, pos.x );

    if ( r > this.turntablerad + this.turntable.pegrad ) {
        acc.y = -this.g;
        return acc;
    }

    var omega = this.omega;
    var alpha = 0;
    if ( this.t - this.lastt > 1e-5 ) {
        alpha = ( this.omega - this.lastomega ) / ( this.t - this.lastt );
        omega = this.lastomega + ( this.omega - this.lastomega ) * ( t - this.lastt ) / ( this.t - this.lastt );
    }

    if ( ! this.gluepeg ) {
        var vspotx = -Math.sin( phi ) * r * omega;
        var vspoty = 0;
        var vspotz = -Math.cos( phi ) * r * omega;
        var vrelx = vel.x - vspotx;
        var vrely = 0;
        var vrelz = vel.z - vspotz;
        var vrelsq = vrelx**2 + vrely**2 + vrelz**2;
        if ( this.issliding && vrelsq < tinyvrelsq ) {
            this.issliding = false;
            console.log( "Object comes to a (relative) stop!" );
        }
    }
    
    if ( ( ! this.issliding ) || this.gluepeg ) {
        var centa = omega**2 * r;
        var tanga = alpha * r;
        acc.x = -centa * Math.cos( phi ) - tanga * Math.sin( phi );
        acc.z = centa * Math.sin( phi ) - tanga * Math.cos( phi );
        var amag = Math.sqrt( tanga**2 + centa**2 );
        if ( (! this.gluepeg) && amag > this.mu_s * this.g ) {
            this.issliding = true
            console.log( "Static friction breaks!" );
        }
    }

    if ( this.issliding && ( ! this.gluepeg ) ) {
        if ( vrelsq < tinyvrelsq ) {
            acc.x = 0;
            acc.z = 0;
        }
        else {
            var vrel = Math.sqrt( vrelsq );
            var fricxhat = -vrelx / vrel;
            var friczhat = -vrelz / vrel;
            acc.x = fricxhat * this.mu_k * this.g;
            acc.z = friczhat * this.mu_k * this.g
        }
    }


    return acc;
}

// **********************************************************************

Twirler.prototype.reset = function()
{
    this.stopMotion();
    
    var omega0 = parseFloat( this.controls[ "omega0" ] .value );
    if ( isNaN(omega0) ) {
        window.alert( "Error parsing ω0=\"" + this.controls[ "omega0" ].value + "\" as a number" );
        return;
    }
    var omega1 = omega0;
    var deltat = 1.;
    if ( this.showomega1 ) {
        var omega1 = parseFloat( this.controls[ "omega1" ] .value );
        if ( isNaN(omega1) ) {
            window.alert( "Error parsing ω1=\"" + this.controls[ "omega1" ].value + "\" as a number" );
            return;
        }
        var deltat = parseFloat( this.controls[ "deltat" ] .value );
        if ( isNaN(deltat) ) {
            window.alert( "Error parsing Δt=\"" + this.controls[ "deltat" ].value + "\" as a number" );
            return;
        }
        if ( deltat <= 0 ) {
            window.alert( "Error: Δt must be greater than 0." );
            return;
        }
    }
    var x = parseFloat( this.controls[ "r0" ].value );
    if ( isNaN(x) ) {
        window.alert( "Error parsing r0=\"" + this.controls[ "r0" ].value + "\" as a number" );
        return;
    }
    var mu_s = parseFloat( this.controls[ "mu_s" ].value );
    if ( isNaN(mu_s) ) {
        window.alert( "Error parsing μ_s=\"" + this.controls[ "mu_s" ].value + "\" as a number" );
        return;
    }
    var mu_k = parseFloat( this.controls[ "mu_k" ].value );
    if ( isNaN(mu_k) ) {
        window.alert( "Error parsing μ_k=\"" + this.controls[ "mu_k" ].value + "\" as a number" );
        return;
    }

    this.gluepeg = this.controls[ "gluedcheckbox" ].checked;
    this.recorddata = this.controls[ "recordcheckbox" ].checked;
    
    this.omega0 = omega0;
    this.omega1 = omega1;
    this.deltat = deltat;
    this.mu_s = mu_s;
    this.mu_k = mu_k
    this.peg.position = { x: x, y: this.pegbasey, z: 0 };
    this.peg.velocity = { x: 0, y: 0, z: -this.omega0 * x };
    this.peg.acceleration = { x: -(this.omega0**2) * x , y: 0, z: -x * (this.omega1-this.omega0)/this.deltat };
    this.issliding = false;
    
    this.turntable.visobj.setRotationFromAxisAngle( new THREE.Vector3( 0, 1, 0 ), 0. );
}

// **********************************************************************

Twirler.prototype.startMotion = function()
{
    this.reset();
    this.scene.addAnimationCallback( this.twirlcallback );
    this.firstcallofcurrentmotion = true;
    for ( let tostop of this.disablewhenmoving ) tostop.disabled = true;
    for ( let tostart of this.disablewhenstopped ) tostart.disabled = false;
    if ( this.recorddata ) {
        var color = Twirler.colors[ this.colordex ];
        this.colordex += 1;
        if ( this.colordex >= Twirler.colors.length ) this.colordex = 0;
        this.rdataset = new SVGPlot.Dataset( { color: color } );
        this.rplot.addDataset( this.rdataset );
        this.vdataset = new SVGPlot.Dataset( { color: color } );
        this.vplot.addDataset( this.vdataset );
        this.adataset = new SVGPlot.Dataset( { color: color } );
        this.aplot.addDataset( this.adataset );
        this.xydataset = new SVGPlot.Dataset( { color: color } );
        this.xyplot.addDataset( this.xydataset );
    }
}

// **********************************************************************

Twirler.prototype.stopMotion = function()
{
    this.scene.removeAnimationCallback( this.twirlcallback );
    for ( let tostop of this.disablewhenstopped ) tostop.disabled = true;
    for ( let tostart of this.disablewhenmoving ) tostart.disabled = false;
    if ( this.recorddata ) {
        this.rplot.redraw();
        this.vplot.redraw();
        this.aplot.redraw();
        this.xyplot.redraw();
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Wooden turntable.  y of top/bottom is ±0.1, radius is 1.
//  Peg top/bottom is ±0.3, radius is 0.04

Twirler.Turntable = function( inparams )
{
    var self = this;

    var params = {
        rx: 1,
        ry: 0.3,
        rz: 1,
        mass: 1,
        name: "Turntable",
        loadfunction: null
    };
    Object.assign( params, inparams );
    PhysicsObjects.Object.call( this, params );

    this.topy = 0.1;
    this.boty = -0.1;
    this.rad = 1.;
    this.pegtop = 0.3;
    this.pegbot = -0.3;
    this.pegrad = 0.04;

    this.ready = false;
    this.error = false;
    this.visobj = null;

    var loader = new GLTFLoader();
    loader.load( "./assets/turntable.glb",
                 function( gltf ) { self.loaded( gltf, params.loadfunction ) },
                 function( xhdr ) { console.log( "Tunrtable loaded fraction:" + xhdr.loaded / xhdr.total ); },
                 function( err ) {
                     self.ready = false;
                     self.error = true;
                     console.log( "Error loading turntable: " + err );
                 } );
}

Twirler.Turntable.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( Twirler.Turntable.prototype, 'constructor',
                       {  value: Twirler.Turntable,
                          enumerate: false,
                          writeable: true } );
    
Twirler.Turntable.prototype.loaded = function( gltf, loadfunc )
{
    this.ready = true;
    this.visobj = gltf.scene;
    this.visobj.name = this.name;
    // Hack to force visobj to the right position
    this.position = this._position;
    if ( loadfunc != null )
        loadfunc( this );
}

// **********************************************************************

export { Twirler };
