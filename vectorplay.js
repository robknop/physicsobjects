import * as THREE from "./lib/three.module.js";
import { DivBox } from "./divbox.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { PhysicsVector } from "./physicsvector.js";
import { StandardScene } from "./standardscene.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var VectorPlay = {};

VectorPlay.colors = [ "rgba(196, 196,   0, 1.0)",
                      "rgba(128,    0,128, 1.0)",
                      "rgba(  0,  96,   0, 1.0)",
                      "rgba( 96,  64,   0, 1.0)",
                      "rgba(196,   0,  96, 1.0)",
                      "rgba(  0, 196, 196, 1.0)",
                      "rgba(255,  64,   0, 1.0)",
                      "rgba(128, 128, 128, 1.0)",                     
                    ];

// VectorPlay.textcolors = [ "#808000", "#330033", "#004000", "#402000",
//                           "#cc0088", "#00cccc", "#802000", "#808080" ];

VectorPlay.Simulation = function( passparams={} )
{
    var params = { "printfpsevery": -1,
                   "showaxes": true,
                   "axeslen": 1,
                   "campos": new THREE.Vector3( 0., 0., 8. ),
                   "camfov": 30
                 };
    for ( var param in passparams )
        params[ param ] = passparams[ param ];

    StandardScene.call( this, params );
    
    this.vectors = [];
    this.selectedmainvector = null;
    this.selectedothervector = null;
    this.colordex = 0;
    this.ordinal = 0;
}

// **********************************************************************

VectorPlay.Simulation.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( VectorPlay.Simulation.prototype, "constructor",
                       { value: VectorPlay.Simulation,
                         enumerable: false,
                         writeable: true } );


// **********************************************************************
// Layout the document elements, including control buttons

VectorPlay.Simulation.prototype.layout = function()
{
    StandardScene.prototype.layout.call( this );
    
    var self = this;
    var div, hbox, vbox, subhbox;
    
    var body = document.body;

    // ****************************************
    // Buttons to the right

    this.buttondiv = new DivBox( 0.24 );
    this.buttondiv.elem.setAttribute( "class", "buttondiv" );
    this.maindiv.appendChild( this.buttondiv.elem );

    var button = document.createElement( "button" );
    button.setAttribute( "class", "marginblock" );
    button.appendChild( document.createTextNode( "Reset View" ) );
    button.addEventListener( "click", function( ) { self.views[0].camerahome( ) } );
    this.buttondiv.elem.appendChild( button );

    var p = document.createElement( "p" );
    this.buttondiv.elem.appendChild( p );
    this.axescheckbox = document.createElement( "input" );
    this.axescheckbox.setAttribute( "type", "checkbox" );
    if ( this.axes ) this.axescheckbox.setAttribute( "checked", true );
    p.appendChild( this.axescheckbox )
    p.appendChild( document.createTextNode( " show x/y/z axes" ) );
    this.axescheckbox.addEventListener( "change", function() { self.axes = self.axescheckbox.checked; } );
    
    this.buttondiv.elem.appendChild( document.createElement( "hr" ) );

    span = document.createElement( "span" );
    span.innerHTML = "Operate on vector:";
    this.buttondiv.elem.appendChild( span );
    this.mainvectorlistdiv = document.createElement( "div" );
    this.mainvectorlistdiv.setAttribute( "class", "vectorlist" );
    this.buttondiv.elem.appendChild( this.mainvectorlistdiv );
    var radio = document.createElement( "input" );
    radio.setAttribute( "type", "radio" );
    radio.setAttribute( "name", "Main Vector Radios" );
    radio.setAttribute( "id", "No Main Vector Selected" );
    radio.setAttribute( "value", "(Select None)" );
    radio.setAttribute( "checked", true );
    radio.addEventListener( "change", function() { self.selectMainVector(); } );
    var label = document.createElement( "label" );
    label.setAttribute( "for", "No Main Vector Selected" );
    label.appendChild( document.createTextNode( "(Select None)" ) );
    this.mainvectorlistdiv.appendChild( radio );
    this.mainvectorlistdiv.appendChild( label );
    this.mainselectnone = radio;
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Delete Selected Vector" ) );
    button.setAttribute( "class", "marginblock" );
    button.addEventListener( "click", function() { self.deleteSelectedVector() } );
    this.buttondiv.elem.appendChild( button );

    
    this.buttondiv.elem.appendChild( document.createElement( "hr" ) );
    span = document.createElement( "span" );
    span.innerHTML = "Other vector:";
    this.buttondiv.elem.appendChild( span );
    this.othervectorlistdiv = document.createElement( "div" );
    this.othervectorlistdiv.setAttribute( "class", "vectorlist" );
    this.buttondiv.elem.appendChild( this.othervectorlistdiv );
    var radio = document.createElement( "input" );
    radio.setAttribute( "type", "radio" );
    radio.setAttribute( "name", "Other Vector Radios" );
    radio.setAttribute( "id", "No Other Vector Selected" );
    radio.setAttribute( "value", "(Select None)" );
    radio.setAttribute( "checked", true );
    radio.addEventListener( "change", function() { self.selectOtherVector(); } );
    var label = document.createElement( "label" );
    label.setAttribute( "for", "No Other Vector Selected" );
    label.appendChild( document.createTextNode( "(Select None)" ) );
    this.othervectorlistdiv.appendChild( radio );
    this.othervectorlistdiv.appendChild( label );
    this.otherselectnone = radio;

    this.buttondiv.elem.appendChild( document.createElement( "hr" ) );

    p = document.createElement( "p" );
    this.buttondiv.elem.appendChild( p );
    var span = document.createElement( "span" );
    span.innerHTML = "Position:";
    span.setAttribute( "class", "bold" );
    p.appendChild( span );
    p.appendChild( document.createElement( "br" ) );
    this.newx = document.createElement( "input" );
    this.newx.setAttribute( "size", "4" );
    this.newx.setAttribute( "value", "0." );
    p.appendChild( this.newx );
    this.newy = document.createElement( "input" );
    this.newy.setAttribute( "size", "4" );
    this.newy.setAttribute( "value", "0." );
    p.appendChild( this.newy );
    this.newz = document.createElement( "input" );
    this.newz.setAttribute( "size", "4" );
    this.newz.setAttribute( "value", "0." );
    p.appendChild( this.newz );
    p.appendChild( document.createElement( "br" ) );
    span = document.createElement( "span" );
    span.innerHTML = "Components:"
    span.setAttribute( "class", "bold" );
    p.appendChild( span );
    p.appendChild( document.createElement( "br" ) );
    this.newxcomp = document.createElement( "input" );
    this.newxcomp.setAttribute( "size", "4" );
    this.newxcomp.setAttribute( "value", "1.0" );
    p.appendChild( this.newxcomp );
    this.newycomp = document.createElement( "input" );
    this.newycomp.setAttribute( "size", "4" );
    this.newycomp.setAttribute( "value", "1.0" );
    p.appendChild( this.newycomp );
    this.newzcomp = document.createElement( "input" );
    this.newzcomp.setAttribute( "size", "4" );
    this.newzcomp.setAttribute( "value", "0.0" );
    p.appendChild( this.newzcomp );
    
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Add New Vector" ) );
    button.setAttribute( "class", "marginblock" );
    button.addEventListener( "click", function() { self.addNewVector(); } );
    this.buttondiv.elem.appendChild( button );

    this.selvectorstatusdiv = document.createElement( "div" );
    this.selvectorstatusdiv.setAttribute( "class", "bordered" );
    this.selvectorstatusdiv.appendChild( document.createTextNode( "(No vector selected.)" ) );
    this.leftbigbox.appendChild( this.selvectorstatusdiv );
    
    this.bottombuttonbox = document.createElement( "div" );
    this.bottombuttonbox.setAttribute( "class", "bottombuttonbox" );
    this.leftbigbox.appendChild( this.bottombuttonbox );

    hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    this.bottombuttonbox.appendChild( hbox );
    
    div = document.createElement( "div" );
    hbox.appendChild( div );
    div.setAttribute( "class", "vbox bordered" );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Move to tail" ) );
    button.addEventListener( "click", function() { self.moveToTail(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Move to tip" ) );
    button.addEventListener( "click", function() { self.moveToTip(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Move tip to tip" ) );
    button.addEventListener( "click", function() { self.moveTipToTip(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Sum Vectors" ) );
    button.addEventListener( "click", function() { self.sumVectors(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Subtract Vectors" ) );
    button.addEventListener( "click", function() { self.subtractVectors(); } );
    div.appendChild( button );

    vbox = document.createElement( "div" )
    vbox.setAttribute( "class", "vbox" );
    hbox.appendChild( vbox );

    subhbox = document.createElement( "div" );
    subhbox.setAttribute( "class", "hbox" );
    vbox.appendChild( subhbox );
    
    div = document.createElement( "div" );
    subhbox.appendChild( div );
    div.setAttribute( "class", "subbuttons" );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Move vector to:" ) );
    button.addEventListener( "click", function() { self.moveVector(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    this.movex = document.createElement( "input" );
    this.movex.setAttribute( "size", "4" );
    this.movex.setAttribute( "value", "" );
    div.appendChild( this.movex );
    this.movey = document.createElement( "input" );
    this.movey.setAttribute( "size", "4" );
    this.movey.setAttribute( "value", "" );
    div.appendChild( this.movey );
    this.movez = document.createElement( "input" );
    this.movez.setAttribute( "size", "4" );
    this.movez.setAttribute( "value", "" );
    div.appendChild( this.movez );

    div = document.createElement( "div" );
    subhbox.appendChild( div );
    div.setAttribute( "class", "subbuttons" );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Set Components:" ) );
    button.addEventListener( "click", function() { self.editVector(); } );
    div.appendChild( button );
    div.appendChild( document.createElement( "br" ) );
    this.compinputx = document.createElement( "input" );
    this.compinputx.setAttribute( "size", "4" );
    this.compinputx.setAttribute( "value", "" );
    div.appendChild( this.compinputx );
    this.compinputy = document.createElement( "input" );
    this.compinputy.setAttribute( "size", "4" );
    this.compinputy.setAttribute( "value", "" );
    div.appendChild( this.compinputy );
    this.compinputz = document.createElement( "input" );
    this.compinputz.setAttribute( "size", "4" );
    this.compinputz.setAttribute( "value", "" );
    div.appendChild( this.compinputz );

    subhbox = document.createElement( "div" );
    subhbox.setAttribute( "class", "hbox" );
    vbox.appendChild( subhbox );

    div = document.createElement( "div" );
    subhbox.appendChild( div );
    div.setAttribute( "class", "subbuttons" );
    button = document.createElement( "button" );
    button.appendChild( document.createTextNode( "Multiply by scalar:" ) );
    div.appendChild( button );
    button.addEventListener( "click", function() { self.multiplyByScalar(); } );
    this.scalartomultiply = document.createElement( "input" );
    this.scalartomultiply.setAttribute( "size", "5" );
    this.scalartomultiply.setAttribute( "value", "1.0" );
    div.appendChild( this.scalartomultiply );


}

// **********************************************************************

VectorPlay.Simulation.prototype.selectMainVector = function()
{
    if ( this.selectedmainvector != null ) {
        this.selectedmainvector.removeHighlight();
        this.selectedmainvector == null;
    }
    this.selectedmainvector = this.findSelectedVector();
    if ( this.selectedmainvector != null ) {
        if ( this.selectedmainvector == this.selectedothervector ) {
            this.otherselectnone.checked = true;
            this.selectedothervector = null;
        }
        else {
            this.selectedmainvector.addHighlight();
        }
    }
    else
        this.mainselectnone.checked = true;
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.selectOtherVector = function()
{
    if ( this.selectedothervector != null ) {
        this.selectedothervector.removeHighlight();
        this.selectedothervector == null;
    }
    this.selectedothervector = this.findSelectedVector( true );
    if ( this.selectedothervector == this.selectedmainvector ) this.selectedothervector = null;
    if ( this.selectedothervector != null )
        this.selectedothervector.addHighlight();
    else
        this.otherselectnone.checked = true
}

// **********************************************************************

VectorPlay.Simulation.prototype.renderVectorInfo = function()
{
    while ( this.selvectorstatusdiv.firstChild)
        this.selvectorstatusdiv.removeChild( this.selvectorstatusdiv.firstChild);
    var vec = this.findSelectedVector();
    if ( vec == null ) {
        this.selvectorstatusdiv.appendChild( document.createTextNode( "(No vector selected.)" ) );
        this.movex.value = "";
        this.movey.value = "";
        this.movez.value = "";
        this.compinputx.value = "";
        this.compinputy.value = "";
        this.compinputz.value = "";
        return
    }

    var span = document.createElement( "span" );
    span.setAttribute( "class", "bold" );
    span.appendChild( document.createTextNode( vec.name + " : " ) );
    this.selvectorstatusdiv.appendChild( span );
    this.selvectorstatusdiv.appendChild( document.createTextNode( "\u00A0" ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( vec.xcomp.toFixed( 2 ) + ", " +
                                                                  vec.ycomp.toFixed( 2 ) + ", " +
                                                                  vec.zcomp.toFixed( 2 ) ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( "\u00A0\u00A0Mag: " ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( vec.magnitude.toFixed( 2 ) ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( "\u00A0\u00A0Unit: " ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( vec.hatx.toFixed( 3 ) + ", " +
                                                                  vec.haty.toFixed( 3 ) + ", " +
                                                                  vec.hatz.toFixed( 3 ) ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( "\u00A0\u00A0Pos: " ) );
    this.selvectorstatusdiv.appendChild( document.createTextNode( vec.x.toFixed( 2 ) + ", " +
                                                                  vec.y.toFixed( 2 ) + ", " +
                                                                  vec.z.toFixed( 2 ) ) );

    this.movex.value = vec.x.toFixed( 3 );
    this.movey.value = vec.y.toFixed( 3 );
    this.movez.value = vec.z.toFixed( 3 );
    this.compinputx.value = vec.xcomp.toFixed( 3 );
    this.compinputy.value = vec.ycomp.toFixed( 3 );
    this.compinputz.value = vec.zcomp.toFixed( 3 );
}

// **********************************************************************

VectorPlay.Simulation.prototype.addNewVector = function()
{
    var self = this;
    
    var x = parseFloat( this.newx.value );
    var y = parseFloat( this.newy.value );
    var z = parseFloat( this.newz.value );
    var xcomp = parseFloat( this.newxcomp.value );
    var ycomp = parseFloat( this.newycomp.value );
    var zcomp = parseFloat( this.newzcomp.value );

    this.addVector( x, y, z, xcomp, ycomp, zcomp );
}

// **********************************************************************

VectorPlay.Simulation.prototype.addVector = function( x, y, z, xcomp, ycomp, zcomp, name=null )
{
    var self = this;
    var color = VectorPlay.colors[ this.colordex ];
    // var textcolor = VectorPlay.textcolors[ this.colordex ];
    this.colordex += 1;
    if ( this.colordex >= VectorPlay.colors.length ) this.colordex = 0;
    this.ordinal += 1;
    if ( name == null ) {
        name = "#" + this.ordinal;
    }
    var vec = new PhysicsVector( { "x": x, "y": y, "z": z,
                                   "xcomp": xcomp, "ycomp": ycomp, "zcomp": zcomp,
                                   "color": color, "name": name } );
    this.scene.addObject( vec );
    
    this.vectors.push( { "ordinal": this.ordinal,
                         "vector": vec
                       } );

    var div = document.createElement( "div" );
    this.mainvectorlistdiv.appendChild( div );
    div.setAttribute( "id", "Main Vector Buttons " + this.ordinal );
    var radio = document.createElement( "input" );
    radio.setAttribute( "type", "radio" );
    radio.setAttribute( "name", "Main Vector Radios" );
    radio.setAttribute( "id", "Main Vector Radios " + this.ordinal );
    radio.setAttribute( "value", name );
    radio.addEventListener( "change", function() { self.selectMainVector(); } );
    var label = document.createElement( "label" );
    label.setAttribute( "for", "Main Vector Radios " + this.ordinal );
    label.setAttribute( "id", "Main Vector Radios Label " + this.ordinal );
    label.setAttribute( "class", "vectorlist" );
    label.style.color = color;
    label.appendChild( document.createTextNode( name ) );
    div.appendChild( radio );
    div.appendChild( label );
    
    div = document.createElement( "div" );
    this.othervectorlistdiv.appendChild( div );
    div.setAttribute( "id", "Other Vector Buttons " + this.ordinal );
    radio = document.createElement( "input" );
    radio.setAttribute( "type", "radio" );
    radio.setAttribute( "name", "Other Vector Radios" );
    radio.setAttribute( "id", "Other Vector Radios " + this.ordinal );
    radio.setAttribute( "value", name );
    radio.addEventListener( "change", function() { self.selectOtherVector(); } );
    label = document.createElement( "label" );
    label.setAttribute( "for", "Other Vector Radios " + this.ordinal );
    label.setAttribute( "id", "Other Vector Radios Label " + this.ordinal );
    label.style.color = color;
    label.appendChild( document.createTextNode( name ) );
    div.appendChild( radio );
    div.appendChild( label );
    
}

// **********************************************************************

VectorPlay.Simulation.prototype.deleteSelectedVector = function()
{
    var vec = this.findSelectedVector()
    if ( vec == null ) return;

    var ordinal = -1;
    var dex = -1;
    for ( var i = 0 ; i < this.vectors.length ; ++ i ) {
        if ( this.vectors[i].vector == vec ) {
            ordinal = this.vectors[i].ordinal;
            dex = i;
            break
        }
    }
    if ( ordinal < 0 ) {
        window.alert( "Error, coudn't find selected vector in interal list.  This should never happen." );
        return;
    }
    this.vectors.splice( dex, 1 );

    this.scene.removeObject( vec );
    var elem = document.getElementById( "Main Vector Radios " + ordinal );
    elem.remove();
    elem = document.getElementById( "Main Vector Radios Label " + ordinal );
    elem.remove();
    elem = document.getElementById( "Other Vector Radios " + ordinal );
    elem.remove();
    elem = document.getElementById( "Other Vector Radios Label " + ordinal );
    elem.remove();
    this.mainselectnone.checked = true;
    this.selectMainVector();
    this.otherselectnone.checked = true;
    this.selectOtherVector();
}


// **********************************************************************

VectorPlay.Simulation.prototype.moveVector = function()
{
    var vec = this.findSelectedVector();
    if ( vec == null ) return;

    var x = parseFloat( this.movex.value );
    var y = parseFloat( this.movey.value );
    var z = parseFloat( this.movez.value );
    vec.position = { "x": x, "y": y, "z": z };

    // Overdone to do this always, but since I'm not tracking which vector is selected, do it to be safe.
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.editVector = function()
{
    var vec = this.findSelectedVector();
    if ( vec == null ) return;

    vec.xcomp = parseFloat( this.compinputx.value );
    vec.ycomp = parseFloat( this.compinputy.value );
    vec.zcomp = parseFloat( this.compinputz.value );
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.moveToTail = function()
{
    var retval = this.findBothSelectedVectors();
    if ( retval == null ) return;
    var vec = retval[0];
    var other = retval[1];
    vec.position = other.position;
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.moveToTip = function()
{
    var retval = this.findBothSelectedVectors();
    if ( retval == null ) return;
    var vec = retval[0];
    var other = retval[1];
    vec.position = { "x": other.position.x + other.xcomp,
                     "y": other.position.y + other.ycomp,
                     "z": other.position.z + other.zcomp };
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.moveTipToTip = function()
{
    var retval = this.findBothSelectedVectors();
    if ( retval == null ) return;
    var vec = retval[0];
    var other = retval[1];
    vec.position = { "x": other.position.x + other.xcomp - vec.xcomp,
                     "y": other.position.y + other.ycomp - vec.ycomp,
                     "z": other.position.z + other.zcomp - vec.zcomp };
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.sumVectors = function()
{
    var retval = this.findBothSelectedVectors();
    if ( retval == null ) return;
    var vec = retval[0];
    var other = retval[1];

    this.addVector( vec.x, vec.y, vec.z,
                    vec.xcomp+other.xcomp, vec.ycomp+other.ycomp, vec.zcomp+other.zcomp,
                    vec.name + " + " + other.name );
}

// **********************************************************************

VectorPlay.Simulation.prototype.subtractVectors = function()
{
    var retval = this.findBothSelectedVectors();
    if ( retval == null ) return;
    var vec = retval[0];
    var other = retval[1];

    this.addVector( vec.x, vec.y, vec.z,
                    vec.xcomp-other.xcomp, vec.ycomp-other.ycomp, vec.zcomp-other.zcomp,
                    vec.name + " - " + other.name );
}

// **********************************************************************

VectorPlay.Simulation.prototype.multiplyByScalar = function()
{
    var vec = this.findSelectedVector();
    if ( vec == null ) return;

    var scalar = parseFloat( this.scalartomultiply.value );
    vec.xcomp *= scalar;
    vec.ycomp *= scalar;
    vec.zcomp *= scalar;
    this.renderVectorInfo();
}

// **********************************************************************

VectorPlay.Simulation.prototype.findBothSelectedVectors = function()
{
    var vec = this.findSelectedVector();
    if ( vec == null ) {
        window.alert( "No vector to operate on selected." );
        return null;
    }
    var other = this.findSelectedVector( true );
    if ( other == null ) {
        window.alert( "No \"other\" vector to use." );
        return null;
    }
    return [ vec, other ];
}

// **********************************************************************

VectorPlay.Simulation.prototype.findSelectedVector = function( other=false )
{
    var stringbase;
    if ( other )
        stringbase = "Other Vector Radios";
    else
        stringbase = "Main Vector Radios";
    var radios = document.getElementsByName( stringbase );
    var ordinal = -1;
    for ( var radio of radios ) {
        if ( radio.checked ) {
            if ( radio.id.substring( 0, stringbase.length ) != stringbase ) {
                // window.alert( "Error, " + radio.id.substring( 0, stringbase.length ) +
                //               "' is not '" + stringbase + "'");
                return null;
            }
            ordinal = parseInt( radio.id.substring( stringbase.length+1 ) );
            break;
        }
    }
    if ( ordinal < 0 ) {
        return null;
    }
    var vec = null;
    for ( var i = 0 ; i < this.vectors.length ; ++i ) {
        if ( this.vectors[i].ordinal == ordinal ) {
            vec = this.vectors[i].vector;
            break;
        }
    }
    if ( vec == null ) {
        window.alert( "Error: could not find vector with ordinal " + ordinal );
        return null;
    }
    return vec;
}

// **********************************************************************

export { VectorPlay };
