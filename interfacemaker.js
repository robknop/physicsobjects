import { ListWidget } from "./listwidget.js";
import { TableWidget } from "./tablewidget.js";
// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// **********************************************************************
// Pass an object with properties id and type and (optionally) _class  (CSS class)
// The following types take an array of things to be passed recursively back:
//   box — a div with elements as children
//   table — children must all be tr
//   tr — children must be tds
//   td — Usually you just want one child, but it can be a box
//
// Otherwise, type can be one of the following, with the other properties listed:
//    button — text, callback (optional)
//    input — size (optional), width (optional), initval (optional), readonly=false,
//         inputcallback (optional), changecallback (optional)
//    numinput — width (optional), initval, readonly=false,
//         changecallback (optional), min (optional), max ( optional ), step ( optional ), initval ( optional )
//    checkbox — checked=false, callback (optional)
//    label — _for, text
//    select — options (array of text values), selindex, changecallback
//    listwidget — multiselect=false, callback (optional)
//    tablewidget — columns=0, multiselect=false, headers (optional, callback (optional)
//    span — text
//
// Returns a flat object with identifier->DOMElement pairs, except in
//  the case of listwidget or tablewidget, in which case it's a
//  ListWidget/TableWidget object.  All will have been added as children
//  to each other appropriately.

var InterfaceMaker = function( spec, elemlist=[] )
{
    if ( !spec.hasOwnProperty( "id" ) ) {
        let err = "Interface error: all widgets must have an id; a \"" + spec.type + "\" didn't.";
        console.log( err );
        window.alert( err );
        return null;
    }
    if ( elemlist.hasOwnProperty( "id" ) ) {
        let err = "Interface error: id \"" + spec.id + "\" shows up more than once."
        console.log( err );
        window.alert( err );
        return null;
    }

    var domelem;
    var type = spec.type;
    if ( type == "box" ) type = "div";
    if ( type == "numinput" || type == "checkbox" ) type = "input";
    if ( type == "listwidget" ) {
        let params = {};
        if ( spec.hasOwnProperty( "multiselect" ) )
            params.multiselect = spec.multiselect;
        domelem = new ListWidget( params );
        if ( spec.hasOwnProperty( "callback" ) )
            domelem.addEventListener( "selectionchanged", spec.callback )
        domelem.div.setAttribute( "id", spec.id );
        if ( spec.hasOwnProperty( "_class" ) )
            domelem.div.setAttribute( "class", spec["_class"] );
    }
    else if ( type == "tablewidget" ) {
        let params = { columns: 0 };
        if ( spec.hasOwnProperty( "multiselect" ) )
            params.multiselect = spec.multiselect;
        if ( spec.hasOwnProperty( "columns" ) )
            params.columns = spec.columns;
        if ( spec.hasOwnProperty( "headers" ) )
            params.headers = spec.headers
        domelem = new TableWidget( params );
        if ( spec.hasOwnProperty( "callback" ) )
            domelem.addEventListener( "selectionchanged", spec.callback );
        domelem.table.setAttribute( "id", spec.id );
        if ( spec.hasOwnProperty( "_class" ) )
            domelem.table.setAttribute( "class", spec["_class"] );
    }        
    else {
        domelem = document.createElement( type );
        domelem.setAttribute( "id", spec.id );
        if ( spec.hasOwnProperty( "_class" ) )
            domelem.setAttribute( "class", spec["_class"] );
    }
    elemlist[ spec.id ] = domelem;
    
    if ( spec.type == "box" || spec.type == "table" || spec.type == "tr" || spec.type == "td" ) {
        if ( spec.hasOwnProperty( "spec" ) ) {
            for ( let i in spec.spec ) {
                elemlist = InterfaceMaker( spec.spec[i], elemlist );
                if ( elemlist == null ) return null;
                if ( spec.spec[i].type == "listwidget" )
                    domelem.appendChild( elemlist[ spec.spec[i].id ].div );
                else if ( spec.spec[i].type == "tablewidget" )
                    domelem.appendChild( elemlist[ spec.spec[i].id ].table );
                else
                    domelem.appendChild( elemlist[ spec.spec[i].id ] );
            }
        }
    }
    else if ( spec.type == "button" ) {
        if ( spec.hasOwnProperty( "text" ) )
            domelem.appendChild( document.createTextNode( spec.text ) );
        if ( spec.hasOwnProperty( "callback" ) )
            domelem.addEventListener( "click", spec.callback);
    }
    else if ( spec.type == "input" ) {
        if ( spec.hasOwnProperty( "size" ) )
            domelem.setAttribute( "size", spec.size );
        if ( spec.hasOwnProperty( "width" ) )
            domelem.style.width = spec.width;
        if ( spec.hasOwnProperty( "initval" ) )
            domelem.value = spec.initval;
        if ( spec.hasOwnProperty( "readonly" ) && spec.readonly )
            domelem.setAttribute( "readonly", 1 )
        if ( spec.hasOwnProperty( "inputcallback" ) )
            domelem.addEventListener( "input", spec.inputcallback )
        if ( spec.hasOwnProperty( "changecallback" ) )
            domelem.addEventListener( "change", spec.changecallback );
    }
    else if ( spec.type == "checkbox" ) {
        domelem.setAttribute( "type", "checkbox" );
        if ( spec.hasOwnProperty( "checked" ) && spec.checked )
            domelem.checked = true;
        else
            domelem.checked = false;
        if ( spec.hasOwnProperty( "callback" ) )
            domelem.addEventListener( "click", spec.callback )
    }
    else if ( spec.type == "label" ) {
        if ( spec.hasOwnProperty( "_for" ) )
             domelem.setAttribute( "for", spec._for );
        if ( spec.hasOwnProperty( "text" ) )
            domelem.appendChild( document.createTextNode( spec.text ) );
    }
    else if ( spec.type == "numinput" ) {
        domelem.setAttribute( "type", "number" );
        if ( spec.hasOwnProperty( "width" ) )
            domelem.style.width = spec.width;
        if ( spec.hasOwnProperty( "initval" ) )
            domelem.value = spec.initval;
        if ( spec.hasOwnProperty( "min" ) )
            domelem.setAttribute( "min", spec.min );
        if ( spec.hasOwnProperty( "max" ) )
            domelem.setAttribute( "max", spec.max );
        if ( spec.hasOwnProperty( "step" ) )
            domelem.setAttribute( "step", spec.step );
        if ( spec.hasOwnProperty( "readonly" ) && spec.readonly )
            domelem.setAttribute( "readonly", 1 );
        if ( spec.hasOwnProperty( "inputcallback" ) )
            domelem.addEventListener( "input", spec.inputcallback )
        if ( spec.hasOwnProperty( "changecallback" ) )
            domelem.addEventListener( "change", spec.changecallback );
    }
    else if ( spec.type == "select" ) {
        for ( let i in spec.options ) {
            let option = document.createElement( "option" );
            domelem.appendChild( option );
            option.text = spec.options[i];
        }
        if ( spec.hasOwnProperty( "selindex" ) )
            domelem.selectedIndex = spec.selindex;
        if ( spec.hasOwnProperty( "changecallback" ) )
            domelem.addEventListener( "change", spec.changecallback );
    }
    else if ( spec.type == "span" ) {
        if ( spec.hasOwnProperty( "text" ) )
            domelem.appendChild( document.createTextNode( spec.text ) )
    }
    else if ( spec.type != "listwidget" && spec.type != "tablewidget" ) {
        let err = "Interface error: unknown type \"" + spec.type + "\"";
        console.log( err );
        window.alert (err );
        return null;
    }

    return elemlist;
}

// **********************************************************************

export { InterfaceMaker };
