import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

// **********************************************************************
// **********************************************************************
// **********************************************************************
// A PhysicsVector is an arrow whose position is its base.  It's built from
// a cylinder and a cone

var PhysicsVector = function( passparams )
{
    var params = { x: 0, y: 0, z: 0,
                   xcomp: 0, ycomp: 10, zcomp: 0,
                   magnitude: 10,
                   hatx: 1, haty: 0, hatz: 0,
                   cylrad: 0.05, conelength: 0.15, conebaserad: 0.1,
                   color: 0xcccccc, emit: 0x000000, metalness: 0., roughness: 0.25, name: "Vector" };
    for ( const param in passparams ) 
        params[ param ] = passparams[ param ];

    PhysicsObjects.Object.call( this, { name: params.name, mass: 1., rx: 0, ry: 0, rz: 0 } );

    this._xcomp = params.xcomp;
    this._ycomp = params.ycomp
    this._zcomp = params.zcomp;
    // If any of xcomp, ycomp, or zcomp is passed, then ignore magnitude and hats
    if ( ! ( passparams.hasOwnProperty( "xcomp" ) || passparams.hasOwnProperty( "ycomp" ) ||
             passparams.hasOwnProperty( "zcomp" ) ) ) {
        // Otherwise, set position based on magnitudes and hats
        let hatmag = Math.sqrt( params.hatx**2 + params.haty**2 + params.hatz**2 );
        if ( hatmag > 1e-10 ) {
            params.hatx /= hatmag;
            params.haty /= hatmag;
            params.hatz /= hatmag;
            this._xcomp = params.magnitude * params.hatx;
            this._ycomp = params.magnitude * params.haty;
            this._zcomp = params.magnitude * params.hatz;
        }
    }

    this.cylrad = params.cylrad;
    this.conebaserad = params.conebaserad;
    this.conelength = params.conelength;
    this.color = params.color;
    
    var shaftgeom = new THREE.CylinderGeometry( this.cylrad, this.cylrad, 1., 16 );
    var conegeom = new THREE.ConeGeometry( this.conebaserad, 1., 16 );
    var mat = new THREE.MeshStandardMaterial( { color: this.color,
                                                emissive: params.emit,
                                                metalness: params.metalness,
                                                roughness: params.roughness
                                              } );

    // I am surprised this is necessary for ConeGeometry
    conegeom.mergeVertices();
    conegeom.computeVertexNormals();
    
    this.visobj = new THREE.Group();
    this.shaft = new THREE.Mesh( shaftgeom, mat );
    this.shaft.position.y = 1/2.;
    this.visobj.add( this.shaft )
    this.arrowhead = new THREE.Mesh( conegeom, mat );
    this.arrowhead.position.y = 1 + params.conelength/2.;
    this.visobj.add( this.arrowhead );

    this.visobj.name = params.name;
    // this.parent.addObject( this );

    this.x = params.x;
    this.y = params.y;
    this.z = params.z;

    this.highlight = null;
    
    this.updateVector();
}

PhysicsVector.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( PhysicsVector.prototype, 'constructor',
                       {
                           value: PhysicsVector,
                           enumerate: false,
                           writeable: true } );

// **********************************************************************

Object.defineProperty( PhysicsVector.prototype, "xcomp", {
    get() { return this._xcomp },
    set( val ) {
        this._xcomp = val;
        this.updateVector();
    }
} );
        
Object.defineProperty( PhysicsVector.prototype, "ycomp", {
    get() { return this._ycomp },
    set( val ) {
        this._ycomp = val;
        this.updateVector();
    }
} );
        
Object.defineProperty( PhysicsVector.prototype, "zcomp", {
    get() { return this._zcomp },
    set( val ) {
        this._zcomp = val;
        this.updateVector();
    }
} );

Object.defineProperty( PhysicsVector.prototype, "magnitude", {
    get() { return this._magnitude },
    set( val ) {
        this._xcomp = val * this._hatx;
        this._ycomp = val * this._haty;
        this._zcomp = val * this._hatz;
        this.updateVector();
    }
} );

Object.defineProperty( PhysicsVector.prototype, "hatx", {
    get() { return this._hatx },
    set( val ) {
        retval = PhysicsVector.normalizeHats( val, this._haty, this._hatz );
        this._xcomp = this._magnitude * retval[0];
        this._ycomp = this._magnitude * retval[1];
        this._zcomp = this._magnitude * retval[2];
        this.updateVector();
    }
} );

Object.defineProperty( PhysicsVector.prototype, "haty", {
    get() { return this._haty },
    set( val ) {
        retval = PhysicsVector.normalizeHats( val, this._hatx, this._hatz );
        this._xcomp = this._magnitude * retval[1];
        this._ycomp = this._magnitude * retval[0];
        this._zcomp = this._magnitude * retval[2];
        this.updateVector();
    }
} );

Object.defineProperty( PhysicsVector.prototype, "hatz", {
    get() { return this._hatz },
    set( val ) {
        retval = PhysicsVector.normalizeHats( val, this._hatx, this._haty );
        this._xcomp = this._magnitude * retval[1];
        this._ycomp = this._magnitude * retval[2];
        this._zcomp = this._magnitude * retval[0];
        this.updateVector();
    }
} );


// **********************************************************************

PhysicsVector.prototype.addHighlight = function()
{
    if ( this.highlight != null ) return;

    var cylfac = 2.;
    var conefac = 1.5;
    var opacity = 0.5
    var shaftgeom = new THREE.CylinderGeometry( cylfac*this.cylrad, cylfac*this.cylrad, 1., 16 );
    var conegeom = new THREE.ConeGeometry( conefac*this.conebaserad, 1., 16 );
    var mat = new THREE.MeshLambertMaterial( { color: this.color,
                                               opacity: opacity,
                                               transparent: true } );

    conegeom.mergeVertices();
    conegeom.computeVertexNormals();
    
    this.highlight = new THREE.Group();
    this.highlightshaft = new THREE.Mesh( shaftgeom, mat );
    this.highlightshaft.position.y = 1/2.;
    this.highlight.add( this.highlightshaft );
    this.highlightarrowhead = new THREE.Mesh( conegeom, mat );
    this.highlightarrowhead.position.y = 1 + this.conelength/2.;
    this.highlight.add( this.highlightarrowhead );
    this.visobj.add( this.highlight );
    this.updateVector();
}

// **********************************************************************

PhysicsVector.prototype.removeHighlight = function()
{
    if ( this.highlight == null ) return;
    this.visobj.remove( this.highlight );
    this.highlight = null;
}

// **********************************************************************

PhysicsVector.normalizeHats = function( newhat, oldhat1, oldhat2 )
{
    if ( newhat > 1. )
        return [1., 0., 0.];
    
    var other = Math.sqrt( 1 - newhat**2 );
    var oldother = math.sqrt( oldhat1**2 + oldhat2**2 );
    if ( oldother == 0. ) {
        oldhat1 = other / Math.sqrt(2.);
        oldhat2 = other / Math.sqrt(2.);
    } else {
        oldhat1 = other / oldother * oldhat1;
        oldhat2 = other / oldother * oldhat2;
    }
    return [ newhat, oldhat1, oldhat2 ];
}
 

// **********************************************************************
// Once _xcomp, _ycomp, and _zcomp are set right, update everything else
// from those three

PhysicsVector.prototype.updateVector = function()
{
    this._magnitude = Math.sqrt( this._xcomp ** 2 + this._ycomp ** 2 + this._zcomp ** 2 );
    if ( this._magnitude == 0 ) {
        this._hatx = 0.;
        this._haty = 1.;
        this._hatz = 0.;
    }
    else {
        this._hatx = this._xcomp / this._magnitude;
        this._haty = this._ycomp / this._magnitude;
        this._hatz = this._zcomp / this._magnitude;
    }

    if ( this._magnitude < this.conelength*2 ) {
        this.shaft.scale.set( 1., this._magnitude/2., 1. );
        this.arrowhead.scale.set( 1., this._magnitude/2., 1. );
        this.shaft.position.y = this._magnitude/4.;
        this.arrowhead.position.y = 3.*this._magnitude/4.;
    }
    else {
        this.shaft.scale.set( 1., this._magnitude - this.conelength, 1. );
        this.arrowhead.scale.set( 1., this.conelength, 1. );
        this.shaft.position.y = ( this._magnitude - this.conelength) / 2.;
        this.arrowhead.position.y = this._magnitude - this.conelength / 2.
    }
    if ( this.highlight != null ) {
        this.highlightshaft.scale.y = this.shaft.scale.y;
        this.highlightarrowhead.scale.y = this.arrowhead.scale.y;
        this.highlightshaft.position.y = this.shaft.position.y;
        this.highlightarrowhead.position.y = this.arrowhead.position.y;
    }
    this.setYAxisDirection( this._hatx, this._haty, this._hatz );
}

// **********************************************************************

export { PhysicsVector };
