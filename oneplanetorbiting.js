import * as THREE from "./lib/three.module.js";
import { PhysicsObjects } from "./physicsobjects.js";
import { InterfaceMaker } from "./interfacemaker.js";
import { PhysicsVector } from "./physicsvector.js";
import { StandardScene} from "./standardscene.js";
import { SVGPlot } from "./svgplot.js";

// PhysicsObjects ©2020 by Rob Knop

// This file is part of PhysicsObjects.

// PhysicsObjects is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// PhysicsObjects is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with PhysicsObjects.  If not, see <https://www.gnu.org/licenses/>.

var OnePlanetOrbits = function( inparams = {} )
{
    var self = this;
    
    var params = { "timedilation": 4,
                   "aspect": 0.75,
                   "gridbg": true,
                   "axeslen": 0.5,
                   "axesemit": 0.5,
                   "lights": [
                       { type: "point",
                         pos: [ 0, 0, 0 ],
                         color: 0xffffff,
                         intensity: 2.,
                       }
                   ],
                   "name": "One Planet Orbiting Sun",
                   "campos": new THREE.Vector3( 0., 0., 2. ),
                   "sunmass": 1,
                   "planetmass": 3.00349e-6,    // in solar masses
                   "G": 4*Math.PI**2,           // AU³ Msun¯¹ yr¯²
                   "sunrad": 0.1,
                   "suncolor": null,
                   "planetrad": 0.05,
                   "posscale": 2,
                   // "planetcolor": 0x8080ff,
                   // "planetemit": 0x8080ff,
                   "planetemitintensity": 0.1,
                   "planetemittexture": PhysicsObjects.baseurl + "assets/135704main_worldview_512.jpg",
                   "planetemit": 0xffffff,
                   "planettexture": PhysicsObjects.baseurl + "assets/135704main_worldview_512.jpg",
                   "planetcolor": 0xffffff,
                   "planetpos": [1, 0, 0],
                   "planetvel": [ 0, 2*Math.PI, 0 ],
                   "showvel": true,
                   "velscale": 0.1,
                   "velcolor": 0x0000f0,
                   "velemit": 0x000080,
                   "showacc": true,
                   "accscale": 0.015,
                   "acccolor": 0xf00000,
                   "accemit": 0x800000,
                   "trail": true,
                   "trailrad": 0.01,
                   "trailcolor": 0x4040ff,
                   "trailemit": 0x101040,
                   "traildt": 0.01,
                   "plotdt": 0.05,
                   "showEwidgets": true,
                   "showcircVwidget": true,
                   "showsaveparamsbutton": true
                 };
    Object.assign( params, inparams );

    this.showEwidgets = params.showEwidgets;
    this.showcircVwidget = params.showcircVwidget;
    this.showsaveparamsbutton = params.showsaveparamsbutton;
    this.deltaphi = 2*Math.PI;
    this.colordex = 0;
    this.plotdt = params.plotdt;
    this.nextplot = 0;
    
    StandardScene.call( this, params );
    
    this.scene.debugfocuscolor = 0x8060ff;
    this.scene.debugfocusemit = 0x201840;
    this.scene.debugfocusscale = 0.08;

    var spheregeom = new THREE.SphereGeometry( params.sunrad, 32, 16 );
    spheregeom.rotateX( Math.PI / 2. );
    var spheremat;
    if ( params.suncolor != null ) {
        spheremat = new THREE.MeshLambertMaterial( { color: params.suncolor,
                                                      emissive: params.suncolor,
                                                   } );
    }
    else {
        var loader = new THREE.TextureLoader();
        var suntexture = loader.load( PhysicsObjects.baseurl + "assets/1024px-Map_of_the_full_sun.jpg" );
        spheremat = new THREE.MeshLambertMaterial( { emissive: 0xffffff } );
        spheremat.emissiveMap = suntexture;
    }
    this.sun = new THREE.Mesh( spheregeom, spheremat );
    this.scene.scene.add( this.sun );

    this.planet = new Planet( { scene: this.scene,
                                G: params.G,
                                starmass: params.sunmass,
                                mass: params.planetmass,
                                radius: params.planetrad,
                                color: params.planetcolor,
                                texture: params.planettexture,
                                emit: params.planetemit,
                                emittexture: params.planetemittexture,
                                emitintensity: params.planetemitintensity,
                                trail: params.trail,
                                trailrad: params.trailrad,
                                trailcolor: params.trailcolor,
                                trailemit: params.trailemit,
                                traildt:params.traildt,
                                showvel: params.showvel,
                                velscale: params.velscale,
                                velrad: params.planetrad * 0.25,
                                velcolor: params.velcolor,
                                velemit: params.velemit,
                                showacc: params.showacc,
                                accscale: params.accscale,
                                accrad: params.planetrad * 0.25,
                                acccolor: params.acccolor,
                                accemit: params.accemit
                              } );
    this.initplanetpos = [...params.planetpos];
    this.initplanetvel = [...params.planetvel];
    this.resetPlanet();
    
    this.movecallback = function( t ) { self.updatePlanet( t ); }
};

// **********************************************************************

OnePlanetOrbits.prototype = Object.create( StandardScene.prototype );
Object.defineProperty( OnePlanetOrbits.prototype, "constructor",
                       { value: OnePlanetOrbits,
                         enumerable: false,
                         writeable: true } );

// **********************************************************************

OnePlanetOrbits.colors = [ "#cc0000",
                           "#007000",
                           "#0000cc",
                           "#cc9900",
                           "#33cc00",
                           "#cc00cc" ];

// **********************************************************************

OnePlanetOrbits.prototype.layout = function( parent )
{
    StandardScene.prototype.layout.call( this );
    
    var self = this;

    var controls = {
        type: "box",
        id: "controlsdiv",
        _class: "controlsdiv",
        spec: [
            { type: "button",
              id: "startbutton",
              text: "Go!",
              callback: function() { self.startMoving(); }
            },
            { type: "button",
              id: "stopbutton",
              text: "Stop",
              callback: function() { self.stopMoving(); }
            },
            { type: "button",
              id: "resetbutton",
              text: "Reset",
              callback: function() { self.resetPlanet(); }
            },
            
            { type: "button",
              _class: "margintop",
              id: "saveposvelbutton",
              text: "Save Pos/Vel",
              callback: function() { self.saveDefaultParams(); }
            },

            { type: "span",
              id: "postitle",
              _class: "margintop",
              text: "Position (AU)"
            },
            { type: "box",
              id: "positionbox",
              _class: "hbox",
              spec: [
                  { type: "input",
                    width: "6ex",
                    id: "xpos",
                    changecallback: function() { self.parseWidgets(); }
                  },
                  { type: "input",
                    width: "6ex",
                    id: "ypos",
                    changecallback: function() { self.parseWidgets(); }
                  },
                  { type: "input",
                    width: "6ex",
                    id: "zpos",
                    changecallback: function() { self.parseWidgets(); }
                  },
              ]
            },
            { type: "box",
              id: "distancebox",
              _class: "hbox aligncenter",
              spec: [
                  { type: "span",
                    id: "distancelabel",
                    text: "|r⃗|= ",
                  },
                  { type: "input",
                    id: "distance",
                    width: "8ex",
                    readonly: true,
                    _class: "readonly"
                  }
              ]
            },
            { type: "span",
              id: "veltitle",
              _class: "margintop",
              text: "Velocity (AU/yr)"
            },
            { type: "box",
              id: "velocitybox",
              _class: "hbox",
              spec: [
                  { type: "input",
                    width: "6ex",
                    id: "xvel",
                    changecallback: function() { self.parseWidgets(); }
                  },
                  { type: "input",
                    width: "6ex",
                    id: "yvel",
                    changecallback: function() { self.parseWidgets(); }
                  },
                  { type: "input",
                    width: "6ex",
                    id: "zvel",
                    changecallback: function() { self.parseWidgets(); }
                  },
              ]
            },
            { type: "box",
              id: "speedbox",
              _class: "hbox aligncenter",
              spec: [
                  { type: "span",
                    id: "speedlabel",
                    text: "|v⃗|= ",
                  },
                  { type: "input",
                    id: "speed",
                    width: "8ex",
                    readonly: true,
                    _class: "readonly"
                  }
              ]
            },
            { type: "button",
              id: "setcircularbutton",
              text: "Set Circle V",
              callback: function() { self.setCircleV(); }
            },
            { type: "span",
              id: "acctitle",
              _class: "margintop",
              text: "Accel. (AU/yr²)"
            },
            { type: "box",
              id: "accelerationbox",
              _class: "hbox",
              spec: [
                  { type: "input",
                    width: "6ex",
                    id: "xacc",
                    readonly: true,
                    _class: "readonly",
                  },
                  { type: "input",
                    width: "6ex",
                    id: "yacc",
                    readonly: true,
                    _class: "readonly",
                  },
                  { type: "input",
                    width: "6ex",
                    id: "zacc",
                    readonly: true,
                    _class: "readonly",
                  },
              ]
            },
            { type: "box",
              id: "magaccbox",
              _class: "hbox aligncenter",
              spec: [
                  { type: "span",
                    id: "magacclabel",
                    text: "|a⃗|= ",
                  },
                  { type: "input",
                    id: "magacc",
                    width: "8ex",
                    readonly: true,
                    _class: "readonly"
                  }
              ]
            },

            { type: "table",
              id: "energytable",
              _class: "margintop",
              spec: [
                  { type: "tr",
                    id: "energytableheaderrow",
                    spec: [
                        { type: "td",
                          id: "KElabeltd",
                          spec: [
                              { type: "span",
                                id: "KElabel",
                                text: "KE"
                              }
                          ]
                        },
                        { type: "td",
                          id: "PElabeltd",
                          spec: [
                              { type: "span",
                                id: "PElabel",
                                text: "PE"
                              }
                          ]
                        },
                        { type: "td",
                          id: "Elabeltd",
                          spec: [
                              { type: "span",
                                id: "Elabel",
                                text: "Etot"
                              }
                          ]
                        }
                    ]
                  },
                  { type: "tr",
                    id: "energytablerow",
                    spec: [
                        { type: "td",
                          id: "KEtd",
                          spec: [
                              { type: "input",
                                id: "KE",
                                width: "7ex",
                                readonly: true,
                                _class: "readonly"
                              }
                          ]
                        },
                        { type: "td",
                          id: "PEtd",
                          spec: [
                              { type: "input",
                                id: "PE",
                                width: "7ex",
                                readonly: true,
                                _class: "readonly"
                              }
                          ]
                        },
                        { type: "td",
                          id: "Elabeltd",
                          spec: [
                              { type: "input",
                                id: "E",
                                width: "7ex",
                                readonly: true,
                                _class: "readonly"
                              }
                          ]
                        }
                    ]
                  },
              ]
            },
            
            { type: "box",
              id: "showvbox",
              _class: "hbox margintop",
              spec: [
                  { type: "checkbox",
                    id: "showvel",
                    checked: this.planet.showvel,
                    callback: function() { self.planet.showvel = self.controls[ "showvel" ].checked; }
                  },
                  { type: "span",
                    id: "showvellabel",
                    _class: "vvector",
                    text: " show v⃗"
                  }
              ]
            },

            { type: "box",
              id: "showabox",
              _class: "hbox",
              spec: [
                  { type: "checkbox",
                    id: "showacc",
                    checked: this.planet.showacc,
                    callback: function() { self.planet.showacc = self.controls[ "showacc" ].checked; }
                  },
                  { type: "span",
                    id: "showacclabel",
                    _class: "avector",
                    text: " show a⃗"
                  }
              ]
            },

            { type: "box",
              id: "trailbox",
              _class: "hbox",
              spec: [
                  { type: "checkbox",
                    id: "trail",
                    checked: this.planet.trail,
                    callback: function() { self.planet.trail = self.controls[ "trail" ].checked; }
                  },
                  { type: "span",
                    id: "traillabel",
                    text: " leave trail"
                  }
              ]
            },

            { type: "box",
              id: "deltaphibox",
              _class: "hbox aligncenter",
              spec: [
                  { type: "span",
                    id: "deltaphilabel",
                    text: "Δφ: "
                  },
                  { type: "input",
                    id: "deltaphi",
                    width: "6ex",
                    initval: "360"
                  },
                  { type: "span",
                    id: "deltaphidegrees",
                    text: "°",
                  }
              ]
            },

            { type: "box",
              id: "timefactorbox",
              _class: "hbox aligncenter",
              spec: [
                  { type: "span",
                    id: "timefactorlabel",
                    text: "Time factor: "
                  },
                  { type: "input",
                    id: "timefactor",
                    width: "4ex",
                    initval: this.scene.timedilation,
                    changecallback: function() {
                        var str = self.controls[ "timefactor" ].value;
                        var val = parseFloat( str );
                        if ( isNaN( val ) ) {
                            window.alert( "Error parsing \"" + str + "\" as a number." );
                        }
                        else {
                            self.scene.timedilation = val;
                        }
                    }
                  },
                  { type: "span",
                    id: "timefactorunits",
                    text: "s/yr"
                  }
              ]
            },
            
            { type: "button",
              id: "clearplotbutton",
              _class: "margintop",
              text: "Clear Plots",
              callback: function() {
                  self.rplot.clear();
                  self.vplot.clear();
                  self.aplot.clear();
                  self.colordex = 0;
              }
            },

        ]
    };

    this.controls = InterfaceMaker( controls );

    this.maindiv.appendChild( this.controls[ "controlsdiv" ] );
    this.xpos = this.controls[ "xpos" ];
    this.ypos = this.controls[ "ypos" ];
    this.zpos = this.controls[ "zpos" ];
    this.xvel = this.controls[ "xvel" ];
    this.yvel = this.controls[ "yvel" ];
    this.zvel = this.controls[ "zvel" ];
    this.xacc = this.controls[ "xacc" ];
    this.yacc = this.controls[ "yacc" ];
    this.zacc = this.controls[ "zacc" ];
    this.KEwid = this.controls[ "KE" ];
    this.PEwid = this.controls[ "PE" ];
    this.Ewid = this.controls[ "E" ];
    this.distance = this.controls[ "distance" ];
    this.speed = this.controls[ "speed" ];
    this.magacc = this.controls[ "magacc" ];

    if ( ! this.showEwidgets ) this.controls[ "energytable" ].style.display = "none";
    if ( ! this.showcircVwidget ) this.controls[ "setcircularbutton" ].style.display = "none";
    if ( ! this.showsaveparamsbutton ) this.controls[ "saveposvelbutton" ].style.display = "none";
    
    this.disablewhengo = [ this.xpos, this.ypos, this.zpos, this.xvel, this.yvel, this.zvel,
                           this.xacc, this.yacc, this.zacc,
                           this.controls[ "showvel" ], this.controls[ "showacc" ], this.controls[ "trail" ],
                           this.controls[ "startbutton" ], this.controls [ "resetbutton" ]
                         ];
    this.disablewhenstop = [ this.controls[ "stopbutton" ] ];

    for ( let wid of this.disablewhenstop ) wid.disabled = true;
    
    this.updateWidgets();

    // Plots

    var plotwid = 800;
    var plothei = 600;
    
    var hbox = document.createElement( "div" );
    hbox.setAttribute( "class", "hbox" );
    document.body.appendChild( hbox );

    this.rplot = new SVGPlot.Plot( { divid: "rplotdiv",
                                     svgid: "rplotsvg",
                                     ytitle: "r (AU)",
                                     xtitle: "t (year)",
                                     minautoyrange: 0.2,
                                     width: plotwid,
                                     height: plothei
                                   } );
    this.vplot = new SVGPlot.Plot( { divid: "vplotdiv",
                                     svgid: "vplotsvg",
                                     ytitle: "v (AU/yr)",
                                     xtitle: "t (year)",
                                     minautoyrange: 0.2,
                                     width: plotwid,
                                     height: plothei
                                   } );
    this.aplot = new SVGPlot.Plot( { divid: "aplotdiv",
                                     svgid: "aplotsvg",
                                     ytitle: "a (AU/yr²)",
                                     xtitle: "t (year)",
                                     minautoyrange: 0.5,
                                     width: plotwid,
                                     height: plothei
                                   } );

    hbox.appendChild( this.rplot.topdiv );
    hbox.appendChild( this.vplot.topdiv );
    hbox.appendChild( this.aplot.topdiv );

    this.rplot.redraw();
    this.vplot.redraw();
    this.aplot.redraw();
}

// **********************************************************************

OnePlanetOrbits.prototype.resetPlanet = function()
{
    this.planet.wipeTrail();
    this.planet.position = { x: this.initplanetpos[0], y: this.initplanetpos[1], z: this.initplanetpos[2] };
    this.planet.velocity = { x: this.initplanetvel[0], y: this.initplanetvel[1], z: this.initplanetvel[2] };
    this.planet.acceleration = this.planet.accel( 0., this.planet.position, this.planet.velocity, this.planet, 0. );
    this.planet.update();
    this.updateWidgets();
}

// **********************************************************************

OnePlanetOrbits.prototype.saveDefaultParams = function()
{
    this.initplanetpos = [ this.planet.x, this.planet.y, this.planet.z ];
    this.initplanetvel = [ this.planet.vx, this.planet.vy, this.planet.vz ];
}

// **********************************************************************

OnePlanetOrbits.prototype.setCircleV = function()
{
    var cross = { x:0, y:0, z:0 };
    if ( Math.abs( this.planet.position.x ) < 1e-6 && Math.abs( this.planet.position.y ) < 1e-6 ) {
        window.alert( "I don't know how to set circular orbits for planets on the z-axis yet." );
        return
    }
    else {
        // Cross z with the planet's position
        cross.x = -this.planet.position.y;
        cross.y = this.planet.position.x;
    }
    var crossmag = Math.sqrt( cross.x**2 + cross.y**2 + cross.z**2 );
    cross.x /= crossmag;
    cross.y /= crossmag;
    cross.z /= crossmag;
    var r = Math.sqrt( this.planet.position.x**2 + this.planet.position.y**2 + this.planet.position.z**2 );
    var v = Math.sqrt( this.planet.GM / r );
    this.planet.vx = v * cross.x;
    this.planet.vy = v * cross.y;
    this.planet.vz = v * cross.z;
    this.updateWidgets();
}

// **********************************************************************

OnePlanetOrbits.prototype.startMoving = function()
{
    // console.log( "Starting moving..." );
    for ( let wid of this.disablewhengo ) {
        wid.disabled = true;
    }
    for ( let wid of this.disablewhenstop ) {
        wid.disabled = false;
    }
    this.parseWidgets();
    this.planet.wipeTrail();
    this.planet.initializeState( this.scene.t, this.planet.position, this.planet.velocity, this.planet.acceleration );
    this.t0 = this.planet.t;
    console.log( "Initialized planet to t= " + this.planet.t );
    var r = Math.sqrt( this.planet.x**2 + this.planet.y**2 + this.planet.z**2 );
    var v = Math.sqrt( this.planet.vx**2 + this.planet.vy**2 + this.planet.vz**2 );
    var a = Math.sqrt( this.planet.ax**2 + this.planet.ay**2 + this.planet.az**2 );
    this.phi0 = Math.atan2( this.planet.y , this.planet.x );
    this.phioff = 0.;
    this.lastphi = this.phi0;
    this.scene.addAnimationCallback( this.movecallback );
    this.planet.initTrail();

    var color = OnePlanetOrbits.colors[ this.colordex ];
    this.colordex += 1;
    if ( this.colordex >= OnePlanetOrbits.colors.length ) this.colordex = 0;
    this.rdataset = new SVGPlot.Dataset( { color: color } );
    this.rplot.addDataset( this.rdataset );
    this.vdataset = new SVGPlot.Dataset( { color: color } );
    this.vplot.addDataset( this.vdataset );
    this.adataset = new SVGPlot.Dataset( { color: color } );
    this.aplot.addDataset( this.adataset );

    this.rdataset.addPoint( 0, r );
    this.vdataset.addPoint( 0, v );
    this.adataset.addPoint( 0, a );
    this.plott0 = this.t0;
    this.nextplot = this.t0 + this.plotdt;
}

// **********************************************************************

OnePlanetOrbits.prototype.stopMoving = function()
{
    // console.log( "Stopping moving..." );
    this.scene.removeAnimationCallback( this.movecallback );
    for ( let wid of this.disablewhengo ) {
        wid.disabled = false;
    }
    for ( let wid of this.disablewhenstop ) {
        wid.disabled = true;
    }

    this.rplot.redraw();
    this.vplot.redraw();
    this.aplot.redraw();
}

// **********************************************************************

OnePlanetOrbits.prototype.parseWidgets = function()
{
    var parselist = [ { wid: "xpos", dest: "x" },
                      { wid: "ypos", dest: "y" },
                      { wid: "zpos", dest: "z" },
                      { wid: "xvel", dest: "vx" },
                      { wid: "yvel", dest: "vy" },
                      { wid: "zvel", dest: "vz" }
                    ];
    var parsed = true;
    for ( let toparse of parselist ) {
        let str = this.controls[ toparse.wid ].value;
        let val = parseFloat( str );
        if ( isNaN( val ) ) {
            window.alert( "Error parsing \"" + str + "\" as a number." );
            parsed = false;
        }
        else {
            this.planet[ toparse.dest ] = val
        }
    }
    if ( parsed ) {
        this.planet.update();
        this.updateWidgets();
    }

    var str = this.controls[ "deltaphi" ].value;
    var val = parseFloat( str );
    if ( isNaN( val ) )
        window.alert( "Error parsing \"" + str + "\" as a number." );
    else
        this.deltaphi = val * Math.PI / 180.;
}       

// **********************************************************************

OnePlanetOrbits.prototype.updateWidgets = function()
{
    if ( this.xpos == undefined) return;
    this.xpos.value = this.planet.x.toFixed( 3 );
    this.ypos.value = this.planet.y.toFixed( 3 );
    this.zpos.value = this.planet.z.toFixed( 3 );
    this.xvel.value = this.planet.vx.toFixed( 3 );
    this.yvel.value = this.planet.vy.toFixed( 3 );
    this.zvel.value = this.planet.vz.toFixed( 3 );
    this.xacc.value = this.planet.ax.toFixed( 3 );
    this.yacc.value = this.planet.ay.toFixed( 3 );
    this.zacc.value = this.planet.az.toFixed( 3 );
    var r = Math.sqrt( this.planet.x**2 + this.planet.y**2 + this.planet.z**2 );
    this.distance.value = r.toFixed( 3 );
    var v = Math.sqrt( this.planet.vx**2 + this.planet.vy**2 + this.planet.vz**2 );
    this.speed.value = v.toFixed( 3 );
    this.magacc.value = Math.sqrt( this.planet.ax**2 + this.planet.ay**2 + this.planet.az**2 ).toFixed( 3 );
    var ke = 0.5 * this.planet.mass * v**2;
    var pe = -this.planet.GM * this.planet.mass / r;
    this.KEwid.value = ke.toExponential( 2 );
    this.PEwid.value = pe.toExponential( 2 );
    this.Ewid.value = (ke+pe).toExponential( 2 );
}

// **********************************************************************

OnePlanetOrbits.prototype.updatePlanet = function( t )
{
    console.log( "Updating planet to time " + t + " ; planet.oldt = " + this.planet.oldt );
    this.planet.updateState( t );
    this.planet.update();
    this.updateWidgets();

    if ( t >= this.nextplot ) {
        var r = Math.sqrt( this.planet.x**2 + this.planet.y**2 + this.planet.z**2 );
        var v = Math.sqrt( this.planet.vx**2 + this.planet.vy**2 + this.planet.vz**2 );
        var a = Math.sqrt( this.planet.ax**2 + this.planet.ay**2 + this.planet.az**2 );
        this.rdataset.addPoint( t - this.t0, r );
        this.vdataset.addPoint( t - this.t0, v );
        this.adataset.addPoint( t - this.t0, a );
        this.nextplot += this.plotdt;
    }
    
    var rawphi = Math.atan2( this.planet.y , this.planet.x );

    // Check for stopping; stop when φ - φ0 >= this.deltahi

    // console.log( "Planet moved.  phi0 = " + this.phi0 + " , lastphi = " + this.lastphi + " , phi = " + phi );
    
    // There's a discontinuity at π because of what atan2 returns..  Detect
    //  crossing of π and adjust phy accordingly

    if ( ( rawphi < -Math.PI/2 && this.lastphi > Math.PI/2 ) )
        this.phioff += 2*Math.PI;
    else if ( ( rawphi > Math.PI/2 && this.lastphi < -Math.PI/2 ) )
        this.phioff -= 2*Math.PI;

    var phi = rawphi + this.phioff;
    if ( ( phi - this.phi0 ) >= this.deltaphi ) {
        this.stopMoving();
    }

    // ROB, also check for hyperbolic unbound

    this.lastphi = rawphi;
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// A planet orbiting a star at the origin

var Planet = function( inparams = {} )
{
    var self = this;

    var params = { name: "Planet",
                   G: 4 * Math.PI**2,
                   scene: null,
                   starmass: 1,
                   mass: 3.00349e-6,
                   radius: 0.05,
                   color: 0x8080ff,
                   texture: null,
                   emit: 0x202040,
                   emittexture: null,
                   emitintensity: 1,
                   trail: true,
                   trailrad: 0.01,
                   trailcolor: 0x4040ff,
                   trailemit: 0x101040,
                   traildt: 0.01,       // This is in planet time, so scene time dilation has been applied
                   showvel: true,
                   velscale: 0.1,
                   velrad: 0.02,
                   velcolor: 0x0000c0,
                   velemit: 0x000040,
                   showacc: true,
                   accscale: 0.1,
                   accrad: 0.02,
                   acccolor: 0xc00000,
                   accemit: 0x400000
                 };
    Object.assign( params, inparams );

    this.GM = params.G * params.starmass;
    this.scene = params.scene;

    this.radius = params.radius;
    
    PhysicsObjects.Object.call( this, { name: params.name,
                                        parent: params.scene,
                                        mass: params.mass,
                                        rx: params.radius,
                                        ry: params.radius,
                                        rz: params.radius,
                                        accelfunc: function( t, pos, vel, obj, dt ) {
                                            return self.accel( t, pos, vel, obj, dt )
                                        },
                                      } );

    var spheregeom = new THREE.SphereGeometry( params.radius );
    spheregeom.rotateX( Math.PI / 2. );
    var spheremat = new THREE.MeshStandardMaterial( { color: params.color,
                                                      emissive: params.emit,
                                                      emissiveIntensity: params.emitintensity,
                                                      metalness: 0.1,
                                                      roughness: 0.2 } );
    if ( params.texture != null || params.emittexture != null ) {
        var loader = new THREE.TextureLoader();
        if ( params.texture != null ) {
            let texture = loader.load( params.texture );
            spheremat.map = texture;
        }
        if ( params.emittexture != null ) {
            let texture = loader.load( params.emittexture );
            spheremat.emissiveMap = texture;
        }
    }
    this.visobj = new THREE.Mesh( spheregeom, spheremat );
    if ( this.scene != null )
        this.scene.addObject( this );

    this.vvector = new PhysicsVector( { parent: params.scene,
                                        xcomp: 0, ycomp: 0.5, zcomp:0,
                                        cylrad: params.velrad,
                                        conelength: 3*params.velrad, conebaserad: 2*params.velrad,
                                        color: params.velcolor, emit: params.velemit,
                                        name: "Velocity" } );
    this.avector = new PhysicsVector( { parent: params.scene,
                                        xcomp: 0, ycomp: 0.5, zcomp:0,
                                        cylrad: params.accrad,
                                        conelength: 3*params.accrad, conebaserad: 2*params.accrad,
                                        color: params.acccolor, emit: params.accemit,
                                        name: "Acceleration" } );
    if ( params.showvel ) {
        this._showvel = true
        this.scene.addObject( this.vvector );
    }
    else this._showvel = false;
    if ( params.showacc ) {
        this._showacc = true
        this.scene.addObject( this.avector );
    }
    else this._showacc = false;

    this.velscale = params.velscale;
    this.accscale = params.accscale;
    
    this._trail = params.trail;
    this.trailgeo = new THREE.CylinderGeometry( params.trailrad, params.trailrad, 1, 6 );
    this.trailmat = new THREE.MeshLambertMaterial( { color: params.trailcolor,
                                                     emissive: params.trailemit} );
    this.trailcyls = [];
    this.lasttrailpos = null;
    this.lasttrailt = 0;
    this.traildt = params.traildt;

    this.eps2 = 1e-4;
}
    
Planet.prototype = Object.create( PhysicsObjects.Object.prototype );
Object.defineProperty( Planet.prototype, 'constructor',
                       {
                           value: Planet,
                           enumerate: false,
                           writeable: true } );


Object.defineProperty( Planet.prototype, "showvel", {
    get() { return this._showvel },
    set( val ) {
        if ( val && ! this._showvel ) {
            this._showvel = true;
            this.scene.addObject( this.vvector );
        }
        else if ( this._showvel && ! val ) {
            this._showvel = false;
            this.scene.removeObject( this.vvector );
        }
    }
} );

Object.defineProperty( Planet.prototype, "showacc", {
    get() { return this._showacc },
    set( val ) {
        if ( val && ! this._showacc ) {
            this._showacc = true;
            this.scene.addObject( this.avector );
        }
        else if ( this._showacc && ! val ) {
            this._showacc = false;
            this.scene.removeObject( this.avector );
        }
    }
} );

Object.defineProperty( Planet.prototype, "trail", {
    get() { return this._trail },
    set( val ) { this._trail = val }
} );

// **********************************************************************

Planet.prototype.accel = function( t, pos, vel, obj, dt )
{
    var r2 = pos.x**2 + pos.y**2 + pos.z**2;
    var r = Math.sqrt( r2 );
    var rhatx = pos.x / r;
    var rhaty = pos.y / r;
    var rhatz = pos.z / r;

    var a = this.GM / ( r2 + this.eps2 );
    return { x: -rhatx * a,
             y: -rhaty * a,
             z: -rhatz * a };
}

// **********************************************************************

Planet.prototype.wipeTrail = function()
{
    this.lasttrailpos = null;
    this.lasttrailt = 0;
    for ( let cyl of this.trailcyls ) {
        this.scene.scene.remove( cyl );
    }
    this.trailcyls = [];
}

// **********************************************************************

Planet.prototype.initTrail = function()
{
    this.wipeTrail();
    this.lasttrailt = this.t;
    this.lasttrailpos = { x: this.x, y: this.y, z: this.z };
}

// **********************************************************************

Planet.prototype.update = function()
{
    this.vvector.xcomp = this.velscale * this.vx;
    this.vvector.ycomp = this.velscale * this.vy;
    this.vvector.zcomp = this.velscale * this.vz;
    this.vvector.x = this.x + this.radius * this.vvector.hatx;
    this.vvector.y = this.y + this.radius * this.vvector.haty;
    this.vvector.z = this.z + this.radius * this.vvector.hatz;
    this.avector.xcomp = this.accscale * this.ax;
    this.avector.ycomp = this.accscale * this.ay;
    this.avector.zcomp = this.accscale * this.az;
    this.avector.x = this.x + this.radius * this.avector.hatx;
    this.avector.y = this.y + this.radius * this.avector.haty;
    this.avector.z = this.z + this.radius * this.avector.hatz;

    if ( this._trail) {
        if ( ( this.lasttrailpos != null ) && ( this.t - this.lasttrailt > this.traildt ) ) {
            var cyl = new THREE.Mesh( this.trailgeo, this.trailmat );
            var dx = this.x - this.lasttrailpos.x;
            var dy = this.y - this.lasttrailpos.y;
            var dz = this.z - this.lasttrailpos.z;
            var ds = Math.sqrt( dx*dx + dy*dy + dz*dz );
            var theta = Math.acos( dy / ds );
            var phi = Math.atan2( -dz , dx );
            cyl.rotateY( phi );
            cyl.rotateZ( -theta );
            cyl.scale.y = ds;
            cyl.position.x = this.lasttrailpos.x + dx/2;
            cyl.position.y = this.lasttrailpos.y + dy/2;
            cyl.position.z = this.lasttrailpos.z + dz/2;

            this.trailcyls.push( cyl );
            this.scene.scene.add( cyl );

            this.lasttrailt = this.t;
            this.lasttrailpos.x = this.x;
            this.lasttrailpos.y = this.y;
            this.lasttrailpos.z = this.z;
        }
    }
}

// **********************************************************************

export { OnePlanetOrbits };
