NOTE : Minimum versions supported are Chrome 64, Firefox 69 (because of ResizeObserver)

To install to test directory:

rsync --exclude="*~" --exclude="00readme.txt" --exclude=".git" -n -a -v --delete ./ /home/www/html/physicsobjects_test/

Remove the -n to do it for real
